import { Cart, Discount, EventDetail, Installments, Payment, Server, User, OttoAlertMessage, OttoImg, Purchase, ProductCart, PaymentMethod, OutgoOrigin, PaymentSaleMethod } from './model';
import { HttpClient } from "@angular/common/http";
import * as i0 from "@angular/core";
export declare class OutPayService {
    http: HttpClient;
    constructor(http: HttpClient);
    serverUrl: string;
    pagseguroUrl: string;
    mercadopagoKey: string;
    moderninha: boolean;
    server?: Server;
    origin: OutgoOrigin;
    purchase?: Purchase;
    cart: Cart;
    payment: Payment;
    ticketData?: any;
    installments: Installments[];
    isSelling: boolean;
    productsSale: boolean;
    productCart: ProductCart;
    inscriptionParticipantData: {
        name: string;
        email: string;
        phone?: string;
        cpf?: string;
        birthdate?: string;
        ticket_id: number;
        extra_fields?: {
            [fieldId: number]: string;
        };
    }[];
    selectedSaleInstallment: number;
    purchaser?: User;
    private _paymentMethod?;
    get paymentMethod(): "credit" | "boleto" | "free" | undefined;
    private _paymentSaleMethod?;
    get paymentSaleMethod(): PaymentSaleMethod | undefined;
    private _deliveryMethod?;
    get deliveryMethod(): "digital" | "physical" | undefined;
    private _grossValue?;
    get grossValue(): number;
    user?: User;
    eventDetail?: EventDetail;
    get platform(): "pagseguro" | "mercadopago" | undefined;
    /**
     * Verifica se é necessário pedir os dados do comprador.
     * Serve tanto para exibir a tela de form quanto pra exibir o formulário de dados do comprador
     */
    get needsBuyerData(): boolean;
    private _value;
    get value(): number;
    private setValue;
    get discount(): Discount | undefined;
    get boletoPermit(): boolean;
    private pagseguroLoad;
    private pagseguroPromise?;
    private senderHash?;
    private mercadopagoLoad;
    private mercadopagoPromise?;
    sessionId: string;
    setup(serverUrl: string, pagseguroUrl: string, mercadopagoKey: string, origin: OutgoOrigin, server: Server, data?: {
        moderninha?: boolean;
    }): void;
    /**
     * Limpar dados da compra/venda.
     * Deve ser chamado no começo de cada compra/venda pra evitar que uma compra anterior influencie compras futuras.
     */
    private cleanCache;
    /**
     * Sender hash é utilizado nas compras de pagseguro pra identificar o comprador.
     */
    private initSenderHash;
    private startPagseguroSession;
    pagseguroResolve(): Promise<boolean>;
    mercadopagoResolve(): Promise<unknown>;
    getInstallments(cardNumber?: string): Promise<Installments[]>;
    getBrand(cardNumber: string): Promise<string>;
    /**
     * Validação dos dados do usuário.
     */
    private personalDataValidate;
    /**
     * Função que carrega os dados do evento antes de iniciar a venda.
     * @param eventId O ID do evento.
     * @param userId O ID do usuário. Os dados do usuário são baixados apenas se o parâmetro for fornecido.
     * @throws GENERAL_ERROR se a função for chamada antes de salvar os dados do carrinho de compras.
     */
    eventPurchaseData(eventId: number, userId?: number): Promise<boolean>;
    private getEventDetail;
    getUserDetail(companyId: number, userId: number): Promise<User>;
    /**
     * Função que obtem os parâmetros básicos para criar uma compra no servidor.
     * @returns `null` Caso o usuário não esteja registrado, e um `Object` com os dados em caso contrário.
     */
    private getPaymentParams;
    /**************************************************
    INICIA AS COMPRAS
    Chamado após escolher os ingressos e clicar em comprar
    @throws VALIDATE_EMAIL caso a compra seja paga e o usuário ainda não tiver validado seu email.
    @throws UNAUTHENTICATED caso o id de usuário não seja informado.
    @throws TICKET_UNAVAILABLE caso o ingresso solicitado não esteja mais disponível.
    @param cart O carrinho de compras, com o desconto, caso exista.
    @param eventId O ID do evento.
    @param userId O ID do usuário, caso ele esteja logado no sistema.
    @param cleanPurchaseCache Se os dados da compra devem ser limpos do cache. `true` por padrão, só deve ser sobrescrito se necessário.
    **************************************************/
    init(cart: Cart, eventId: number, userId?: number, cleanPurchaseCache?: boolean): Promise<OutPayResult>;
    /**************************************************
    INICIA O PROCESSO DO VENDEDOR
    Chamado após escolher os ingressos e clicar em vender
    **************************************************/
    sellerInit(cart: Cart, eventId: number, userId: number): Promise<OutPayResult>;
    /**************************************************
    INICIA O PROCESSO DO VENDEDOR DE PRODUTOS
    Chamado após escolher os produtos e clicar em vender
    **************************************************/
    productSellerInit(productCart: ProductCart, eventDetail: EventDetail, user: User): OutPayResult;
    /**
     * saveFormData
     * Chamado após preencher os dados de formulário.
     */
    saveFormData(participantData: {
        name: string;
        email: string;
        phone?: string;
        cpf?: string;
        birthdate?: string;
        ticket_id: number;
        extra_fields?: {
            [fieldId: number]: string;
        };
    }[]): Promise<OutPayResult>;
    /**
     * selectPaymentMethod
     * Chamada após selecionar método de pagamento para compras online.
     */
    selectPaymentMethod(paymentMethod: PaymentMethod): OutPayResult;
    /**
     * Chamada após selecionar o método de pagamento para vendas.
     * @param paymentSaleMethod Método de pagamento da venda.
     * @param usingExternalAppForPayment Opcional. Permite que o outpay prossiga em uma venda de cartão sem usar moderninha.
     */
    selectSalePaymentMethod(paymentSaleMethod: PaymentSaleMethod, usingExternalAppForPayment?: boolean): Promise<OutPayResult>;
    /**
     * Chamada após selecionar a quantidade de parcelas em uma venda de cartão de crédito.
     * @param selectedInstallment O índice da quantidade de parcelas, iniciando em zero.
     */
    selectSaleInstallments(selectedInstallment: number): Promise<OutPayResult>;
    /**
     * Chamada após selecionar o método de entrega da venda.
     * @param deliveryMethod O método de entrega da venda.
     */
    selectDeliveryMethod(deliveryMethod: "digital" | "physical"): Promise<OutPayResult>;
    /**
     * Chamada para determinar o comprador em uma venda digital sem formulário.
     * @param email O email do comprador.
     * @param name O nome do comprador, para criar um novo usuário.
     * @throws NOT_FOUND caso seja informado apenas o email, e não exista usuário com esse email.
     */
    selectPurchaser(email: string, name?: string): Promise<OutPayResult>;
    /**
     * finishPurchase
     * Chamada após digitar os dados de pagamento.
     * @throws PURCHASE_ERROR quando ocorre um erro na compra.
     */
    finishPurchase(cardToken?: string): Promise<OutPayResult>;
    finishSale(): Promise<OutPayResult>;
    private pagseguroCreditCard;
    private finalizePurchase;
    private doPayment;
    private createPurchase;
    /**
     * Função que analísa a compra, e retorna o próximo input necessário pelo usuário.
     * @returns Um objeto `OutPayResult`, com o próximo passo e objeto da compra.
     */
    private nextStepSale;
    static ɵfac: i0.ɵɵFactoryDef<OutPayService, never>;
    static ɵprov: i0.ɵɵInjectableDef<OutPayService>;
}
export declare type OutPayResult = {
    finished: boolean;
    nextStep: OutPayStep;
    purchase?: Purchase;
};
export declare enum OutPayStep {
    /** Seleção de forma de pagamento */
    PAYMENT_METHOD = 0,
    /** Formulário de dados do comprador e dos participantes */
    PURCHASE_FORM = 1,
    /** Impressão do ingresso, nos casos onde o pagamento não ocorre via PlugPag (ie. Dinheiro) */
    PRINT = 2,
    /** Finalizar processo de compra */
    FINISH = 3,
    /** Formulário de dados do pagamento (cartão) */
    PAYMENT_DATA = 4,
    /** Seleção de forma de recebimento */
    DELIVERY_METHOD = 5,
    /** Seleção do parcelamento do pagamento */
    INSTALLMENTS = 6,
    /** Pagamento via PlugPag */
    PLUGPAG = 7,
    /** Identificação do comprador para vendas digitais sem formulário */
    PURCHASER = 8
}
export declare class OutPayError {
    msg: string;
    errorCode: OutPayErrorCode;
    data: {
        otto?: boolean;
        ottoData?: {
            image: OttoImg;
            duration?: number;
        };
    };
    constructor(msg: string, errorCode?: OutPayErrorCode, data?: {
        otto?: boolean;
        ottoData?: {
            image: OttoImg;
            duration?: number;
        };
    });
    get ottoAlert(): OttoAlertMessage;
}
export declare enum OutPayErrorCode {
    /** Erro genérico */
    GENERAL_ERROR = 1,
    /** O usuário precisa validar o email para prosseguir */
    VALIDATE_EMAIL = 2,
    /** O ingresso solicitado já está esgotado */
    TICKET_UNAVAILABLE = 3,
    /** Houve um erro ao processar a compra */
    PURCHASE_ERROR = 4,
    /** O usuário não está logado no sistema */
    UNAUTHENTICATED = 5,
    /** Não é possível executar a ação, um caminho alternativo deve ser buscado */
    UNABLE_TO_PROCEED = 6,
    /** Não foi encontrado um resultado para os dados fornecidos */
    NOT_FOUND = 7
}
