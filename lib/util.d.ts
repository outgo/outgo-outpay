/**
 * Função que arredonda o número em duas casas decimais.
 * @param number O número a ser formatado
 * @param separator O separador a ser utilizado. Usa ',' por padrão.
 * @returns Uma string correspondente ao número fornecido, com o separador desejado.
 */
export declare function valueRound(number: number, separator?: string): string;
