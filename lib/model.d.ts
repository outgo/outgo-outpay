export declare type User = {
    id: number;
    name: string;
    username: string;
    cpf: string;
    email: string;
    birthdate: string;
    phone: string;
    thumbxl: string;
    thumblg: string;
    thumbmd: string;
    thumbsm: string;
    thumbxs: string;
    address: Address;
    companyUserId?: number;
    purchase_tax: number;
    has_notification: boolean;
    discounts_available: number;
    know: number;
    user_type: "participant" | "producer";
    payment_accounts: PaymentAccount[];
    companies: (Company | Producer)[];
    is_outgo?: boolean;
    client: {
        [key: string]: (string | undefined);
    };
};
export declare type Company = {
    id: number;
    type: "Company";
    name: string;
    identifier: string;
    avatar_url: string;
    address: Address;
    phone: string;
    email: string;
    category: number;
    category_text: string;
    subcategory: number;
    subcategory_text: string;
    description: string;
    owner_id: number;
    info: string;
    priority?: number;
    city?: string;
    stateCode?: string;
    google_place_id?: string;
    cnpj?: string;
    instagram?: string;
    team: TeamMember[];
};
export declare type Event = {
    id: number;
    clientId: number;
    name: string;
    completeDate: string;
    identificador: string;
    thumblg: string;
    thumbmd: string;
    thumbsm: string;
    status: number;
    startDate: string;
    endDate: string;
    location: string;
    place?: Company;
    usersAccess: {
        [key: number]: AccessLevel;
    };
    isPublic: boolean;
    ticket_count: number;
    has_notification: boolean;
    publish_time?: string;
    syncedAt: string;
    lastSync: string;
    isLiked?: boolean;
    numLikes?: number;
    numComments?: number;
    isCommented?: boolean;
};
export declare type Producer = {
    id: number;
    type: "User";
    avatar_url: string;
    name: string;
    identifier: string;
};
export declare type EventDetail = {
    event: Event;
    ownerId: number;
    managedBy: string;
    managerContact: string;
    tag: string;
    logo?: string;
    description: string;
    participants_count: number;
    income: number;
    allow_products: boolean;
    product_sales_count: number;
    product_stock: number;
    product_income: number;
    hideTax: boolean;
    hiddenTickets: Ticket[];
    tickets: Ticket[];
    ticket_link: string;
    ticket_link_access_count: number;
    discounts: Discount[];
    hasActiveDiscounts: boolean;
    team: TeamMember[];
    purchases: Purchase[];
    purchased_tickets: PurchasedTicket[];
    canShare: boolean;
    paymentMethod: "pagseguro" | "mercadopago";
    payment_account?: PaymentAccount;
    edit_date?: string;
    edit_end_date?: string;
    updatedAt?: string;
    publish_message?: string;
    tax_credit_buyer: number;
    tax_debit_buyer: number;
    tax_credit_system: number;
    tax_debit_system: number;
    installmentsWithoutInterest: number;
    physicalInstallmentsWithoutInterest: number;
    allow_online_sales?: boolean;
    allow_digital_sales?: boolean;
    allow_physical_sales?: boolean;
    allow_reservations?: boolean;
    allow_lists: boolean;
    allow_transfer?: boolean;
    allow_money?: boolean;
    allow_credit?: boolean;
    allow_debit?: boolean;
    allowComments: boolean;
    hasBoleto: boolean;
    cardSalesOnApp?: boolean;
    boletoDeadline: string;
    have_participant_form: boolean;
    participant_form_phone: boolean;
    participant_form_cpf: boolean;
    participant_form_birthdate: boolean;
    participant_form_extra_fields: ParticipantFormField[];
    comments: Comment[];
    producer?: Producer | Company;
    is_online?: boolean;
    instagram_live_link?: string;
    youtube_live_link?: string;
    external_live_link?: string;
    inventory: Product[];
    product_categories: ProductCategory[];
    map_url?: string;
    sections: EventSection[];
};
export declare type TeamMember = {
    id: number;
    userId: number;
    name: string;
    email: string;
    thumbxl: string;
    thumblg: string;
    thumbmd: string;
    thumbsm: string;
    thumbxs: string;
    permission: AccessLevel;
    created: string;
    eventId?: number;
    commission?: number;
    forbidden_product_ids?: number[];
};
export declare type AccessLevel = "edit" | "view" | "checkin" | "seller" | "product_manager" | "product_seller" | "none";
export declare type PurchasedTicket = {
    id: number;
    ticket_name: string;
    name: string;
    email: string;
    was_shared: boolean;
    checkin_time: string;
    voucher: string;
    owner_name: string;
};
export declare type PaymentAccount = {
    id: number;
    type: PaymentAccountType;
    email?: string;
    owner_name?: string;
    bank_name?: string;
    bank_code?: string;
    branch_code?: string;
    account_number?: string;
    account_type?: "Conta corrente" | "Conta poupança";
};
export declare type PaymentAccountType = "pagseguro" | "mercadopago" | "bank";
export declare type ParticipantFormField = {
    id: number;
    label: string;
    fieldType: FormFieldType;
    publicType: string;
    options?: {
        name: string;
        value: string;
    }[];
};
export declare type FormFieldType = "simple_text" | "multiple_choice";
export declare type Product = {
    id: number;
    name: string;
    avatar_url: string;
    description: string;
    price: number;
    quantity: number;
    been_sold: boolean;
    flows?: ProductFlow[];
    category?: ProductCategory;
    category_id?: number;
    product_id?: number;
};
export declare type ProductCategory = {
    id: number;
    name: string;
    description?: string;
};
export declare type ProductFlow = {
    id: number;
    quantity: number;
    cost: number;
    description: string;
    created_at: string;
};
export declare type EventSection = {
    id: number;
    name: string;
    image_url: string;
    image_thumb: string;
    status: string;
    section_reservations: SectionReservation[];
    tickets: Ticket[];
    has_reservations: boolean;
    image?: string;
};
export declare type PaymentMethod = "credit" | "boleto" | "free";
export declare enum PaymentSaleMethod {
    FREE = -1,
    MONEY = 0,
    CREDIT = 1,
    DEBIT = 2,
    TRANSFER = 3
}
export declare type Purchase = {
    id: number;
    status: number;
    statusText: string;
    eventId: number;
    eventName: string;
    userId: number;
    purchaseDate: string;
    confirmationDate: string;
    boleto_link: string;
    cancellation_type: string;
    payment_type: string;
    purchase_type: "online" | "digital" | "physical" | "products";
    isOnline: boolean;
    buyerName: string;
    buyerEmail: string;
    buyerPhone: string;
    buyerId?: number;
    buyerAvatar?: string;
    sellerName?: string;
    sellerId?: number;
    pdv?: string;
    price: number;
    finalPrice: number;
    commission?: number;
    pagseguroCode?: string;
    pagseguroEmail?: string;
    paymentMedium: "Mercado Pago" | "PagSeguro";
    paymentMethod: string;
    purchaseTickets: PurchaseTicket[];
    purchaseProducts: PurchaseProduct[];
    paymentCode: string;
    origin: string;
    printCount: number;
    canReprint: boolean;
    event_discounts_id?: number;
};
export declare type PurchaseTicket = {
    id: number;
    ticketId: number;
    ticketName: string;
    price: number;
    finalPrice: number;
    message: string;
    voucher?: string;
    email?: string;
    name?: string;
    ticketPrice: number;
};
export declare type PurchaseProduct = {
    id: number;
    productId: number;
    productName: string;
    price: number;
    code?: string;
    commission?: number;
};
export declare type Ticket = {
    id: number;
    name: string;
    price: number;
    final_price: number;
    tax_payed_by_company: boolean;
    available: boolean;
    quantity: number;
    ticket_type: "ticket" | "list";
    available_tickets: number;
    max_purchase_tickets: number;
    limit_per_user?: number;
    description: string;
    selling_start: string;
    selling_closing: string;
    been_sold: boolean;
    confirmed_participants: number;
    pending_participants: number;
    inavailability_message: string;
    visible: boolean;
    allowOnlineSales: boolean;
    allowManualSales: boolean;
    statusMessage: string;
    expired: boolean;
    notInitialized: boolean;
    dateValidation: boolean;
    soldout: boolean;
    sellers: TicketSeller[];
    can_sell: boolean;
    hideTax?: boolean;
    is_seller?: boolean;
    status?: string;
    allow_casadinha_edit?: boolean;
    participant_count?: number;
    casadinha_name?: string;
    event_section_id?: number;
    cannotAdd?: boolean;
};
export declare type TicketSeller = {
    id: number;
    userId: number;
    name: string;
    email: string;
    avatar?: string;
    thumbxl?: string;
    thumblg?: string;
    thumbmd?: string;
    thumbsm?: string;
    thumbxs?: string;
};
export declare type Discount = {
    id: number;
    code: string;
    value: number;
    quantity: number;
    start_date: string;
    closing_date: string;
    status: number;
    qtd_used: number;
    message: string;
};
export declare type SectionReservation = {
    id: number;
    identifier: string;
    status: string;
    reserved: boolean;
    user?: User;
    ongoing?: boolean;
    selected?: boolean;
    purchase?: Purchase;
};
export declare type Address = {
    id: number;
    street: string;
    zip: string;
    number: string;
    district: string;
    complement: string;
    city: string;
    stateCode: string;
    full_address?: string;
};
export declare type Cart = {
    items: {
        ticket: Ticket;
        qtd: number;
        reservations: SectionReservation[];
    }[];
    discount?: Discount;
};
export declare type ProductCart = {
    items: {
        id: number;
        qtd: number;
        price: number;
    }[];
};
export declare type PagseguroSession = {
    session: {
        id: string;
    };
};
export declare type Installments = {
    quantity: number;
    installmentAmount: number;
    totalAmount: number;
    interestFree: boolean;
    cardProvider?: string;
};
export declare class Payment {
    type: PaymentType;
    credit: Credit;
    constructor(type?: PaymentType, credit?: Credit);
}
export declare enum PaymentType {
    creditCard = "creditCard",
    boleto = "boleto"
}
export declare class Credit {
    name: string;
    birthdate: string;
    cpf: string;
    phone: string;
    number: string;
    validity: string;
    cvv: string;
    brand: string;
    installments: number;
    address: Address;
    constructor(name: string, birthdate: string, cpf: string, phone: string, number: string, validity: string, cvv: string, brand: string, installments: number, address?: Address);
}
export declare type OutgoOrigin = "APP_OUTGO" | "MODERNINHA" | "SITE_OUTGO";
export declare class OutPayConfiguration {
    serverUrl: string;
    pagseguroUrl: string;
    mercadopagoKey: string;
    server: Server;
    data?: {
        moderninha: boolean;
    };
}
export interface Server {
    talkToServer<T>(method: string, params?: any, options?: RequestOptions): Promise<T>;
    reportError(error: any, description: string, extra_data: any[]): Promise<any>;
}
export declare type RequestOptions = {
    type?: "GET" | "POST" | "PUT" | "DELETE";
    timeout?: number;
    toast?: boolean;
    customRequest?: string;
    retries?: number;
    nAttempts?: number;
    important?: boolean;
    uniqueTaskID?: string;
};
export declare type OttoAlertMessage = {
    text: string;
    image: OttoImg;
    duration?: number;
    btn?: string | OttoAlertButton | (string | OttoAlertButton)[];
};
export declare type OttoImg = "atento" | "feliz" | "legal" | "triste";
export interface OttoAlertButton {
    text: string;
    action: () => Promise<any>;
}
