(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common/http')) :
    typeof define === 'function' && define.amd ? define('@outgo/out-pay', ['exports', '@angular/core', '@angular/common/http'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory((global.outgo = global.outgo || {}, global.outgo['out-pay'] = {}), global.ng.core, global.ng.common.http));
}(this, (function (exports, i0, i1) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b)
                if (Object.prototype.hasOwnProperty.call(b, p))
                    d[p] = b[p]; };
        return extendStatics(d, b);
    };
    function __extends(d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }
    var __assign = function () {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p))
                        t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };
    function __rest(s, e) {
        var t = {};
        for (var p in s)
            if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
                t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }
    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function")
            r = Reflect.decorate(decorators, target, key, desc);
        else
            for (var i = decorators.length - 1; i >= 0; i--)
                if (d = decorators[i])
                    r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }
    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); };
    }
    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function")
            return Reflect.metadata(metadataKey, metadataValue);
    }
    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try {
                step(generator.next(value));
            }
            catch (e) {
                reject(e);
            } }
            function rejected(value) { try {
                step(generator["throw"](value));
            }
            catch (e) {
                reject(e);
            } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }
    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function () { if (t[0] & 1)
                throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f)
                throw new TypeError("Generator is already executing.");
            while (_)
                try {
                    if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done)
                        return t;
                    if (y = 0, t)
                        op = [op[0] & 2, t.value];
                    switch (op[0]) {
                        case 0:
                        case 1:
                            t = op;
                            break;
                        case 4:
                            _.label++;
                            return { value: op[1], done: false };
                        case 5:
                            _.label++;
                            y = op[1];
                            op = [0];
                            continue;
                        case 7:
                            op = _.ops.pop();
                            _.trys.pop();
                            continue;
                        default:
                            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                                _ = 0;
                                continue;
                            }
                            if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) {
                                _.label = op[1];
                                break;
                            }
                            if (op[0] === 6 && _.label < t[1]) {
                                _.label = t[1];
                                t = op;
                                break;
                            }
                            if (t && _.label < t[2]) {
                                _.label = t[2];
                                _.ops.push(op);
                                break;
                            }
                            if (t[2])
                                _.ops.pop();
                            _.trys.pop();
                            continue;
                    }
                    op = body.call(thisArg, _);
                }
                catch (e) {
                    op = [6, e];
                    y = 0;
                }
                finally {
                    f = t = 0;
                }
            if (op[0] & 5)
                throw op[1];
            return { value: op[0] ? op[1] : void 0, done: true };
        }
    }
    var __createBinding = Object.create ? (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        Object.defineProperty(o, k2, { enumerable: true, get: function () { return m[k]; } });
    }) : (function (o, m, k, k2) {
        if (k2 === undefined)
            k2 = k;
        o[k2] = m[k];
    });
    function __exportStar(m, o) {
        for (var p in m)
            if (p !== "default" && !Object.prototype.hasOwnProperty.call(o, p))
                __createBinding(o, m, p);
    }
    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m)
            return m.call(o);
        if (o && typeof o.length === "number")
            return {
                next: function () {
                    if (o && i >= o.length)
                        o = void 0;
                    return { value: o && o[i++], done: !o };
                }
            };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }
    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m)
            return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done)
                ar.push(r.value);
        }
        catch (error) {
            e = { error: error };
        }
        finally {
            try {
                if (r && !r.done && (m = i["return"]))
                    m.call(i);
            }
            finally {
                if (e)
                    throw e.error;
            }
        }
        return ar;
    }
    /** @deprecated */
    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }
    /** @deprecated */
    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++)
            s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    }
    function __spreadArray(to, from) {
        for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
            to[j] = from[i];
        return to;
    }
    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }
    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n])
            i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try {
            step(g[n](v));
        }
        catch (e) {
            settle(q[0][3], e);
        } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length)
            resume(q[0][0], q[0][1]); }
    }
    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }
    function __asyncValues(o) {
        if (!Symbol.asyncIterator)
            throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function (v) { resolve({ value: v, done: d }); }, reject); }
    }
    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) {
            Object.defineProperty(cooked, "raw", { value: raw });
        }
        else {
            cooked.raw = raw;
        }
        return cooked;
    }
    ;
    var __setModuleDefault = Object.create ? (function (o, v) {
        Object.defineProperty(o, "default", { enumerable: true, value: v });
    }) : function (o, v) {
        o["default"] = v;
    };
    function __importStar(mod) {
        if (mod && mod.__esModule)
            return mod;
        var result = {};
        if (mod != null)
            for (var k in mod)
                if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k))
                    __createBinding(result, mod, k);
        __setModuleDefault(result, mod);
        return result;
    }
    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }
    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }
    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    (function (PaymentSaleMethod) {
        PaymentSaleMethod[PaymentSaleMethod["FREE"] = -1] = "FREE";
        PaymentSaleMethod[PaymentSaleMethod["MONEY"] = 0] = "MONEY";
        PaymentSaleMethod[PaymentSaleMethod["CREDIT"] = 1] = "CREDIT";
        PaymentSaleMethod[PaymentSaleMethod["DEBIT"] = 2] = "DEBIT";
        PaymentSaleMethod[PaymentSaleMethod["TRANSFER"] = 3] = "TRANSFER";
    })(exports.PaymentSaleMethod || (exports.PaymentSaleMethod = {}));
    var Payment = /** @class */ (function () {
        function Payment(type, credit) {
            if (type === void 0) { type = exports.PaymentType.creditCard; }
            if (credit === void 0) { credit = new Credit("", "", "", "", "", "", "", "", 0); }
            this.type = type;
            this.credit = credit;
        }
        return Payment;
    }());
    (function (PaymentType) {
        PaymentType["creditCard"] = "creditCard";
        PaymentType["boleto"] = "boleto";
    })(exports.PaymentType || (exports.PaymentType = {}));
    var Credit = /** @class */ (function () {
        function Credit(name, birthdate, cpf, phone, number, validity, cvv, brand, installments, address) {
            if (address === void 0) { address = {
                id: 0,
                city: "",
                complement: "",
                district: "",
                number: "",
                stateCode: "",
                street: "",
                zip: "",
            }; }
            this.name = name;
            this.birthdate = birthdate;
            this.cpf = cpf;
            this.phone = phone;
            this.number = number;
            this.validity = validity;
            this.cvv = cvv;
            this.brand = brand;
            this.installments = installments;
            this.address = address;
        }
        return Credit;
    }());
    var OutPayConfiguration = /** @class */ (function () {
        function OutPayConfiguration() {
            this.serverUrl = '';
            this.pagseguroUrl = '';
            this.mercadopagoKey = '';
            this.server = { talkToServer: function () { return Promise.reject(); }, reportError: function () { return Promise.reject(); } };
        }
        return OutPayConfiguration;
    }());

    /// Funções auxiliares
    /**
     * Função que arredonda o número em duas casas decimais.
     * @param number O número a ser formatado
     * @param separator O separador a ser utilizado. Usa ',' por padrão.
     * @returns Uma string correspondente ao número fornecido, com o separador desejado.
     */
    function valueRound(number, separator) {
        if (!separator) {
            separator = ',';
        }
        if (number === 0) {
            return '0' + separator + '00';
        }
        var num = Math.round(number * 100) + '';
        num = num.substr(0, num.length - 2) + separator + num.substr(num.length - 2, 2);
        return num;
    }

    var OutPayService = /** @class */ (function () {
        function OutPayService(http) {
            this.http = http;
            // ENVIRONMENT
            this.serverUrl = "";
            this.pagseguroUrl = "";
            this.mercadopagoKey = "";
            this.moderninha = false;
            this.origin = "APP_OUTGO";
            this.cart = { items: [] };
            this.payment = new Payment();
            this.installments = [];
            this.isSelling = false;
            this.productsSale = false;
            this.productCart = { items: [] };
            this.inscriptionParticipantData = [];
            this.selectedSaleInstallment = 0;
            //Valor da compra/venda
            this._value = 0;
            // PAGSEGURO
            this.pagseguroLoad = false;
            this.mercadopagoLoad = false;
            this.sessionId = "";
        }
        Object.defineProperty(OutPayService.prototype, "paymentMethod", {
            get: function () { return this._paymentMethod; },
            enumerable: false,
            configurable: true
        });
        ;
        Object.defineProperty(OutPayService.prototype, "paymentSaleMethod", {
            get: function () { return this._paymentSaleMethod; },
            enumerable: false,
            configurable: true
        });
        ;
        Object.defineProperty(OutPayService.prototype, "deliveryMethod", {
            get: function () { return this._deliveryMethod; },
            enumerable: false,
            configurable: true
        });
        ;
        Object.defineProperty(OutPayService.prototype, "grossValue", {
            get: function () { return this._grossValue || this.value; },
            enumerable: false,
            configurable: true
        });
        ;
        Object.defineProperty(OutPayService.prototype, "platform", {
            get: function () {
                var _a;
                return (_a = this.eventDetail) === null || _a === void 0 ? void 0 : _a.paymentMethod;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(OutPayService.prototype, "needsBuyerData", {
            /**
             * Verifica se é necessário pedir os dados do comprador.
             * Serve tanto para exibir a tela de form quanto pra exibir o formulário de dados do comprador
             */
            get: function () {
                return !this.isSelling && !(this.user && this.user.email && this.user.phone && this.user.cpf && this.user.birthdate);
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(OutPayService.prototype, "value", {
            get: function () {
                return this._value;
            },
            enumerable: false,
            configurable: true
        });
        OutPayService.prototype.setValue = function () {
            var _this = this;
            if (!this.cart.items.length && !this.productCart.items.length)
                throw new OutPayError("O carrinho ainda não foi carregado", exports.OutPayErrorCode.GENERAL_ERROR);
            var value = this.cart.items.map(function (item) {
                var itemValue;
                if (_this.isSelling) {
                    if (_this.moderninha) {
                        itemValue = ((item.ticket.tax_payed_by_company ? item.ticket.final_price : item.ticket.price) *
                            (_this.paymentSaleMethod == exports.PaymentSaleMethod.CREDIT ? (1 + _this.eventDetail.tax_credit_buyer) : (_this.paymentSaleMethod == exports.PaymentSaleMethod.DEBIT ? (1 + _this.eventDetail.tax_debit_buyer) : 1)));
                    }
                    else {
                        if (item.ticket.is_seller && !item.ticket.tax_payed_by_company) {
                            itemValue = item.ticket.price;
                        }
                        else {
                            itemValue = item.ticket.final_price;
                        }
                    }
                }
                else {
                    itemValue = item.ticket.final_price;
                }
                return itemValue * item.qtd;
            }).reduce(function (total, current) { return total + current; }, 0);
            this.productCart.items.forEach(function (productItem) {
                var productValue = (productItem.price * productItem.qtd);
                if (_this.eventDetail && _this.user) {
                    var teamMember = _this.eventDetail.team.find(function (tm) { var _a; return tm.userId == ((_a = _this.user) === null || _a === void 0 ? void 0 : _a.id); });
                    value += productValue + (productValue * ((teamMember === null || teamMember === void 0 ? void 0 : teamMember.commission) ? teamMember.commission : 0));
                }
                else {
                    value += productValue;
                }
            });
            if (this.cart.discount)
                this._value = value - value * (this.cart.discount.value / 100);
            else
                this._value = value;
        };
        Object.defineProperty(OutPayService.prototype, "discount", {
            get: function () {
                return this.cart.discount;
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(OutPayService.prototype, "boletoPermit", {
            get: function () {
                return this.eventDetail ? this.eventDetail.hasBoleto : false;
            },
            enumerable: false,
            configurable: true
        });
        OutPayService.prototype.setup = function (serverUrl, pagseguroUrl, mercadopagoKey, origin, server, data) {
            if (data === void 0) { data = {}; }
            this.serverUrl = serverUrl;
            this.pagseguroUrl = pagseguroUrl;
            this.mercadopagoKey = mercadopagoKey;
            this.server = server;
            this.origin = origin;
            if (data.moderninha)
                this.moderninha = data.moderninha;
        };
        /**
         * Limpar dados da compra/venda.
         * Deve ser chamado no começo de cada compra/venda pra evitar que uma compra anterior influencie compras futuras.
         */
        OutPayService.prototype.cleanCache = function (cleanPurchaseCache) {
            if (cleanPurchaseCache === void 0) { cleanPurchaseCache = true; }
            if (cleanPurchaseCache) {
                this.eventDetail = undefined;
                this.purchase = undefined;
            }
            this.user = undefined;
            this.cart = {
                items: []
            };
            this.productCart = {
                items: []
            };
            this._value = 0;
            this.payment = new Payment();
            this.ticketData = undefined;
            this.installments = [];
            this.isSelling = false;
            this.productsSale = false;
            this.inscriptionParticipantData = [];
            this.selectedSaleInstallment = -1;
            this.purchaser = undefined;
            this._paymentMethod = undefined;
            this._paymentSaleMethod = undefined;
            this._deliveryMethod = undefined;
            this._grossValue = undefined;
        };
        /**
         * Sender hash é utilizado nas compras de pagseguro pra identificar o comprador.
         */
        OutPayService.prototype.initSenderHash = function () {
            this.senderHash = new Promise(function (ok) {
                PagSeguroDirectPayment.onSenderHashReady(function (response) {
                    if (response.status == 'error') {
                        console.log(response);
                        return false;
                    }
                    ok(response.senderHash); //Hash estará disponível nesta variável.
                    return true;
                });
            });
        };
        OutPayService.prototype.startPagseguroSession = function () {
            return this.http.get(this.serverUrl + 'api/getIdSession');
        };
        //forma robusta de carregar pagseguro, garante carregar script e depois carregar sessionID
        OutPayService.prototype.pagseguroResolve = function () {
            var _this = this;
            if (this.pagseguroLoad) {
                return new Promise(function (ok) { return ok(true); });
            }
            else {
                //Verifica se já foi iniciado o carregamento
                if (!this.pagseguroPromise) {
                    this.pagseguroPromise = new Promise(function (ok, reject) {
                        var script = document.createElement('script');
                        script.addEventListener('load', function () {
                            _this.startPagseguroSession().subscribe(function (result) {
                                if (result.session) {
                                    _this.sessionId = result.session.id;
                                    PagSeguroDirectPayment.setSessionId(result.session.id);
                                    _this.initSenderHash();
                                    //this.senderHash = PagSeguroDirectPayment.getSenderHash();
                                    //Terminado carregamento, ajustando variáveis
                                    _this.pagseguroLoad = true;
                                    _this.pagseguroPromise = undefined;
                                    ok(true);
                                }
                                else {
                                    reject("O PagSeguro não está disponível. Tente novamente mais tarde.");
                                }
                            });
                        });
                        script.src = _this.pagseguroUrl;
                        document.head.appendChild(script);
                    });
                }
                return this.pagseguroPromise;
            }
        };
        // Carregamento do mercadopago
        OutPayService.prototype.mercadopagoResolve = function () {
            var _this = this;
            if (this.mercadopagoLoad) {
                return new Promise(function (ok) { return ok(true); });
            }
            else {
                //Verifica se já foi iniciado o carregamento
                if (!this.mercadopagoPromise) {
                    this.mercadopagoPromise = new Promise(function (ok) {
                        var script = document.createElement('script');
                        script.addEventListener('load', function () {
                            Mercadopago.setPublishableKey(_this.mercadopagoKey);
                            //Terminado carregamento, ajustando variáveis
                            _this.mercadopagoLoad = true;
                            _this.mercadopagoPromise = undefined;
                            ok(true);
                        });
                        script.src = "https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js";
                        document.head.appendChild(script);
                    });
                }
                return this.mercadopagoPromise;
            }
        };
        // Busca valor das parcelas. Deve carregar primeiro o pagseguro/mercadopago
        OutPayService.prototype.getInstallments = function (cardNumber) {
            var _this = this;
            if (cardNumber) {
                cardNumber = cardNumber.replace(/ /g, "");
                // console.log("PEGANDO INSTALLMENTS", cardNumber);
                return new Promise(function (success, error) {
                    var amount = _this.value;
                    if (_this.platform == "pagseguro" && _this.pagseguroLoad) {
                        _this.getBrand(cardNumber).then(function (brand) {
                            _this.payment.credit.brand = brand;
                            var pagseguroParams = {
                                amount: amount,
                                brand: brand,
                                success: function (response) {
                                    _this.installments = response.installments[brand];
                                    success(response.installments[brand]);
                                },
                                error: function (response) {
                                    error(response);
                                    console.error("Falhou no getInstallments", response);
                                },
                            };
                            if (_this.eventDetail) {
                                if (_this.moderninha) {
                                    if (_this.eventDetail.physicalInstallmentsWithoutInterest > 1) {
                                        pagseguroParams["maxInstallmentNoInterest"] = _this.eventDetail.physicalInstallmentsWithoutInterest;
                                    }
                                }
                                else {
                                    if (_this.eventDetail.installmentsWithoutInterest > 1) {
                                        pagseguroParams["maxInstallmentNoInterest"] = _this.eventDetail.installmentsWithoutInterest;
                                    }
                                }
                            }
                            PagSeguroDirectPayment.getInstallments(pagseguroParams);
                        }).catch(function (fail) {
                            error(fail);
                            console.log("falhou o brand", fail);
                        });
                    }
                    else if (_this.platform == "mercadopago" && _this.mercadopagoLoad) {
                        var bin = cardNumber.substring(0, 6);
                        Mercadopago.getInstallments({
                            bin: bin,
                            amount: amount
                        }, function (status, response) {
                            if (status >= 200 && status < 300) {
                                _this.installments = response[0].payer_costs.map(function (data) {
                                    return {
                                        quantity: data.installments,
                                        installmentAmount: data.installment_amount,
                                        totalAmount: data.total_amount,
                                        interestFree: data.installment_rate == 0,
                                        cardProvider: response[0].payment_method_id,
                                    };
                                });
                                success(_this.installments);
                            }
                            else {
                                error(response);
                                console.log("falhou as parcelas mercadopago");
                            }
                        });
                    }
                    else {
                        error('Biblioteca de pagamento não carregada');
                        console.log("sem " + _this.platform);
                    }
                });
            }
            else {
                //Obter parcelas pagseguro sem informação do cartão.
                return new Promise(function (success, error) {
                    var pagseguroParams = {
                        amount: _this.value,
                        success: function (response) {
                            //O pagseguro responde com vários tipos de cartões. Verifiquei que os mais comuns respondem com a mesma quantidade de parcelas.
                            //Uma abordagem mais segura seria sempre usar a resposta com o menor número de parcelas.
                            _this.installments = response.installments["visa"];
                            success(response.installments["visa"]);
                        },
                        error: function (response) {
                            error(response);
                            console.error("Falhou no getInstallments sem cartão", response);
                        }
                    };
                    if (_this.eventDetail) {
                        if (_this.moderninha) {
                            if (_this.eventDetail.physicalInstallmentsWithoutInterest > 1) {
                                pagseguroParams["maxInstallmentNoInterest"] = _this.eventDetail.physicalInstallmentsWithoutInterest;
                            }
                        }
                        else {
                            if (_this.eventDetail.installmentsWithoutInterest > 1) {
                                pagseguroParams["maxInstallmentNoInterest"] = _this.eventDetail.installmentsWithoutInterest;
                            }
                        }
                    }
                    PagSeguroDirectPayment.getInstallments(pagseguroParams);
                });
            }
        };
        //pegar a bandeira do cartão (pagseguro)
        OutPayService.prototype.getBrand = function (cardNumber) {
            return new Promise(function (success, error) {
                PagSeguroDirectPayment.getBrand({
                    cardBin: cardNumber,
                    success: function (response) {
                        success(response.brand.name);
                    },
                    error: function (response) {
                        console.error(response);
                        error(response);
                    }
                });
            });
        };
        /**
         * Validação dos dados do usuário.
         */
        OutPayService.prototype.personalDataValidate = function () {
            if (!this.user)
                return false;
            var actualPersonalValidate = true;
            if (this.user.email.includes("@facebook.com") || !this.user.email) {
                actualPersonalValidate = false;
                this.user.email = ""; //Limpando email
            }
            if (!this.user.phone) {
                actualPersonalValidate = false;
            }
            if (!this.user.cpf) {
                actualPersonalValidate = false;
            }
            if (!this.user.birthdate) {
                actualPersonalValidate = false;
            }
            return actualPersonalValidate;
        };
        /**
         * Função que carrega os dados do evento antes de iniciar a venda.
         * @param eventId O ID do evento.
         * @param userId O ID do usuário. Os dados do usuário são baixados apenas se o parâmetro for fornecido.
         * @throws GENERAL_ERROR se a função for chamada antes de salvar os dados do carrinho de compras.
         */
        OutPayService.prototype.eventPurchaseData = function (eventId, userId) {
            return __awaiter(this, void 0, void 0, function () {
                var eventDetail;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            if (!userId && this.user)
                                userId = this.user.id;
                            return [4 /*yield*/, this.getEventDetail(eventId)];
                        case 1:
                            eventDetail = _c.sent();
                            this.setValue();
                            if (!userId) return [3 /*break*/, 3];
                            return [4 /*yield*/, this.getUserDetail(eventDetail.event.clientId, userId)];
                        case 2:
                            _c.sent();
                            _c.label = 3;
                        case 3: return [2 /*return*/, true];
                    }
                });
            });
        };
        OutPayService.prototype.getEventDetail = function (eventId) {
            return __awaiter(this, void 0, void 0, function () {
                var eventDetail;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0: return [4 /*yield*/, this.server.talkToServer("event_detail/" + encodeURIComponent(eventId), { ignoreVisit: true })];
                        case 1:
                            eventDetail = _c.sent();
                            this.eventDetail = eventDetail;
                            return [2 /*return*/, eventDetail];
                    }
                });
            });
        };
        OutPayService.prototype.getUserDetail = function (companyId, userId) {
            return __awaiter(this, void 0, void 0, function () {
                var user;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0: return [4 /*yield*/, this.server.talkToServer('user_data', { company_id: companyId })];
                        case 1:
                            user = _c.sent();
                            this.user = user;
                            return [2 /*return*/, user];
                    }
                });
            });
        };
        /**
         * Função que obtem os parâmetros básicos para criar uma compra no servidor.
         * @returns `null` Caso o usuário não esteja registrado, e um `Object` com os dados em caso contrário.
         */
        OutPayService.prototype.getPaymentParams = function () {
            var _a;
            return __awaiter(this, void 0, void 0, function () {
                var totalPrice, totalFinalPrice, productNumber, data, description, _c, _d, item, i, participantData, i_1, formFieldId, _e, _f;
                var e_1, _g;
                return __generator(this, function (_h) {
                    switch (_h.label) {
                        case 0:
                            if (!this.user)
                                return [2 /*return*/, null];
                            totalPrice = 0;
                            totalFinalPrice = 0;
                            productNumber = 1;
                            data = {};
                            description = "";
                            try {
                                // Adicionando dados de todos os produtos
                                for (_c = __values(this.cart.items), _d = _c.next(); !_d.done; _d = _c.next()) {
                                    item = _d.value;
                                    if (item.qtd > 0) {
                                        data['itemId' + productNumber] = item.ticket.id.toString();
                                        data['itemDescription' + productNumber] = item.ticket.name;
                                        data['itemAmount' + productNumber] = valueRound(item.ticket.final_price, '.');
                                        data['itemClientAmount' + productNumber] = valueRound(item.ticket.price, '.');
                                        data['itemQuantity' + productNumber] = item.qtd.toString();
                                        data['itemReservationId' + productNumber] = item.reservations.map(function (r) { return r.id; });
                                        if (this.inscriptionParticipantData) {
                                            data['itemParticipants' + productNumber] = "";
                                            for (i = 0; i < this.inscriptionParticipantData.length; i++) {
                                                participantData = this.inscriptionParticipantData[i];
                                                data['itemParticipants' + productNumber] += "name:" + participantData.name;
                                                data['itemParticipants' + productNumber] += ",email:" + participantData.email;
                                                if (participantData.cpf)
                                                    data['itemParticipants' + productNumber] += ",cpf:" + participantData.cpf;
                                                if (participantData.phone)
                                                    data['itemParticipants' + productNumber] += ",phone:" + participantData.phone;
                                                if (participantData.birthdate)
                                                    data['itemParticipants' + productNumber] += ",birthdate:" + participantData.birthdate;
                                                for (i_1 = 0; i_1 < (participantData.extra_fields ? Object.keys(participantData.extra_fields).length : 0); i_1++) {
                                                    formFieldId = parseInt(Object.keys(participantData.extra_fields)[i_1]);
                                                    data['itemParticipants' + productNumber] += ",extra_fields/" + formFieldId + ":" + participantData.extra_fields[formFieldId];
                                                }
                                                if (i < this.inscriptionParticipantData.length - 1)
                                                    data['itemParticipants' + productNumber] += "§";
                                            }
                                        }
                                        productNumber++;
                                        totalPrice += item.ticket.price * item.qtd;
                                        totalFinalPrice += item.ticket.final_price * item.qtd;
                                        if (description != "")
                                            description += " | ";
                                        description += item.qtd.toString() + "x " + item.ticket.name;
                                    }
                                }
                            }
                            catch (e_1_1) { e_1 = { error: e_1_1 }; }
                            finally {
                                try {
                                    if (_d && !_d.done && (_g = _c.return)) _g.call(_c);
                                }
                                finally { if (e_1) throw e_1.error; }
                            }
                            data['clientId'] = this.eventDetail.event.clientId.toString();
                            _e = data;
                            _f = 'senderHash';
                            return [4 /*yield*/, this.senderHash];
                        case 1:
                            _e[_f] = _h.sent();
                            data['fullName'] = this.user.name;
                            data['name'] = this.user.name;
                            if (this.user.cpf) {
                                data['usercpf'] = this.user.cpf.replace(/\.|-/g, '');
                                data['cpf'] = this.user.cpf.replace(/\.|-/g, '');
                            }
                            else {
                                data['usercpf'] = '';
                                data['cpf'] = '';
                            }
                            data['email'] = this.user.email;
                            if (this.user.phone) {
                                data['completePhone'] = this.user.phone.replace(/[(_)-\s]/g, '');
                                data['areaCode'] = data['completePhone'].substr(0, 2);
                                data['phone'] = data['completePhone'].substring(2);
                            }
                            else {
                                data['completePhone'] = '';
                                data['areaCode'] = '';
                                data['phone'] = '';
                            }
                            data['senderBirthdate'] = this.user.birthdate;
                            data['birthday'] = data['senderBirthdate'];
                            data['userId'] = this.user.id.toString();
                            data['changeUser'] = false;
                            data['eventId'] = this.eventDetail.event.id;
                            data['purchaseId'] = (_a = this.purchase) === null || _a === void 0 ? void 0 : _a.id;
                            data['companyUserId'] = this.user.companyUserId;
                            data['totalPrice'] = valueRound(totalPrice, '.');
                            data['totalFinalPrice'] = valueRound(totalFinalPrice, '.');
                            data['eventName'] = this.eventDetail.event.name;
                            data['origin'] = this.origin;
                            if (this.discount)
                                data['discountId'] = this.discount.id;
                            if (this.eventDetail.paymentMethod == "mercadopago") {
                                data["transaction_amount"] = totalFinalPrice;
                                data["description"] = description;
                            }
                            return [2 /*return*/, data];
                    }
                });
            });
        };
        /**************************************************
        INICIA AS COMPRAS
        Chamado após escolher os ingressos e clicar em comprar
        @throws VALIDATE_EMAIL caso a compra seja paga e o usuário ainda não tiver validado seu email.
        @throws UNAUTHENTICATED caso o id de usuário não seja informado.
        @throws TICKET_UNAVAILABLE caso o ingresso solicitado não esteja mais disponível.
        @param cart O carrinho de compras, com o desconto, caso exista.
        @param eventId O ID do evento.
        @param userId O ID do usuário, caso ele esteja logado no sistema.
        @param cleanPurchaseCache Se os dados da compra devem ser limpos do cache. `true` por padrão, só deve ser sobrescrito se necessário.
        **************************************************/
        OutPayService.prototype.init = function (cart, eventId, userId, cleanPurchaseCache) {
            if (cleanPurchaseCache === void 0) { cleanPurchaseCache = true; }
            var _a, _b;
            return __awaiter(this, void 0, void 0, function () {
                var _c, _d, item, _e, _f, item, response, _g, _h, _j, _k, error_1;
                var e_2, _l, e_3, _m;
                return __generator(this, function (_o) {
                    switch (_o.label) {
                        case 0:
                            this.cleanCache(cleanPurchaseCache);
                            this.isSelling = false;
                            this.cart = cart;
                            return [4 /*yield*/, this.eventPurchaseData(eventId, userId)];
                        case 1:
                            _o.sent();
                            if (!(this.value > 0)) return [3 /*break*/, 6];
                            if (!(this.platform == "pagseguro")) return [3 /*break*/, 3];
                            return [4 /*yield*/, this.pagseguroResolve()];
                        case 2:
                            if (!(_o.sent())) {
                                throw new OutPayError("Não carregou Pagseguro");
                            }
                            return [3 /*break*/, 5];
                        case 3: return [4 /*yield*/, this.mercadopagoResolve()];
                        case 4:
                            if (!(_o.sent())) {
                                throw new OutPayError("Não carregou Mercadopago");
                            }
                            _o.label = 5;
                        case 5:
                            try {
                                for (_c = __values(this.cart.items), _d = _c.next(); !_d.done; _d = _c.next()) {
                                    item = _d.value;
                                    if (item.ticket.price == 0)
                                        item.qtd = 0;
                                }
                            }
                            catch (e_2_1) { e_2 = { error: e_2_1 }; }
                            finally {
                                try {
                                    if (_d && !_d.done && (_l = _c.return)) _l.call(_c);
                                }
                                finally { if (e_2) throw e_2.error; }
                            }
                            return [3 /*break*/, 7];
                        case 6:
                            //Compras sem valor são consideradas gratuitas e tem seus itens ajustados para valor 0
                            this._paymentMethod = "free";
                            if (!this.cart.discount || this.cart.discount.value != 100) {
                                try {
                                    for (_e = __values(this.cart.items), _f = _e.next(); !_f.done; _f = _e.next()) {
                                        item = _f.value;
                                        if (item.ticket.price > 0)
                                            item.qtd = 0;
                                    }
                                }
                                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                                finally {
                                    try {
                                        if (_f && !_f.done && (_m = _e.return)) _m.call(_e);
                                    }
                                    finally { if (e_3) throw e_3.error; }
                                }
                            }
                            _o.label = 7;
                        case 7:
                            if (!userId)
                                throw new OutPayError("Você não está logado.", exports.OutPayErrorCode.UNAUTHENTICATED);
                            _o.label = 8;
                        case 8:
                            _o.trys.push([8, 11, , 12]);
                            _h = (_g = this.server).talkToServer;
                            _j = ["doublecheck_tickets"];
                            _k = { cart: this.cart.items };
                            return [4 /*yield*/, this.getPaymentParams()];
                        case 9: return [4 /*yield*/, _h.apply(_g, _j.concat([(_k.params = _o.sent(), _k), { type: "POST", retries: 4 }]))];
                        case 10:
                            response = _o.sent();
                            if (response.error) {
                                if ("ticket_name" in response) {
                                    throw new OutPayError("Limite do ingresso " + response.ticket_name + " atingido. Selecione uma nova quantidade.", exports.OutPayErrorCode.TICKET_UNAVAILABLE, { otto: true, ottoData: { image: "atento" } });
                                }
                                if ("errorMsg" in response) {
                                    throw new OutPayError(response.errorMsg, exports.OutPayErrorCode.GENERAL_ERROR, { otto: true, ottoData: { image: "atento" } });
                                }
                                // Se não souber qual foi o erro, jogar para evitar erros silenciosos.
                                throw response;
                            }
                            else {
                                if (response.purchase)
                                    this.purchase = response.purchase;
                                if (((_a = this.eventDetail) === null || _a === void 0 ? void 0 : _a.paymentMethod) == "mercadopago") {
                                    this.ticketData = response.mercadopago;
                                }
                                else {
                                    this.ticketData = response.pagseguro;
                                }
                                if (this.value > 0 && this.personalDataValidate() && !this.eventDetail.have_participant_form) {
                                    return [2 /*return*/, { finished: false, nextStep: exports.OutPayStep.PAYMENT_METHOD, purchase: this.purchase }];
                                }
                                else {
                                    return [2 /*return*/, { finished: false, nextStep: exports.OutPayStep.PURCHASE_FORM, purchase: this.purchase }];
                                }
                            }
                            return [3 /*break*/, 12];
                        case 11:
                            error_1 = _o.sent();
                            if (error_1.status == 400) {
                                if (error_1.error == "verify_email" || error_1.error.verify_email) {
                                    //Deve recuperar os dados de compra, pois mesmo com o erro já criou a pre-reserva
                                    if (error_1.error.purchase)
                                        this.purchase = error_1.error.purchase;
                                    if (((_b = this.eventDetail) === null || _b === void 0 ? void 0 : _b.paymentMethod) == "mercadopago") {
                                        this.ticketData = error_1.error.mercadopago;
                                    }
                                    else {
                                        this.ticketData = error_1.error.pagseguro;
                                    }
                                    throw new OutPayError("Enviamos um c\u00F3digo para " + this.user.email + " para voc\u00EA validar abaixo.", exports.OutPayErrorCode.VALIDATE_EMAIL);
                                }
                                if (error_1.error.error) {
                                    throw new OutPayError(error_1.error.errorMsg, exports.OutPayErrorCode.GENERAL_ERROR);
                                }
                            }
                            else if (error_1.status == 401) {
                                throw new OutPayError("Você não está logado.", exports.OutPayErrorCode.UNAUTHENTICATED);
                            }
                            // Evitar erros silenciosos
                            throw error_1;
                        case 12: return [2 /*return*/];
                    }
                });
            });
        };
        /**************************************************
        INICIA O PROCESSO DO VENDEDOR
        Chamado após escolher os ingressos e clicar em vender
        **************************************************/
        OutPayService.prototype.sellerInit = function (cart, eventId, userId) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            this.cleanCache();
                            this.isSelling = true;
                            this.cart = cart;
                            return [4 /*yield*/, this.eventPurchaseData(eventId, userId)];
                        case 1:
                            _c.sent();
                            return [4 /*yield*/, this.nextStepSale()];
                        case 2: return [2 /*return*/, _c.sent()];
                    }
                });
            });
        };
        /**************************************************
        INICIA O PROCESSO DO VENDEDOR DE PRODUTOS
        Chamado após escolher os produtos e clicar em vender
        **************************************************/
        OutPayService.prototype.productSellerInit = function (productCart, eventDetail, user) {
            this.cleanCache();
            this.eventDetail = eventDetail;
            this.isSelling = true;
            this.productsSale = true;
            this.user = user;
            this.productCart = productCart;
            this.setValue();
            return { finished: false, nextStep: exports.OutPayStep.PAYMENT_METHOD, purchase: this.purchase };
        };
        /**
         * saveFormData
         * Chamado após preencher os dados de formulário.
         */
        OutPayService.prototype.saveFormData = function (participantData) {
            return __awaiter(this, void 0, void 0, function () {
                var _c, error_2, _d;
                return __generator(this, function (_e) {
                    switch (_e.label) {
                        case 0:
                            this.inscriptionParticipantData = participantData;
                            if (!this.isSelling) return [3 /*break*/, 9];
                            if (!(this.deliveryMethod == "digital" && this.inscriptionParticipantData.length > 0)) return [3 /*break*/, 7];
                            _e.label = 1;
                        case 1:
                            _e.trys.push([1, 3, , 7]);
                            _c = this;
                            return [4 /*yield*/, this.server.talkToServer("purchaser", { email: this.inscriptionParticipantData[0].email }, { retries: 4 })];
                        case 2:
                            _c.purchaser = _e.sent();
                            return [3 /*break*/, 7];
                        case 3:
                            error_2 = _e.sent();
                            if (!(error_2.error == "name")) return [3 /*break*/, 5];
                            _d = this;
                            return [4 /*yield*/, this.server.talkToServer("purchaser/create", {
                                    name: this.inscriptionParticipantData[0].name,
                                    email: this.inscriptionParticipantData[0].email,
                                    phone: this.inscriptionParticipantData[0].phone,
                                    cpf: this.inscriptionParticipantData[0].cpf,
                                    birthdat: this.inscriptionParticipantData[0].birthdate
                                }, {
                                    type: "POST",
                                    retries: 4,
                                })];
                        case 4:
                            _d.purchaser = _e.sent();
                            return [3 /*break*/, 6];
                        case 5: throw error_2;
                        case 6: return [3 /*break*/, 7];
                        case 7: return [4 /*yield*/, this.nextStepSale()];
                        case 8: return [2 /*return*/, _e.sent()];
                        case 9:
                            if (this.value > 0) {
                                return [2 /*return*/, { finished: false, nextStep: exports.OutPayStep.PAYMENT_METHOD, purchase: this.purchase }];
                            }
                            else {
                                return [2 /*return*/, { finished: false, nextStep: exports.OutPayStep.FINISH, purchase: this.purchase }];
                            }
                            _e.label = 10;
                        case 10: return [2 /*return*/];
                    }
                });
            });
        };
        /**
         * selectPaymentMethod
         * Chamada após selecionar método de pagamento para compras online.
         */
        OutPayService.prototype.selectPaymentMethod = function (paymentMethod) {
            this._paymentMethod = paymentMethod;
            if (paymentMethod == "credit") {
                this.payment.type = exports.PaymentType.creditCard;
            }
            if (this.paymentMethod == "credit" || this.needsBuyerData) {
                return { finished: false, nextStep: exports.OutPayStep.PAYMENT_DATA, purchase: this.purchase };
            }
            else {
                return { finished: false, nextStep: exports.OutPayStep.FINISH, purchase: this.purchase };
            }
        };
        /**
         * Chamada após selecionar o método de pagamento para vendas.
         * @param paymentSaleMethod Método de pagamento da venda.
         * @param usingExternalAppForPayment Opcional. Permite que o outpay prossiga em uma venda de cartão sem usar moderninha.
         */
        OutPayService.prototype.selectSalePaymentMethod = function (paymentSaleMethod, usingExternalAppForPayment) {
            if (usingExternalAppForPayment === void 0) { usingExternalAppForPayment = false; }
            return __awaiter(this, void 0, void 0, function () {
                var _a_1, _c;
                return __generator(this, function (_d) {
                    switch (_d.label) {
                        case 0:
                            this._paymentSaleMethod = paymentSaleMethod;
                            if (!(this.moderninha && [exports.PaymentSaleMethod.CREDIT, exports.PaymentSaleMethod.DEBIT].includes(paymentSaleMethod))) return [3 /*break*/, 8];
                            //Aplicar taxas de venda em cartão no preço da moderninha.
                            this.setValue();
                            if (!this.eventDetail.cardSalesOnApp) return [3 /*break*/, 5];
                            _d.label = 1;
                        case 1:
                            _d.trys.push([1, 3, , 4]);
                            return [4 /*yield*/, this.pagseguroResolve()];
                        case 2:
                            _d.sent();
                            return [3 /*break*/, 4];
                        case 3:
                            _a_1 = _d.sent();
                            throw new OutPayError("Erro ao iniciar pagseguro", exports.OutPayErrorCode.GENERAL_ERROR);
                        case 4: return [3 /*break*/, 8];
                        case 5:
                            if (!usingExternalAppForPayment) return [3 /*break*/, 7];
                            _c = this;
                            return [4 /*yield*/, this.createPurchase(true)];
                        case 6:
                            _c.purchase = _d.sent();
                            return [3 /*break*/, 8];
                        case 7: throw new OutPayError('Use o app "Vendas" ou outra máquina e realize o pagamento no cartão para continuar.', exports.OutPayErrorCode.UNABLE_TO_PROCEED);
                        case 8: return [4 /*yield*/, this.nextStepSale()];
                        case 9: return [2 /*return*/, _d.sent()];
                    }
                });
            });
        };
        /**
         * Chamada após selecionar a quantidade de parcelas em uma venda de cartão de crédito.
         * @param selectedInstallment O índice da quantidade de parcelas, iniciando em zero.
         */
        OutPayService.prototype.selectSaleInstallments = function (selectedInstallment) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            this.selectedSaleInstallment = selectedInstallment;
                            this._grossValue = this.installments[selectedInstallment].totalAmount;
                            return [4 /*yield*/, this.nextStepSale()];
                        case 1: return [2 /*return*/, _c.sent()];
                    }
                });
            });
        };
        /**
         * Chamada após selecionar o método de entrega da venda.
         * @param deliveryMethod O método de entrega da venda.
         */
        OutPayService.prototype.selectDeliveryMethod = function (deliveryMethod) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            this._deliveryMethod = deliveryMethod;
                            return [4 /*yield*/, this.nextStepSale()];
                        case 1: return [2 /*return*/, _c.sent()];
                    }
                });
            });
        };
        /**
         * Chamada para determinar o comprador em uma venda digital sem formulário.
         * @param email O email do comprador.
         * @param name O nome do comprador, para criar um novo usuário.
         * @throws NOT_FOUND caso seja informado apenas o email, e não exista usuário com esse email.
         */
        OutPayService.prototype.selectPurchaser = function (email, name) {
            return __awaiter(this, void 0, void 0, function () {
                var _c, _d, error_3;
                return __generator(this, function (_e) {
                    switch (_e.label) {
                        case 0:
                            _e.trys.push([0, 5, , 6]);
                            if (!!name) return [3 /*break*/, 2];
                            _c = this;
                            return [4 /*yield*/, this.server.talkToServer("purchaser", { email: email }, { retries: 4 })];
                        case 1:
                            _c.purchaser = _e.sent();
                            return [3 /*break*/, 4];
                        case 2:
                            _d = this;
                            return [4 /*yield*/, this.server.talkToServer("purchaser/create", { email: email, name: name }, {
                                    type: "POST",
                                    retries: 4,
                                })];
                        case 3:
                            _d.purchaser = _e.sent();
                            _e.label = 4;
                        case 4: return [3 /*break*/, 6];
                        case 5:
                            error_3 = _e.sent();
                            if (error_3.error == "name")
                                throw new OutPayError(error_3.error, exports.OutPayErrorCode.NOT_FOUND);
                            else
                                throw new OutPayError(error_3, exports.OutPayErrorCode.GENERAL_ERROR);
                            return [3 /*break*/, 6];
                        case 6: return [2 /*return*/, this.nextStepSale()];
                    }
                });
            });
        };
        /**
         * finishPurchase
         * Chamada após digitar os dados de pagamento.
         * @throws PURCHASE_ERROR quando ocorre um erro na compra.
         */
        OutPayService.prototype.finishPurchase = function (cardToken) {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            if (!(this.value > 0 && this.eventDetail.paymentMethod == "pagseguro" && this.paymentMethod == "credit")) return [3 /*break*/, 2];
                            return [4 /*yield*/, this.pagseguroCreditCard()];
                        case 1: return [2 /*return*/, _c.sent()];
                        case 2: return [4 /*yield*/, this.finalizePurchase(cardToken)];
                        case 3: return [2 /*return*/, _c.sent()];
                    }
                });
            });
        };
        OutPayService.prototype.finishSale = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _c;
                return __generator(this, function (_d) {
                    switch (_d.label) {
                        case 0:
                            if (!!this.purchase) return [3 /*break*/, 2];
                            _c = this;
                            return [4 /*yield*/, this.createPurchase()];
                        case 1:
                            _c.purchase = _d.sent();
                            return [3 /*break*/, 4];
                        case 2:
                            if (!(this.purchase.status < 4)) return [3 /*break*/, 4];
                            this.purchase.status = 4;
                            return [4 /*yield*/, this.server.talkToServer("purchase/" + this.purchase.id, this.purchase, {
                                    type: "PUT",
                                    retries: 4,
                                })];
                        case 3:
                            _d.sent();
                            _d.label = 4;
                        case 4: return [2 /*return*/, this.nextStepSale()];
                    }
                });
            });
        };
        //serve para tratar cartão tanto do pagseguro quanto do mercadopago
        OutPayService.prototype.pagseguroCreditCard = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                var param = {
                    cardNumber: _this.payment.credit.number.replace(/ /g, ""),
                    brand: _this.payment.credit.brand,
                    cvv: _this.payment.credit.cvv,
                    expirationMonth: _this.payment.credit.validity.substr(0, 2),
                    expirationYear: '20' + _this.payment.credit.validity.substr(3, 2),
                    error: function (error) {
                        reject(error);
                    },
                    success: function (response) {
                        _this.finalizePurchase(response.card.token).then(function (result) {
                            resolve(result);
                        }, function (error) { return reject(error); });
                    }
                };
                PagSeguroDirectPayment.createCardToken(param);
            });
        };
        OutPayService.prototype.finalizePurchase = function (cardToken) {
            var _a;
            return __awaiter(this, void 0, void 0, function () {
                var data, installments, purchase, error_4;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            if (this.user) {
                                if (!this.user.cpf)
                                    this.user.cpf = this.payment.credit.cpf;
                                if (!this.user.birthdate)
                                    this.user.birthdate = this.payment.credit.birthdate;
                                if (!this.user.phone)
                                    this.user.phone = this.payment.credit.phone;
                            }
                            return [4 /*yield*/, this.getPaymentParams()];
                        case 1:
                            data = _c.sent();
                            if (cardToken) {
                                installments = this.installments[this.payment.credit.installments];
                                data['paymentMethod'] = 'creditCard';
                                if (this.eventDetail.paymentMethod == "mercadopago") {
                                    data["payment_method_id"] = installments.cardProvider;
                                }
                                data['street'] = this.payment.credit.address.street;
                                data['number'] = this.payment.credit.address.number;
                                data['zip'] = this.payment.credit.address.zip;
                                data['district'] = this.payment.credit.address.district;
                                data['city'] = this.payment.credit.address.city;
                                data['stateCode'] = this.payment.credit.address.stateCode;
                                data['complement'] = '';
                                data['installmentsQtd'] = installments.quantity.toString();
                                data['installmentsValue'] = valueRound(installments.installmentAmount, '.');
                                data['code'] = cardToken;
                                data['cardName'] = this.payment.credit.name;
                                data['cardCpf'] = this.payment.credit.cpf;
                                data['cardPhone'] = this.payment.credit.phone;
                                data['pagamento'] = '' + installments.quantity + ',' + installments.installmentAmount + ',' + installments.totalAmount;
                            }
                            else if (this.paymentMethod == "boleto") {
                                data['paymentMethod'] = 'boleto';
                            }
                            else {
                                data['paymentMethod'] = 'free';
                            }
                            data['ticketData'] = this.ticketData;
                            this.inscriptionParticipantData = [];
                            _c.label = 2;
                        case 2:
                            _c.trys.push([2, 4, , 5]);
                            return [4 /*yield*/, this.doPayment(data)];
                        case 3:
                            purchase = _c.sent();
                            this.purchase = purchase;
                            return [2 /*return*/, { finished: true, nextStep: exports.OutPayStep.FINISH, purchase: this.purchase }];
                        case 4:
                            error_4 = _c.sent();
                            if (error_4.errorCode)
                                throw error_4;
                            throw new OutPayError(((_a = error_4.error) === null || _a === void 0 ? void 0 : _a.text) || "Houve um erro na compra", exports.OutPayErrorCode.PURCHASE_ERROR);
                        case 5: return [2 /*return*/];
                    }
                });
            });
        };
        OutPayService.prototype.doPayment = function (data) {
            var _a;
            return __awaiter(this, void 0, void 0, function () {
                var response, error_5;
                return __generator(this, function (_c) {
                    switch (_c.label) {
                        case 0:
                            _c.trys.push([0, 7, , 8]);
                            if (!(this.value == 0)) return [3 /*break*/, 2];
                            return [4 /*yield*/, this.server.talkToServer("paymentMercadopago", data, { type: "POST" })];
                        case 1:
                            response = _c.sent();
                            return [3 /*break*/, 6];
                        case 2:
                            if (!(this.eventDetail.paymentMethod == "mercadopago")) return [3 /*break*/, 4];
                            return [4 /*yield*/, this.http.post(this.serverUrl + 'api/paymentMercadopago', data).toPromise()];
                        case 3:
                            response = _c.sent();
                            return [3 /*break*/, 6];
                        case 4: return [4 /*yield*/, this.http.post(this.serverUrl + 'api/payment', data).toPromise()];
                        case 5:
                            response = _c.sent();
                            _c.label = 6;
                        case 6: return [3 /*break*/, 8];
                        case 7:
                            error_5 = _c.sent();
                            throw new OutPayError(((_a = error_5.error) === null || _a === void 0 ? void 0 : _a.text) || "Houve um erro na compra", exports.OutPayErrorCode.PURCHASE_ERROR);
                        case 8:
                            if (response.error) {
                                throw new OutPayError(response.errorMsg, exports.OutPayErrorCode.PURCHASE_ERROR);
                            }
                            return [2 /*return*/, response.purchase];
                    }
                });
            });
        };
        OutPayService.prototype.createPurchase = function (unpaidPurchase) {
            if (unpaidPurchase === void 0) { unpaidPurchase = false; }
            return __awaiter(this, void 0, void 0, function () {
                var data, ticketList_1, reservationList_1;
                return __generator(this, function (_c) {
                    data = { purchaser_id: this.purchaser ? this.purchaser.id : null, payment_method: this.paymentSaleMethod };
                    if (this.cart.discount)
                        data.discount_id = this.cart.discount.id;
                    if (this.cart) {
                        ticketList_1 = {};
                        reservationList_1 = {};
                        this.cart.items.forEach(function (item) {
                            ticketList_1[item.ticket.id] = item.qtd;
                            if (item.reservations.length > 0)
                                reservationList_1[item.ticket.id] = item.reservations.map(function (r) { return r.id; });
                        });
                        data.cart = ticketList_1;
                        if (Object.getOwnPropertyNames(reservationList_1).length > 0)
                            data.cart_reservation = reservationList_1;
                    }
                    if (this.productCart) {
                        data.product_cart = this.productCart.items;
                    }
                    if (this.inscriptionParticipantData)
                        data.participant_data = this.inscriptionParticipantData;
                    if (this.deliveryMethod)
                        data.purchase_type = this.deliveryMethod;
                    if (unpaidPurchase)
                        data.unpaid_purchase = unpaidPurchase;
                    data.is_selling = this.isSelling;
                    if (this.moderninha) {
                        data.moderninha = true;
                    }
                    return [2 /*return*/, this.server.talkToServer("purchase/create", data, {
                            type: "POST",
                            retries: 4,
                        })];
                });
            });
        };
        /**
         * Função que analísa a compra, e retorna o próximo input necessário pelo usuário.
         * @returns Um objeto `OutPayResult`, com o próximo passo e objeto da compra.
         */
        OutPayService.prototype.nextStepSale = function () {
            return __awaiter(this, void 0, void 0, function () {
                var result, _c, _d;
                return __generator(this, function (_e) {
                    switch (_e.label) {
                        case 0:
                            result = { finished: false, nextStep: -1, purchase: this.purchase };
                            //Se a compra for paga, verificar método de pagamento e parcelamento.
                            if (this.value > 0) {
                                if (this.paymentSaleMethod == undefined) {
                                    result.nextStep = exports.OutPayStep.PAYMENT_METHOD;
                                    return [2 /*return*/, result];
                                }
                                else {
                                    if (this.moderninha && this.paymentSaleMethod == exports.PaymentSaleMethod.CREDIT) {
                                        if (this.selectedSaleInstallment < 0) {
                                            result.nextStep = exports.OutPayStep.INSTALLMENTS;
                                        }
                                    }
                                }
                            }
                            else {
                                if (this.paymentSaleMethod == undefined) {
                                    this._paymentSaleMethod = exports.PaymentSaleMethod.FREE;
                                }
                            }
                            //Depois, verificar o método de entrega. Caso só seja possível um, este é selecionado.
                            if (result.nextStep < 0) {
                                if (!this.deliveryMethod) {
                                    if (this.moderninha && (this.productsSale || this.value == 0)) {
                                        this._deliveryMethod = "physical";
                                    }
                                    else {
                                        if (this.moderninha && this.eventDetail.allow_physical_sales && this.eventDetail.allow_digital_sales) {
                                            result.nextStep = exports.OutPayStep.DELIVERY_METHOD;
                                        }
                                        else if (this.moderninha && this.eventDetail.allow_physical_sales) {
                                            this._deliveryMethod = "physical";
                                        }
                                        else if (this.eventDetail.allow_digital_sales) {
                                            this._deliveryMethod = "digital";
                                        }
                                        else {
                                            throw new OutPayError("Houve um problema para continuar a venda. Fale com o administrador do evento.", exports.OutPayErrorCode.PURCHASE_ERROR, { otto: true, ottoData: { image: "triste", duration: 3000 } });
                                        }
                                    }
                                }
                            }
                            //O próximo passo é verificar dados dos compradores, via formulário ou tela do comprador.
                            //Vendas de produtos não precisam de informação sobre comprador.
                            if (result.nextStep < 0 && !this.productsSale) {
                                if (this.eventDetail.have_participant_form || this.value == 0) {
                                    if (!this.inscriptionParticipantData.length) {
                                        result.nextStep = exports.OutPayStep.PURCHASE_FORM;
                                    }
                                }
                                else if (this.deliveryMethod == "digital" && !this.eventDetail.have_participant_form) {
                                    if (!this.purchaser) {
                                        result.nextStep = exports.OutPayStep.PURCHASER;
                                    }
                                }
                            }
                            if (!(result.nextStep < 0)) return [3 /*break*/, 2];
                            if (!(this.moderninha && this.paymentSaleMethod && [exports.PaymentSaleMethod.CREDIT, exports.PaymentSaleMethod.DEBIT].includes(this.paymentSaleMethod))) return [3 /*break*/, 2];
                            if (!!this.purchase) return [3 /*break*/, 2];
                            _c = this;
                            return [4 /*yield*/, this.createPurchase(true)];
                        case 1:
                            _c.purchase = _e.sent();
                            result.purchase = this.purchase;
                            result.nextStep = exports.OutPayStep.PLUGPAG;
                            _e.label = 2;
                        case 2:
                            if (!(result.nextStep < 0)) return [3 /*break*/, 5];
                            if (!(this.deliveryMethod == "physical")) return [3 /*break*/, 5];
                            if (!!this.purchase) return [3 /*break*/, 4];
                            _d = this;
                            return [4 /*yield*/, this.createPurchase(true)];
                        case 3:
                            _d.purchase = _e.sent();
                            result.purchase = this.purchase;
                            _e.label = 4;
                        case 4:
                            if (this.purchase.status < 4) {
                                result.nextStep = exports.OutPayStep.PRINT;
                            }
                            else {
                                result.nextStep = exports.OutPayStep.FINISH;
                            }
                            _e.label = 5;
                        case 5:
                            if (!(result.nextStep < 0)) return [3 /*break*/, 8];
                            if (!(!this.purchase || this.purchase.status < 4)) return [3 /*break*/, 7];
                            return [4 /*yield*/, this.finishSale()];
                        case 6:
                            _e.sent();
                            _e.label = 7;
                        case 7:
                            result.purchase = this.purchase;
                            result.finished = true;
                            result.nextStep = exports.OutPayStep.FINISH;
                            _e.label = 8;
                        case 8: return [2 /*return*/, result];
                    }
                });
            });
        };
        return OutPayService;
    }());
    OutPayService.ɵfac = function OutPayService_Factory(t) { return new (t || OutPayService)(i0.ɵɵinject(i1.HttpClient)); };
    OutPayService.ɵprov = i0.ɵɵdefineInjectable({ token: OutPayService, factory: OutPayService.ɵfac, providedIn: 'root' });
    (function () {
        (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OutPayService, [{
                type: i0.Injectable,
                args: [{ providedIn: 'root' }]
            }], function () { return [{ type: i1.HttpClient }]; }, null);
    })();
    (function (OutPayStep) {
        /** Seleção de forma de pagamento */
        OutPayStep[OutPayStep["PAYMENT_METHOD"] = 0] = "PAYMENT_METHOD";
        /** Formulário de dados do comprador e dos participantes */
        OutPayStep[OutPayStep["PURCHASE_FORM"] = 1] = "PURCHASE_FORM";
        /** Impressão do ingresso, nos casos onde o pagamento não ocorre via PlugPag (ie. Dinheiro) */
        OutPayStep[OutPayStep["PRINT"] = 2] = "PRINT";
        /** Finalizar processo de compra */
        OutPayStep[OutPayStep["FINISH"] = 3] = "FINISH";
        /** Formulário de dados do pagamento (cartão) */
        OutPayStep[OutPayStep["PAYMENT_DATA"] = 4] = "PAYMENT_DATA";
        /** Seleção de forma de recebimento */
        OutPayStep[OutPayStep["DELIVERY_METHOD"] = 5] = "DELIVERY_METHOD";
        /** Seleção do parcelamento do pagamento */
        OutPayStep[OutPayStep["INSTALLMENTS"] = 6] = "INSTALLMENTS";
        /** Pagamento via PlugPag */
        OutPayStep[OutPayStep["PLUGPAG"] = 7] = "PLUGPAG";
        /** Identificação do comprador para vendas digitais sem formulário */
        OutPayStep[OutPayStep["PURCHASER"] = 8] = "PURCHASER";
    })(exports.OutPayStep || (exports.OutPayStep = {}));
    var OutPayError = /** @class */ (function () {
        function OutPayError(msg, errorCode, data) {
            if (errorCode === void 0) { errorCode = exports.OutPayErrorCode.GENERAL_ERROR; }
            if (data === void 0) { data = {}; }
            this.msg = msg;
            this.errorCode = errorCode;
            this.data = data;
        }
        ;
        Object.defineProperty(OutPayError.prototype, "ottoAlert", {
            get: function () {
                if (this.data.otto) {
                    return {
                        text: this.msg,
                        image: this.data.ottoData.image,
                        duration: this.data.ottoData.duration,
                    };
                }
                else
                    throw new Error("Não há mensagem do otto neste erro");
            },
            enumerable: false,
            configurable: true
        });
        return OutPayError;
    }());
    (function (OutPayErrorCode) {
        /** Erro genérico */
        OutPayErrorCode[OutPayErrorCode["GENERAL_ERROR"] = 1] = "GENERAL_ERROR";
        /** O usuário precisa validar o email para prosseguir */
        OutPayErrorCode[OutPayErrorCode["VALIDATE_EMAIL"] = 2] = "VALIDATE_EMAIL";
        /** O ingresso solicitado já está esgotado */
        OutPayErrorCode[OutPayErrorCode["TICKET_UNAVAILABLE"] = 3] = "TICKET_UNAVAILABLE";
        /** Houve um erro ao processar a compra */
        OutPayErrorCode[OutPayErrorCode["PURCHASE_ERROR"] = 4] = "PURCHASE_ERROR";
        /** O usuário não está logado no sistema */
        OutPayErrorCode[OutPayErrorCode["UNAUTHENTICATED"] = 5] = "UNAUTHENTICATED";
        /** Não é possível executar a ação, um caminho alternativo deve ser buscado */
        OutPayErrorCode[OutPayErrorCode["UNABLE_TO_PROCEED"] = 6] = "UNABLE_TO_PROCEED";
        /** Não foi encontrado um resultado para os dados fornecidos */
        OutPayErrorCode[OutPayErrorCode["NOT_FOUND"] = 7] = "NOT_FOUND";
    })(exports.OutPayErrorCode || (exports.OutPayErrorCode = {}));

    var OutPayModule = /** @class */ (function () {
        function OutPayModule() {
        }
        return OutPayModule;
    }());
    OutPayModule.ɵmod = i0.ɵɵdefineNgModule({ type: OutPayModule });
    OutPayModule.ɵinj = i0.ɵɵdefineInjector({ factory: function OutPayModule_Factory(t) { return new (t || OutPayModule)(); }, providers: [
            OutPayService
        ], imports: [[
                i1.HttpClientModule,
            ]] });
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(OutPayModule, { imports: [i1.HttpClientModule] }); })();
    (function () {
        (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OutPayModule, [{
                type: i0.NgModule,
                args: [{
                        imports: [
                            i1.HttpClientModule,
                        ],
                        providers: [
                            OutPayService
                        ],
                    }]
            }], null, null);
    })();

    /*
     * Public API Surface of out-pay
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.Credit = Credit;
    exports.OutPayConfiguration = OutPayConfiguration;
    exports.OutPayError = OutPayError;
    exports.OutPayModule = OutPayModule;
    exports.OutPayService = OutPayService;
    exports.Payment = Payment;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=outgo-out-pay.umd.js.map
