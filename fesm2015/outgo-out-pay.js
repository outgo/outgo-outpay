import { __awaiter } from 'tslib';
import { ɵɵinject, ɵɵdefineInjectable, ɵsetClassMetadata, Injectable, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';

var PaymentSaleMethod;
(function (PaymentSaleMethod) {
    PaymentSaleMethod[PaymentSaleMethod["FREE"] = -1] = "FREE";
    PaymentSaleMethod[PaymentSaleMethod["MONEY"] = 0] = "MONEY";
    PaymentSaleMethod[PaymentSaleMethod["CREDIT"] = 1] = "CREDIT";
    PaymentSaleMethod[PaymentSaleMethod["DEBIT"] = 2] = "DEBIT";
    PaymentSaleMethod[PaymentSaleMethod["TRANSFER"] = 3] = "TRANSFER";
})(PaymentSaleMethod || (PaymentSaleMethod = {}));
class Payment {
    constructor(type = PaymentType.creditCard, credit = new Credit("", "", "", "", "", "", "", "", 0)) {
        this.type = type;
        this.credit = credit;
    }
}
var PaymentType;
(function (PaymentType) {
    PaymentType["creditCard"] = "creditCard";
    PaymentType["boleto"] = "boleto";
})(PaymentType || (PaymentType = {}));
class Credit {
    constructor(name, birthdate, cpf, phone, number, validity, cvv, brand, installments, address = {
        id: 0,
        city: "",
        complement: "",
        district: "",
        number: "",
        stateCode: "",
        street: "",
        zip: "",
    }) {
        this.name = name;
        this.birthdate = birthdate;
        this.cpf = cpf;
        this.phone = phone;
        this.number = number;
        this.validity = validity;
        this.cvv = cvv;
        this.brand = brand;
        this.installments = installments;
        this.address = address;
    }
}
class OutPayConfiguration {
    constructor() {
        this.serverUrl = '';
        this.pagseguroUrl = '';
        this.mercadopagoKey = '';
        this.server = { talkToServer: () => Promise.reject(), reportError: () => Promise.reject() };
    }
}

/// Funções auxiliares
/**
 * Função que arredonda o número em duas casas decimais.
 * @param number O número a ser formatado
 * @param separator O separador a ser utilizado. Usa ',' por padrão.
 * @returns Uma string correspondente ao número fornecido, com o separador desejado.
 */
function valueRound(number, separator) {
    if (!separator) {
        separator = ',';
    }
    if (number === 0) {
        return '0' + separator + '00';
    }
    let num = Math.round(number * 100) + '';
    num = num.substr(0, num.length - 2) + separator + num.substr(num.length - 2, 2);
    return num;
}

class OutPayService {
    constructor(http) {
        this.http = http;
        // ENVIRONMENT
        this.serverUrl = "";
        this.pagseguroUrl = "";
        this.mercadopagoKey = "";
        this.moderninha = false;
        this.origin = "APP_OUTGO";
        this.cart = { items: [] };
        this.payment = new Payment();
        this.installments = [];
        this.isSelling = false;
        this.productsSale = false;
        this.productCart = { items: [] };
        this.inscriptionParticipantData = [];
        this.selectedSaleInstallment = 0;
        //Valor da compra/venda
        this._value = 0;
        // PAGSEGURO
        this.pagseguroLoad = false;
        this.mercadopagoLoad = false;
        this.sessionId = "";
    }
    get paymentMethod() { return this._paymentMethod; }
    ;
    get paymentSaleMethod() { return this._paymentSaleMethod; }
    ;
    get deliveryMethod() { return this._deliveryMethod; }
    ;
    get grossValue() { return this._grossValue || this.value; }
    ;
    get platform() {
        var _a;
        return (_a = this.eventDetail) === null || _a === void 0 ? void 0 : _a.paymentMethod;
    }
    /**
     * Verifica se é necessário pedir os dados do comprador.
     * Serve tanto para exibir a tela de form quanto pra exibir o formulário de dados do comprador
     */
    get needsBuyerData() {
        return !this.isSelling && !(this.user && this.user.email && this.user.phone && this.user.cpf && this.user.birthdate);
    }
    get value() {
        return this._value;
    }
    setValue() {
        if (!this.cart.items.length && !this.productCart.items.length)
            throw new OutPayError("O carrinho ainda não foi carregado", OutPayErrorCode.GENERAL_ERROR);
        let value = this.cart.items.map(item => {
            let itemValue;
            if (this.isSelling) {
                if (this.moderninha) {
                    itemValue = ((item.ticket.tax_payed_by_company ? item.ticket.final_price : item.ticket.price) *
                        (this.paymentSaleMethod == PaymentSaleMethod.CREDIT ? (1 + this.eventDetail.tax_credit_buyer) : (this.paymentSaleMethod == PaymentSaleMethod.DEBIT ? (1 + this.eventDetail.tax_debit_buyer) : 1)));
                }
                else {
                    if (item.ticket.is_seller && !item.ticket.tax_payed_by_company) {
                        itemValue = item.ticket.price;
                    }
                    else {
                        itemValue = item.ticket.final_price;
                    }
                }
            }
            else {
                itemValue = item.ticket.final_price;
            }
            return itemValue * item.qtd;
        }).reduce((total, current) => total + current, 0);
        this.productCart.items.forEach(productItem => {
            const productValue = (productItem.price * productItem.qtd);
            if (this.eventDetail && this.user) {
                const teamMember = this.eventDetail.team.find(tm => { var _a; return tm.userId == ((_a = this.user) === null || _a === void 0 ? void 0 : _a.id); });
                value += productValue + (productValue * ((teamMember === null || teamMember === void 0 ? void 0 : teamMember.commission) ? teamMember.commission : 0));
            }
            else {
                value += productValue;
            }
        });
        if (this.cart.discount)
            this._value = value - value * (this.cart.discount.value / 100);
        else
            this._value = value;
    }
    get discount() {
        return this.cart.discount;
    }
    get boletoPermit() {
        return this.eventDetail ? this.eventDetail.hasBoleto : false;
    }
    setup(serverUrl, pagseguroUrl, mercadopagoKey, origin, server, data = {}) {
        this.serverUrl = serverUrl;
        this.pagseguroUrl = pagseguroUrl;
        this.mercadopagoKey = mercadopagoKey;
        this.server = server;
        this.origin = origin;
        if (data.moderninha)
            this.moderninha = data.moderninha;
    }
    /**
     * Limpar dados da compra/venda.
     * Deve ser chamado no começo de cada compra/venda pra evitar que uma compra anterior influencie compras futuras.
     */
    cleanCache(cleanPurchaseCache = true) {
        if (cleanPurchaseCache) {
            this.eventDetail = undefined;
            this.purchase = undefined;
        }
        this.user = undefined;
        this.cart = {
            items: []
        };
        this.productCart = {
            items: []
        };
        this._value = 0;
        this.payment = new Payment();
        this.ticketData = undefined;
        this.installments = [];
        this.isSelling = false;
        this.productsSale = false;
        this.inscriptionParticipantData = [];
        this.selectedSaleInstallment = -1;
        this.purchaser = undefined;
        this._paymentMethod = undefined;
        this._paymentSaleMethod = undefined;
        this._deliveryMethod = undefined;
        this._grossValue = undefined;
    }
    /**
     * Sender hash é utilizado nas compras de pagseguro pra identificar o comprador.
     */
    initSenderHash() {
        this.senderHash = new Promise(ok => {
            PagSeguroDirectPayment.onSenderHashReady((response) => {
                if (response.status == 'error') {
                    console.log(response);
                    return false;
                }
                ok(response.senderHash); //Hash estará disponível nesta variável.
                return true;
            });
        });
    }
    startPagseguroSession() {
        return this.http.get(this.serverUrl + 'api/getIdSession');
    }
    //forma robusta de carregar pagseguro, garante carregar script e depois carregar sessionID
    pagseguroResolve() {
        if (this.pagseguroLoad) {
            return new Promise(ok => ok(true));
        }
        else {
            //Verifica se já foi iniciado o carregamento
            if (!this.pagseguroPromise) {
                this.pagseguroPromise = new Promise((ok, reject) => {
                    let script = document.createElement('script');
                    script.addEventListener('load', () => {
                        this.startPagseguroSession().subscribe(result => {
                            if (result.session) {
                                this.sessionId = result.session.id;
                                PagSeguroDirectPayment.setSessionId(result.session.id);
                                this.initSenderHash();
                                //this.senderHash = PagSeguroDirectPayment.getSenderHash();
                                //Terminado carregamento, ajustando variáveis
                                this.pagseguroLoad = true;
                                this.pagseguroPromise = undefined;
                                ok(true);
                            }
                            else {
                                reject("O PagSeguro não está disponível. Tente novamente mais tarde.");
                            }
                        });
                    });
                    script.src = this.pagseguroUrl;
                    document.head.appendChild(script);
                });
            }
            return this.pagseguroPromise;
        }
    }
    // Carregamento do mercadopago
    mercadopagoResolve() {
        if (this.mercadopagoLoad) {
            return new Promise(ok => ok(true));
        }
        else {
            //Verifica se já foi iniciado o carregamento
            if (!this.mercadopagoPromise) {
                this.mercadopagoPromise = new Promise(ok => {
                    let script = document.createElement('script');
                    script.addEventListener('load', () => {
                        Mercadopago.setPublishableKey(this.mercadopagoKey);
                        //Terminado carregamento, ajustando variáveis
                        this.mercadopagoLoad = true;
                        this.mercadopagoPromise = undefined;
                        ok(true);
                    });
                    script.src = "https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js";
                    document.head.appendChild(script);
                });
            }
            return this.mercadopagoPromise;
        }
    }
    // Busca valor das parcelas. Deve carregar primeiro o pagseguro/mercadopago
    getInstallments(cardNumber) {
        if (cardNumber) {
            cardNumber = cardNumber.replace(/ /g, "");
            // console.log("PEGANDO INSTALLMENTS", cardNumber);
            return new Promise((success, error) => {
                const amount = this.value;
                if (this.platform == "pagseguro" && this.pagseguroLoad) {
                    this.getBrand(cardNumber).then(brand => {
                        this.payment.credit.brand = brand;
                        let pagseguroParams = {
                            amount,
                            brand: brand,
                            success: (response) => {
                                this.installments = response.installments[brand];
                                success(response.installments[brand]);
                            },
                            error: (response) => {
                                error(response);
                                console.error("Falhou no getInstallments", response);
                            },
                        };
                        if (this.eventDetail) {
                            if (this.moderninha) {
                                if (this.eventDetail.physicalInstallmentsWithoutInterest > 1) {
                                    pagseguroParams["maxInstallmentNoInterest"] = this.eventDetail.physicalInstallmentsWithoutInterest;
                                }
                            }
                            else {
                                if (this.eventDetail.installmentsWithoutInterest > 1) {
                                    pagseguroParams["maxInstallmentNoInterest"] = this.eventDetail.installmentsWithoutInterest;
                                }
                            }
                        }
                        PagSeguroDirectPayment.getInstallments(pagseguroParams);
                    }).catch(fail => {
                        error(fail);
                        console.log("falhou o brand", fail);
                    });
                }
                else if (this.platform == "mercadopago" && this.mercadopagoLoad) {
                    const bin = cardNumber.substring(0, 6);
                    Mercadopago.getInstallments({
                        bin,
                        amount
                    }, (status, response) => {
                        if (status >= 200 && status < 300) {
                            this.installments = response[0].payer_costs.map(data => {
                                return {
                                    quantity: data.installments,
                                    installmentAmount: data.installment_amount,
                                    totalAmount: data.total_amount,
                                    interestFree: data.installment_rate == 0,
                                    cardProvider: response[0].payment_method_id,
                                };
                            });
                            success(this.installments);
                        }
                        else {
                            error(response);
                            console.log("falhou as parcelas mercadopago");
                        }
                    });
                }
                else {
                    error('Biblioteca de pagamento não carregada');
                    console.log(`sem ${this.platform}`);
                }
            });
        }
        else {
            //Obter parcelas pagseguro sem informação do cartão.
            return new Promise((success, error) => {
                let pagseguroParams = {
                    amount: this.value,
                    success: (response) => {
                        //O pagseguro responde com vários tipos de cartões. Verifiquei que os mais comuns respondem com a mesma quantidade de parcelas.
                        //Uma abordagem mais segura seria sempre usar a resposta com o menor número de parcelas.
                        this.installments = response.installments["visa"];
                        success(response.installments["visa"]);
                    },
                    error: (response) => {
                        error(response);
                        console.error("Falhou no getInstallments sem cartão", response);
                    }
                };
                if (this.eventDetail) {
                    if (this.moderninha) {
                        if (this.eventDetail.physicalInstallmentsWithoutInterest > 1) {
                            pagseguroParams["maxInstallmentNoInterest"] = this.eventDetail.physicalInstallmentsWithoutInterest;
                        }
                    }
                    else {
                        if (this.eventDetail.installmentsWithoutInterest > 1) {
                            pagseguroParams["maxInstallmentNoInterest"] = this.eventDetail.installmentsWithoutInterest;
                        }
                    }
                }
                PagSeguroDirectPayment.getInstallments(pagseguroParams);
            });
        }
    }
    //pegar a bandeira do cartão (pagseguro)
    getBrand(cardNumber) {
        return new Promise((success, error) => {
            PagSeguroDirectPayment.getBrand({
                cardBin: cardNumber,
                success: (response) => {
                    success(response.brand.name);
                },
                error: (response) => {
                    console.error(response);
                    error(response);
                }
            });
        });
    }
    /**
     * Validação dos dados do usuário.
     */
    personalDataValidate() {
        if (!this.user)
            return false;
        let actualPersonalValidate = true;
        if (this.user.email.includes("@facebook.com") || !this.user.email) {
            actualPersonalValidate = false;
            this.user.email = ""; //Limpando email
        }
        if (!this.user.phone) {
            actualPersonalValidate = false;
        }
        if (!this.user.cpf) {
            actualPersonalValidate = false;
        }
        if (!this.user.birthdate) {
            actualPersonalValidate = false;
        }
        return actualPersonalValidate;
    }
    /**
     * Função que carrega os dados do evento antes de iniciar a venda.
     * @param eventId O ID do evento.
     * @param userId O ID do usuário. Os dados do usuário são baixados apenas se o parâmetro for fornecido.
     * @throws GENERAL_ERROR se a função for chamada antes de salvar os dados do carrinho de compras.
     */
    eventPurchaseData(eventId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!userId && this.user)
                userId = this.user.id;
            const eventDetail = yield this.getEventDetail(eventId);
            this.setValue();
            if (userId) {
                yield this.getUserDetail(eventDetail.event.clientId, userId);
            }
            return true;
        });
    }
    getEventDetail(eventId) {
        return __awaiter(this, void 0, void 0, function* () {
            const eventDetail = yield this.server.talkToServer("event_detail/" + encodeURIComponent(eventId), { ignoreVisit: true });
            this.eventDetail = eventDetail;
            return eventDetail;
        });
    }
    getUserDetail(companyId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.server.talkToServer('user_data', { company_id: companyId });
            this.user = user;
            return user;
        });
    }
    /**
     * Função que obtem os parâmetros básicos para criar uma compra no servidor.
     * @returns `null` Caso o usuário não esteja registrado, e um `Object` com os dados em caso contrário.
     */
    getPaymentParams() {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.user)
                return null;
            let totalPrice = 0;
            let totalFinalPrice = 0;
            let productNumber = 1;
            const data = {};
            let description = "";
            // Adicionando dados de todos os produtos
            for (const item of this.cart.items) {
                if (item.qtd > 0) {
                    data['itemId' + productNumber] = item.ticket.id.toString();
                    data['itemDescription' + productNumber] = item.ticket.name;
                    data['itemAmount' + productNumber] = valueRound(item.ticket.final_price, '.');
                    data['itemClientAmount' + productNumber] = valueRound(item.ticket.price, '.');
                    data['itemQuantity' + productNumber] = item.qtd.toString();
                    data['itemReservationId' + productNumber] = item.reservations.map(r => r.id);
                    if (this.inscriptionParticipantData) {
                        data['itemParticipants' + productNumber] = "";
                        for (let i = 0; i < this.inscriptionParticipantData.length; i++) {
                            const participantData = this.inscriptionParticipantData[i];
                            data['itemParticipants' + productNumber] += `name:${participantData.name}`;
                            data['itemParticipants' + productNumber] += `,email:${participantData.email}`;
                            if (participantData.cpf)
                                data['itemParticipants' + productNumber] += `,cpf:${participantData.cpf}`;
                            if (participantData.phone)
                                data['itemParticipants' + productNumber] += `,phone:${participantData.phone}`;
                            if (participantData.birthdate)
                                data['itemParticipants' + productNumber] += `,birthdate:${participantData.birthdate}`;
                            for (let i = 0; i < (participantData.extra_fields ? Object.keys(participantData.extra_fields).length : 0); i++) {
                                const formFieldId = parseInt(Object.keys(participantData.extra_fields)[i]);
                                data['itemParticipants' + productNumber] += `,extra_fields/${formFieldId}:${participantData.extra_fields[formFieldId]}`;
                            }
                            if (i < this.inscriptionParticipantData.length - 1)
                                data['itemParticipants' + productNumber] += "§";
                        }
                    }
                    productNumber++;
                    totalPrice += item.ticket.price * item.qtd;
                    totalFinalPrice += item.ticket.final_price * item.qtd;
                    if (description != "")
                        description += " | ";
                    description += item.qtd.toString() + "x " + item.ticket.name;
                }
            }
            data['clientId'] = this.eventDetail.event.clientId.toString();
            data['senderHash'] = yield this.senderHash;
            data['fullName'] = this.user.name;
            data['name'] = this.user.name;
            if (this.user.cpf) {
                data['usercpf'] = this.user.cpf.replace(/\.|-/g, '');
                data['cpf'] = this.user.cpf.replace(/\.|-/g, '');
            }
            else {
                data['usercpf'] = '';
                data['cpf'] = '';
            }
            data['email'] = this.user.email;
            if (this.user.phone) {
                data['completePhone'] = this.user.phone.replace(/[(_)-\s]/g, '');
                data['areaCode'] = data['completePhone'].substr(0, 2);
                data['phone'] = data['completePhone'].substring(2);
            }
            else {
                data['completePhone'] = '';
                data['areaCode'] = '';
                data['phone'] = '';
            }
            data['senderBirthdate'] = this.user.birthdate;
            data['birthday'] = data['senderBirthdate'];
            data['userId'] = this.user.id.toString();
            data['changeUser'] = false;
            data['eventId'] = this.eventDetail.event.id;
            data['purchaseId'] = (_a = this.purchase) === null || _a === void 0 ? void 0 : _a.id;
            data['companyUserId'] = this.user.companyUserId;
            data['totalPrice'] = valueRound(totalPrice, '.');
            data['totalFinalPrice'] = valueRound(totalFinalPrice, '.');
            data['eventName'] = this.eventDetail.event.name;
            data['origin'] = this.origin;
            if (this.discount)
                data['discountId'] = this.discount.id;
            if (this.eventDetail.paymentMethod == "mercadopago") {
                data["transaction_amount"] = totalFinalPrice;
                data["description"] = description;
            }
            return data;
        });
    }
    /**************************************************
    INICIA AS COMPRAS
    Chamado após escolher os ingressos e clicar em comprar
    @throws VALIDATE_EMAIL caso a compra seja paga e o usuário ainda não tiver validado seu email.
    @throws UNAUTHENTICATED caso o id de usuário não seja informado.
    @throws TICKET_UNAVAILABLE caso o ingresso solicitado não esteja mais disponível.
    @param cart O carrinho de compras, com o desconto, caso exista.
    @param eventId O ID do evento.
    @param userId O ID do usuário, caso ele esteja logado no sistema.
    @param cleanPurchaseCache Se os dados da compra devem ser limpos do cache. `true` por padrão, só deve ser sobrescrito se necessário.
    **************************************************/
    init(cart, eventId, userId, cleanPurchaseCache = true) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            this.cleanCache(cleanPurchaseCache);
            this.isSelling = false;
            this.cart = cart;
            yield this.eventPurchaseData(eventId, userId);
            if (this.value > 0) {
                if (this.platform == "pagseguro") {
                    if (!(yield this.pagseguroResolve())) {
                        throw new OutPayError("Não carregou Pagseguro");
                    }
                }
                else {
                    if (!(yield this.mercadopagoResolve())) {
                        throw new OutPayError("Não carregou Mercadopago");
                    }
                }
                for (let item of this.cart.items) {
                    if (item.ticket.price == 0)
                        item.qtd = 0;
                }
            }
            else {
                //Compras sem valor são consideradas gratuitas e tem seus itens ajustados para valor 0
                this._paymentMethod = "free";
                if (!this.cart.discount || this.cart.discount.value != 100) {
                    for (let item of this.cart.items) {
                        if (item.ticket.price > 0)
                            item.qtd = 0;
                    }
                }
            }
            if (!userId)
                throw new OutPayError("Você não está logado.", OutPayErrorCode.UNAUTHENTICATED);
            try {
                let response = yield this.server.talkToServer("doublecheck_tickets", { cart: this.cart.items, params: yield this.getPaymentParams() }, { type: "POST", retries: 4 });
                if (response.error) {
                    if ("ticket_name" in response) {
                        throw new OutPayError(`Limite do ingresso ${response.ticket_name} atingido. Selecione uma nova quantidade.`, OutPayErrorCode.TICKET_UNAVAILABLE, { otto: true, ottoData: { image: "atento" } });
                    }
                    if ("errorMsg" in response) {
                        throw new OutPayError(response.errorMsg, OutPayErrorCode.GENERAL_ERROR, { otto: true, ottoData: { image: "atento" } });
                    }
                    // Se não souber qual foi o erro, jogar para evitar erros silenciosos.
                    throw response;
                }
                else {
                    if (response.purchase)
                        this.purchase = response.purchase;
                    if (((_a = this.eventDetail) === null || _a === void 0 ? void 0 : _a.paymentMethod) == "mercadopago") {
                        this.ticketData = response.mercadopago;
                    }
                    else {
                        this.ticketData = response.pagseguro;
                    }
                    if (this.value > 0 && this.personalDataValidate() && !this.eventDetail.have_participant_form) {
                        return { finished: false, nextStep: OutPayStep.PAYMENT_METHOD, purchase: this.purchase };
                    }
                    else {
                        return { finished: false, nextStep: OutPayStep.PURCHASE_FORM, purchase: this.purchase };
                    }
                }
            }
            catch (error) {
                if (error.status == 400) {
                    if (error.error == "verify_email" || error.error.verify_email) {
                        //Deve recuperar os dados de compra, pois mesmo com o erro já criou a pre-reserva
                        if (error.error.purchase)
                            this.purchase = error.error.purchase;
                        if (((_b = this.eventDetail) === null || _b === void 0 ? void 0 : _b.paymentMethod) == "mercadopago") {
                            this.ticketData = error.error.mercadopago;
                        }
                        else {
                            this.ticketData = error.error.pagseguro;
                        }
                        throw new OutPayError(`Enviamos um código para ${this.user.email} para você validar abaixo.`, OutPayErrorCode.VALIDATE_EMAIL);
                    }
                    if (error.error.error) {
                        throw new OutPayError(error.error.errorMsg, OutPayErrorCode.GENERAL_ERROR);
                    }
                }
                else if (error.status == 401) {
                    throw new OutPayError("Você não está logado.", OutPayErrorCode.UNAUTHENTICATED);
                }
                // Evitar erros silenciosos
                throw error;
            }
        });
    }
    /**************************************************
    INICIA O PROCESSO DO VENDEDOR
    Chamado após escolher os ingressos e clicar em vender
    **************************************************/
    sellerInit(cart, eventId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            this.cleanCache();
            this.isSelling = true;
            this.cart = cart;
            yield this.eventPurchaseData(eventId, userId);
            return yield this.nextStepSale();
        });
    }
    /**************************************************
    INICIA O PROCESSO DO VENDEDOR DE PRODUTOS
    Chamado após escolher os produtos e clicar em vender
    **************************************************/
    productSellerInit(productCart, eventDetail, user) {
        this.cleanCache();
        this.eventDetail = eventDetail;
        this.isSelling = true;
        this.productsSale = true;
        this.user = user;
        this.productCart = productCart;
        this.setValue();
        return { finished: false, nextStep: OutPayStep.PAYMENT_METHOD, purchase: this.purchase };
    }
    /**
     * saveFormData
     * Chamado após preencher os dados de formulário.
     */
    saveFormData(participantData) {
        return __awaiter(this, void 0, void 0, function* () {
            this.inscriptionParticipantData = participantData;
            if (this.isSelling) {
                if (this.deliveryMethod == "digital" && this.inscriptionParticipantData.length > 0) {
                    try {
                        this.purchaser = yield this.server.talkToServer("purchaser", { email: this.inscriptionParticipantData[0].email }, { retries: 4 });
                    }
                    catch (error) {
                        if (error.error == "name") {
                            this.purchaser = yield this.server.talkToServer("purchaser/create", {
                                name: this.inscriptionParticipantData[0].name,
                                email: this.inscriptionParticipantData[0].email,
                                phone: this.inscriptionParticipantData[0].phone,
                                cpf: this.inscriptionParticipantData[0].cpf,
                                birthdat: this.inscriptionParticipantData[0].birthdate
                            }, {
                                type: "POST",
                                retries: 4,
                            });
                        }
                        else {
                            throw error;
                        }
                    }
                }
                return yield this.nextStepSale();
            }
            else {
                if (this.value > 0) {
                    return { finished: false, nextStep: OutPayStep.PAYMENT_METHOD, purchase: this.purchase };
                }
                else {
                    return { finished: false, nextStep: OutPayStep.FINISH, purchase: this.purchase };
                }
            }
        });
    }
    /**
     * selectPaymentMethod
     * Chamada após selecionar método de pagamento para compras online.
     */
    selectPaymentMethod(paymentMethod) {
        this._paymentMethod = paymentMethod;
        if (paymentMethod == "credit") {
            this.payment.type = PaymentType.creditCard;
        }
        if (this.paymentMethod == "credit" || this.needsBuyerData) {
            return { finished: false, nextStep: OutPayStep.PAYMENT_DATA, purchase: this.purchase };
        }
        else {
            return { finished: false, nextStep: OutPayStep.FINISH, purchase: this.purchase };
        }
    }
    /**
     * Chamada após selecionar o método de pagamento para vendas.
     * @param paymentSaleMethod Método de pagamento da venda.
     * @param usingExternalAppForPayment Opcional. Permite que o outpay prossiga em uma venda de cartão sem usar moderninha.
     */
    selectSalePaymentMethod(paymentSaleMethod, usingExternalAppForPayment = false) {
        return __awaiter(this, void 0, void 0, function* () {
            this._paymentSaleMethod = paymentSaleMethod;
            if (this.moderninha && [PaymentSaleMethod.CREDIT, PaymentSaleMethod.DEBIT].includes(paymentSaleMethod)) {
                //Aplicar taxas de venda em cartão no preço da moderninha.
                this.setValue();
                if (this.eventDetail.cardSalesOnApp) {
                    try {
                        yield this.pagseguroResolve();
                    }
                    catch (_a) {
                        throw new OutPayError("Erro ao iniciar pagseguro", OutPayErrorCode.GENERAL_ERROR);
                    }
                }
                else {
                    if (usingExternalAppForPayment) {
                        this.purchase = yield this.createPurchase(true);
                    }
                    else {
                        throw new OutPayError('Use o app "Vendas" ou outra máquina e realize o pagamento no cartão para continuar.', OutPayErrorCode.UNABLE_TO_PROCEED);
                    }
                }
            }
            return yield this.nextStepSale();
        });
    }
    /**
     * Chamada após selecionar a quantidade de parcelas em uma venda de cartão de crédito.
     * @param selectedInstallment O índice da quantidade de parcelas, iniciando em zero.
     */
    selectSaleInstallments(selectedInstallment) {
        return __awaiter(this, void 0, void 0, function* () {
            this.selectedSaleInstallment = selectedInstallment;
            this._grossValue = this.installments[selectedInstallment].totalAmount;
            return yield this.nextStepSale();
        });
    }
    /**
     * Chamada após selecionar o método de entrega da venda.
     * @param deliveryMethod O método de entrega da venda.
     */
    selectDeliveryMethod(deliveryMethod) {
        return __awaiter(this, void 0, void 0, function* () {
            this._deliveryMethod = deliveryMethod;
            return yield this.nextStepSale();
        });
    }
    /**
     * Chamada para determinar o comprador em uma venda digital sem formulário.
     * @param email O email do comprador.
     * @param name O nome do comprador, para criar um novo usuário.
     * @throws NOT_FOUND caso seja informado apenas o email, e não exista usuário com esse email.
     */
    selectPurchaser(email, name) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!name) {
                    this.purchaser = yield this.server.talkToServer("purchaser", { email }, { retries: 4 });
                }
                else {
                    this.purchaser = yield this.server.talkToServer("purchaser/create", { email, name }, {
                        type: "POST",
                        retries: 4,
                    });
                }
            }
            catch (error) {
                if (error.error == "name")
                    throw new OutPayError(error.error, OutPayErrorCode.NOT_FOUND);
                else
                    throw new OutPayError(error, OutPayErrorCode.GENERAL_ERROR);
            }
            return this.nextStepSale();
        });
    }
    /**
     * finishPurchase
     * Chamada após digitar os dados de pagamento.
     * @throws PURCHASE_ERROR quando ocorre um erro na compra.
     */
    finishPurchase(cardToken) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.value > 0 && this.eventDetail.paymentMethod == "pagseguro" && this.paymentMethod == "credit") {
                return yield this.pagseguroCreditCard();
            }
            else {
                return yield this.finalizePurchase(cardToken);
            }
        });
    }
    finishSale() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.purchase) {
                this.purchase = yield this.createPurchase();
            }
            else if (this.purchase.status < 4) {
                this.purchase.status = 4;
                yield this.server.talkToServer("purchase/" + this.purchase.id, this.purchase, {
                    type: "PUT",
                    retries: 4,
                });
            }
            return this.nextStepSale();
        });
    }
    //serve para tratar cartão tanto do pagseguro quanto do mercadopago
    pagseguroCreditCard() {
        return new Promise((resolve, reject) => {
            const param = {
                cardNumber: this.payment.credit.number.replace(/ /g, ""),
                brand: this.payment.credit.brand,
                cvv: this.payment.credit.cvv,
                expirationMonth: this.payment.credit.validity.substr(0, 2),
                expirationYear: '20' + this.payment.credit.validity.substr(3, 2),
                error: (error) => {
                    reject(error);
                },
                success: (response) => {
                    this.finalizePurchase(response.card.token).then(result => {
                        resolve(result);
                    }, (error) => reject(error));
                }
            };
            PagSeguroDirectPayment.createCardToken(param);
        });
    }
    finalizePurchase(cardToken) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            if (this.user) {
                if (!this.user.cpf)
                    this.user.cpf = this.payment.credit.cpf;
                if (!this.user.birthdate)
                    this.user.birthdate = this.payment.credit.birthdate;
                if (!this.user.phone)
                    this.user.phone = this.payment.credit.phone;
            }
            const data = yield this.getPaymentParams();
            if (cardToken) {
                const installments = this.installments[this.payment.credit.installments];
                data['paymentMethod'] = 'creditCard';
                if (this.eventDetail.paymentMethod == "mercadopago") {
                    data["payment_method_id"] = installments.cardProvider;
                }
                data['street'] = this.payment.credit.address.street;
                data['number'] = this.payment.credit.address.number;
                data['zip'] = this.payment.credit.address.zip;
                data['district'] = this.payment.credit.address.district;
                data['city'] = this.payment.credit.address.city;
                data['stateCode'] = this.payment.credit.address.stateCode;
                data['complement'] = '';
                data['installmentsQtd'] = installments.quantity.toString();
                data['installmentsValue'] = valueRound(installments.installmentAmount, '.');
                data['code'] = cardToken;
                data['cardName'] = this.payment.credit.name;
                data['cardCpf'] = this.payment.credit.cpf;
                data['cardPhone'] = this.payment.credit.phone;
                data['pagamento'] = '' + installments.quantity + ',' + installments.installmentAmount + ',' + installments.totalAmount;
            }
            else if (this.paymentMethod == "boleto") {
                data['paymentMethod'] = 'boleto';
            }
            else {
                data['paymentMethod'] = 'free';
            }
            data['ticketData'] = this.ticketData;
            this.inscriptionParticipantData = [];
            try {
                const purchase = yield this.doPayment(data);
                this.purchase = purchase;
                return { finished: true, nextStep: OutPayStep.FINISH, purchase: this.purchase };
            }
            catch (error) {
                if (error.errorCode)
                    throw error;
                throw new OutPayError(((_a = error.error) === null || _a === void 0 ? void 0 : _a.text) || "Houve um erro na compra", OutPayErrorCode.PURCHASE_ERROR);
            }
        });
    }
    doPayment(data) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            let response;
            try {
                if (this.value == 0) {
                    response = yield this.server.talkToServer("paymentMercadopago", data, { type: "POST" });
                }
                else if (this.eventDetail.paymentMethod == "mercadopago") {
                    response = yield this.http.post(this.serverUrl + 'api/paymentMercadopago', data).toPromise();
                }
                else {
                    response = yield this.http.post(this.serverUrl + 'api/payment', data).toPromise();
                }
            }
            catch (error) {
                throw new OutPayError(((_a = error.error) === null || _a === void 0 ? void 0 : _a.text) || "Houve um erro na compra", OutPayErrorCode.PURCHASE_ERROR);
            }
            if (response.error) {
                throw new OutPayError(response.errorMsg, OutPayErrorCode.PURCHASE_ERROR);
            }
            return response.purchase;
        });
    }
    createPurchase(unpaidPurchase = false) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = { purchaser_id: this.purchaser ? this.purchaser.id : null, payment_method: this.paymentSaleMethod };
            if (this.cart.discount)
                data.discount_id = this.cart.discount.id;
            if (this.cart) {
                const ticketList = {};
                const reservationList = {};
                this.cart.items.forEach(item => {
                    ticketList[item.ticket.id] = item.qtd;
                    if (item.reservations.length > 0)
                        reservationList[item.ticket.id] = item.reservations.map((r) => r.id);
                });
                data.cart = ticketList;
                if (Object.getOwnPropertyNames(reservationList).length > 0)
                    data.cart_reservation = reservationList;
            }
            if (this.productCart) {
                data.product_cart = this.productCart.items;
            }
            if (this.inscriptionParticipantData)
                data.participant_data = this.inscriptionParticipantData;
            if (this.deliveryMethod)
                data.purchase_type = this.deliveryMethod;
            if (unpaidPurchase)
                data.unpaid_purchase = unpaidPurchase;
            data.is_selling = this.isSelling;
            if (this.moderninha) {
                data.moderninha = true;
            }
            return this.server.talkToServer("purchase/create", data, {
                type: "POST",
                retries: 4,
            });
        });
    }
    /**
     * Função que analísa a compra, e retorna o próximo input necessário pelo usuário.
     * @returns Um objeto `OutPayResult`, com o próximo passo e objeto da compra.
     */
    nextStepSale() {
        return __awaiter(this, void 0, void 0, function* () {
            const result = { finished: false, nextStep: -1, purchase: this.purchase };
            //Se a compra for paga, verificar método de pagamento e parcelamento.
            if (this.value > 0) {
                if (this.paymentSaleMethod == undefined) {
                    result.nextStep = OutPayStep.PAYMENT_METHOD;
                    return result;
                }
                else {
                    if (this.moderninha && this.paymentSaleMethod == PaymentSaleMethod.CREDIT) {
                        if (this.selectedSaleInstallment < 0) {
                            result.nextStep = OutPayStep.INSTALLMENTS;
                        }
                    }
                }
            }
            else {
                if (this.paymentSaleMethod == undefined) {
                    this._paymentSaleMethod = PaymentSaleMethod.FREE;
                }
            }
            //Depois, verificar o método de entrega. Caso só seja possível um, este é selecionado.
            if (result.nextStep < 0) {
                if (!this.deliveryMethod) {
                    if (this.moderninha && (this.productsSale || this.value == 0)) {
                        this._deliveryMethod = "physical";
                    }
                    else {
                        if (this.moderninha && this.eventDetail.allow_physical_sales && this.eventDetail.allow_digital_sales) {
                            result.nextStep = OutPayStep.DELIVERY_METHOD;
                        }
                        else if (this.moderninha && this.eventDetail.allow_physical_sales) {
                            this._deliveryMethod = "physical";
                        }
                        else if (this.eventDetail.allow_digital_sales) {
                            this._deliveryMethod = "digital";
                        }
                        else {
                            throw new OutPayError("Houve um problema para continuar a venda. Fale com o administrador do evento.", OutPayErrorCode.PURCHASE_ERROR, { otto: true, ottoData: { image: "triste", duration: 3000 } });
                        }
                    }
                }
            }
            //O próximo passo é verificar dados dos compradores, via formulário ou tela do comprador.
            //Vendas de produtos não precisam de informação sobre comprador.
            if (result.nextStep < 0 && !this.productsSale) {
                if (this.eventDetail.have_participant_form || this.value == 0) {
                    if (!this.inscriptionParticipantData.length) {
                        result.nextStep = OutPayStep.PURCHASE_FORM;
                    }
                }
                else if (this.deliveryMethod == "digital" && !this.eventDetail.have_participant_form) {
                    if (!this.purchaser) {
                        result.nextStep = OutPayStep.PURCHASER;
                    }
                }
            }
            //Caso a compra seja feita no cartão, o app deverá ir para a tela de cobranças.
            if (result.nextStep < 0) {
                if (this.moderninha && this.paymentSaleMethod && [PaymentSaleMethod.CREDIT, PaymentSaleMethod.DEBIT].includes(this.paymentSaleMethod)) {
                    if (!this.purchase) {
                        this.purchase = yield this.createPurchase(true);
                        result.purchase = this.purchase;
                        result.nextStep = OutPayStep.PLUGPAG;
                    }
                }
            }
            //Caso seja uma compra física que não foi finalizada ainda, ela precisa ser impressa.
            if (result.nextStep < 0) {
                if (this.deliveryMethod == "physical") {
                    if (!this.purchase) {
                        this.purchase = yield this.createPurchase(true);
                        result.purchase = this.purchase;
                    }
                    if (this.purchase.status < 4) {
                        result.nextStep = OutPayStep.PRINT;
                    }
                    else {
                        result.nextStep = OutPayStep.FINISH;
                    }
                }
            }
            //Por fim, a venda é encerrada.
            if (result.nextStep < 0) {
                if (!this.purchase || this.purchase.status < 4)
                    yield this.finishSale();
                result.purchase = this.purchase;
                result.finished = true;
                result.nextStep = OutPayStep.FINISH;
            }
            return result;
        });
    }
}
OutPayService.ɵfac = function OutPayService_Factory(t) { return new (t || OutPayService)(ɵɵinject(HttpClient)); };
OutPayService.ɵprov = ɵɵdefineInjectable({ token: OutPayService, factory: OutPayService.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && ɵsetClassMetadata(OutPayService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: HttpClient }]; }, null); })();
var OutPayStep;
(function (OutPayStep) {
    /** Seleção de forma de pagamento */
    OutPayStep[OutPayStep["PAYMENT_METHOD"] = 0] = "PAYMENT_METHOD";
    /** Formulário de dados do comprador e dos participantes */
    OutPayStep[OutPayStep["PURCHASE_FORM"] = 1] = "PURCHASE_FORM";
    /** Impressão do ingresso, nos casos onde o pagamento não ocorre via PlugPag (ie. Dinheiro) */
    OutPayStep[OutPayStep["PRINT"] = 2] = "PRINT";
    /** Finalizar processo de compra */
    OutPayStep[OutPayStep["FINISH"] = 3] = "FINISH";
    /** Formulário de dados do pagamento (cartão) */
    OutPayStep[OutPayStep["PAYMENT_DATA"] = 4] = "PAYMENT_DATA";
    /** Seleção de forma de recebimento */
    OutPayStep[OutPayStep["DELIVERY_METHOD"] = 5] = "DELIVERY_METHOD";
    /** Seleção do parcelamento do pagamento */
    OutPayStep[OutPayStep["INSTALLMENTS"] = 6] = "INSTALLMENTS";
    /** Pagamento via PlugPag */
    OutPayStep[OutPayStep["PLUGPAG"] = 7] = "PLUGPAG";
    /** Identificação do comprador para vendas digitais sem formulário */
    OutPayStep[OutPayStep["PURCHASER"] = 8] = "PURCHASER";
})(OutPayStep || (OutPayStep = {}));
class OutPayError {
    constructor(msg, errorCode = OutPayErrorCode.GENERAL_ERROR, data = {}) {
        this.msg = msg;
        this.errorCode = errorCode;
        this.data = data;
    }
    ;
    get ottoAlert() {
        if (this.data.otto) {
            return {
                text: this.msg,
                image: this.data.ottoData.image,
                duration: this.data.ottoData.duration,
            };
        }
        else
            throw new Error("Não há mensagem do otto neste erro");
    }
}
var OutPayErrorCode;
(function (OutPayErrorCode) {
    /** Erro genérico */
    OutPayErrorCode[OutPayErrorCode["GENERAL_ERROR"] = 1] = "GENERAL_ERROR";
    /** O usuário precisa validar o email para prosseguir */
    OutPayErrorCode[OutPayErrorCode["VALIDATE_EMAIL"] = 2] = "VALIDATE_EMAIL";
    /** O ingresso solicitado já está esgotado */
    OutPayErrorCode[OutPayErrorCode["TICKET_UNAVAILABLE"] = 3] = "TICKET_UNAVAILABLE";
    /** Houve um erro ao processar a compra */
    OutPayErrorCode[OutPayErrorCode["PURCHASE_ERROR"] = 4] = "PURCHASE_ERROR";
    /** O usuário não está logado no sistema */
    OutPayErrorCode[OutPayErrorCode["UNAUTHENTICATED"] = 5] = "UNAUTHENTICATED";
    /** Não é possível executar a ação, um caminho alternativo deve ser buscado */
    OutPayErrorCode[OutPayErrorCode["UNABLE_TO_PROCEED"] = 6] = "UNABLE_TO_PROCEED";
    /** Não foi encontrado um resultado para os dados fornecidos */
    OutPayErrorCode[OutPayErrorCode["NOT_FOUND"] = 7] = "NOT_FOUND";
})(OutPayErrorCode || (OutPayErrorCode = {}));

class OutPayModule {
}
OutPayModule.ɵmod = ɵɵdefineNgModule({ type: OutPayModule });
OutPayModule.ɵinj = ɵɵdefineInjector({ factory: function OutPayModule_Factory(t) { return new (t || OutPayModule)(); }, providers: [
        OutPayService
    ], imports: [[
            HttpClientModule,
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(OutPayModule, { imports: [HttpClientModule] }); })();
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && ɵsetClassMetadata(OutPayModule, [{
        type: NgModule,
        args: [{
                imports: [
                    HttpClientModule,
                ],
                providers: [
                    OutPayService
                ],
            }]
    }], null, null); })();

/*
 * Public API Surface of out-pay
 */

/**
 * Generated bundle index. Do not edit.
 */

export { Credit, OutPayConfiguration, OutPayError, OutPayErrorCode, OutPayModule, OutPayService, OutPayStep, Payment, PaymentSaleMethod, PaymentType };
//# sourceMappingURL=outgo-out-pay.js.map
