import { OutPayService } from './out-pay.service';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import * as i0 from "@angular/core";
export class OutPayModule {
}
OutPayModule.ɵmod = i0.ɵɵdefineNgModule({ type: OutPayModule });
OutPayModule.ɵinj = i0.ɵɵdefineInjector({ factory: function OutPayModule_Factory(t) { return new (t || OutPayModule)(); }, providers: [
        OutPayService
    ], imports: [[
            HttpClientModule,
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(OutPayModule, { imports: [HttpClientModule] }); })();
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OutPayModule, [{
        type: NgModule,
        args: [{
                imports: [
                    HttpClientModule,
                ],
                providers: [
                    OutPayService
                ],
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3V0LXBheS5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9wcm9qZWN0cy9vdXRnby9vdXQtcGF5L3NyYy9saWIvb3V0LXBheS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBV3pDLE1BQU0sT0FBTyxZQUFZOztnREFBWixZQUFZO3VHQUFaLFlBQVksbUJBSlg7UUFDUixhQUFhO0tBQ2YsWUFMUTtZQUNOLGdCQUFnQjtTQUNsQjt3RkFLUyxZQUFZLGNBTm5CLGdCQUFnQjt1RkFNVCxZQUFZO2NBUnhCLFFBQVE7ZUFBQztnQkFDUCxPQUFPLEVBQUU7b0JBQ04sZ0JBQWdCO2lCQUNsQjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1IsYUFBYTtpQkFDZjthQUNIIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgT3V0UGF5U2VydmljZSB9IGZyb20gJy4vb3V0LXBheS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICBpbXBvcnRzOiBbXHJcbiAgICAgIEh0dHBDbGllbnRNb2R1bGUsXHJcbiAgIF0sXHJcbiAgIHByb3ZpZGVyczogW1xyXG4gICAgICBPdXRQYXlTZXJ2aWNlXHJcbiAgIF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBPdXRQYXlNb2R1bGUge30iXX0=