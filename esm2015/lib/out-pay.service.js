import { __awaiter } from "tslib";
import { Payment, PaymentType, PaymentSaleMethod } from './model';
import { Injectable } from "@angular/core";
import { valueRound } from './util';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class OutPayService {
    constructor(http) {
        this.http = http;
        // ENVIRONMENT
        this.serverUrl = "";
        this.pagseguroUrl = "";
        this.mercadopagoKey = "";
        this.moderninha = false;
        this.origin = "APP_OUTGO";
        this.cart = { items: [] };
        this.payment = new Payment();
        this.installments = [];
        this.isSelling = false;
        this.productsSale = false;
        this.productCart = { items: [] };
        this.inscriptionParticipantData = [];
        this.selectedSaleInstallment = 0;
        //Valor da compra/venda
        this._value = 0;
        // PAGSEGURO
        this.pagseguroLoad = false;
        this.mercadopagoLoad = false;
        this.sessionId = "";
    }
    get paymentMethod() { return this._paymentMethod; }
    ;
    get paymentSaleMethod() { return this._paymentSaleMethod; }
    ;
    get deliveryMethod() { return this._deliveryMethod; }
    ;
    get grossValue() { return this._grossValue || this.value; }
    ;
    get platform() {
        var _a;
        return (_a = this.eventDetail) === null || _a === void 0 ? void 0 : _a.paymentMethod;
    }
    /**
     * Verifica se é necessário pedir os dados do comprador.
     * Serve tanto para exibir a tela de form quanto pra exibir o formulário de dados do comprador
     */
    get needsBuyerData() {
        return !this.isSelling && !(this.user && this.user.email && this.user.phone && this.user.cpf && this.user.birthdate);
    }
    get value() {
        return this._value;
    }
    setValue() {
        if (!this.cart.items.length && !this.productCart.items.length)
            throw new OutPayError("O carrinho ainda não foi carregado", OutPayErrorCode.GENERAL_ERROR);
        let value = this.cart.items.map(item => {
            let itemValue;
            if (this.isSelling) {
                if (this.moderninha) {
                    itemValue = ((item.ticket.tax_payed_by_company ? item.ticket.final_price : item.ticket.price) *
                        (this.paymentSaleMethod == PaymentSaleMethod.CREDIT ? (1 + this.eventDetail.tax_credit_buyer) : (this.paymentSaleMethod == PaymentSaleMethod.DEBIT ? (1 + this.eventDetail.tax_debit_buyer) : 1)));
                }
                else {
                    if (item.ticket.is_seller && !item.ticket.tax_payed_by_company) {
                        itemValue = item.ticket.price;
                    }
                    else {
                        itemValue = item.ticket.final_price;
                    }
                }
            }
            else {
                itemValue = item.ticket.final_price;
            }
            return itemValue * item.qtd;
        }).reduce((total, current) => total + current, 0);
        this.productCart.items.forEach(productItem => {
            const productValue = (productItem.price * productItem.qtd);
            if (this.eventDetail && this.user) {
                const teamMember = this.eventDetail.team.find(tm => { var _a; return tm.userId == ((_a = this.user) === null || _a === void 0 ? void 0 : _a.id); });
                value += productValue + (productValue * ((teamMember === null || teamMember === void 0 ? void 0 : teamMember.commission) ? teamMember.commission : 0));
            }
            else {
                value += productValue;
            }
        });
        if (this.cart.discount)
            this._value = value - value * (this.cart.discount.value / 100);
        else
            this._value = value;
    }
    get discount() {
        return this.cart.discount;
    }
    get boletoPermit() {
        return this.eventDetail ? this.eventDetail.hasBoleto : false;
    }
    setup(serverUrl, pagseguroUrl, mercadopagoKey, origin, server, data = {}) {
        this.serverUrl = serverUrl;
        this.pagseguroUrl = pagseguroUrl;
        this.mercadopagoKey = mercadopagoKey;
        this.server = server;
        this.origin = origin;
        if (data.moderninha)
            this.moderninha = data.moderninha;
    }
    /**
     * Limpar dados da compra/venda.
     * Deve ser chamado no começo de cada compra/venda pra evitar que uma compra anterior influencie compras futuras.
     */
    cleanCache(cleanPurchaseCache = true) {
        if (cleanPurchaseCache) {
            this.eventDetail = undefined;
            this.purchase = undefined;
        }
        this.user = undefined;
        this.cart = {
            items: []
        };
        this.productCart = {
            items: []
        };
        this._value = 0;
        this.payment = new Payment();
        this.ticketData = undefined;
        this.installments = [];
        this.isSelling = false;
        this.productsSale = false;
        this.inscriptionParticipantData = [];
        this.selectedSaleInstallment = -1;
        this.purchaser = undefined;
        this._paymentMethod = undefined;
        this._paymentSaleMethod = undefined;
        this._deliveryMethod = undefined;
        this._grossValue = undefined;
    }
    /**
     * Sender hash é utilizado nas compras de pagseguro pra identificar o comprador.
     */
    initSenderHash() {
        this.senderHash = new Promise(ok => {
            PagSeguroDirectPayment.onSenderHashReady((response) => {
                if (response.status == 'error') {
                    console.log(response);
                    return false;
                }
                ok(response.senderHash); //Hash estará disponível nesta variável.
                return true;
            });
        });
    }
    startPagseguroSession() {
        return this.http.get(this.serverUrl + 'api/getIdSession');
    }
    //forma robusta de carregar pagseguro, garante carregar script e depois carregar sessionID
    pagseguroResolve() {
        if (this.pagseguroLoad) {
            return new Promise(ok => ok(true));
        }
        else {
            //Verifica se já foi iniciado o carregamento
            if (!this.pagseguroPromise) {
                this.pagseguroPromise = new Promise((ok, reject) => {
                    let script = document.createElement('script');
                    script.addEventListener('load', () => {
                        this.startPagseguroSession().subscribe(result => {
                            if (result.session) {
                                this.sessionId = result.session.id;
                                PagSeguroDirectPayment.setSessionId(result.session.id);
                                this.initSenderHash();
                                //this.senderHash = PagSeguroDirectPayment.getSenderHash();
                                //Terminado carregamento, ajustando variáveis
                                this.pagseguroLoad = true;
                                this.pagseguroPromise = undefined;
                                ok(true);
                            }
                            else {
                                reject("O PagSeguro não está disponível. Tente novamente mais tarde.");
                            }
                        });
                    });
                    script.src = this.pagseguroUrl;
                    document.head.appendChild(script);
                });
            }
            return this.pagseguroPromise;
        }
    }
    // Carregamento do mercadopago
    mercadopagoResolve() {
        if (this.mercadopagoLoad) {
            return new Promise(ok => ok(true));
        }
        else {
            //Verifica se já foi iniciado o carregamento
            if (!this.mercadopagoPromise) {
                this.mercadopagoPromise = new Promise(ok => {
                    let script = document.createElement('script');
                    script.addEventListener('load', () => {
                        Mercadopago.setPublishableKey(this.mercadopagoKey);
                        //Terminado carregamento, ajustando variáveis
                        this.mercadopagoLoad = true;
                        this.mercadopagoPromise = undefined;
                        ok(true);
                    });
                    script.src = "https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js";
                    document.head.appendChild(script);
                });
            }
            return this.mercadopagoPromise;
        }
    }
    // Busca valor das parcelas. Deve carregar primeiro o pagseguro/mercadopago
    getInstallments(cardNumber) {
        if (cardNumber) {
            cardNumber = cardNumber.replace(/ /g, "");
            // console.log("PEGANDO INSTALLMENTS", cardNumber);
            return new Promise((success, error) => {
                const amount = this.value;
                if (this.platform == "pagseguro" && this.pagseguroLoad) {
                    this.getBrand(cardNumber).then(brand => {
                        this.payment.credit.brand = brand;
                        let pagseguroParams = {
                            amount,
                            brand: brand,
                            success: (response) => {
                                this.installments = response.installments[brand];
                                success(response.installments[brand]);
                            },
                            error: (response) => {
                                error(response);
                                console.error("Falhou no getInstallments", response);
                            },
                        };
                        if (this.eventDetail) {
                            if (this.moderninha) {
                                if (this.eventDetail.physicalInstallmentsWithoutInterest > 1) {
                                    pagseguroParams["maxInstallmentNoInterest"] = this.eventDetail.physicalInstallmentsWithoutInterest;
                                }
                            }
                            else {
                                if (this.eventDetail.installmentsWithoutInterest > 1) {
                                    pagseguroParams["maxInstallmentNoInterest"] = this.eventDetail.installmentsWithoutInterest;
                                }
                            }
                        }
                        PagSeguroDirectPayment.getInstallments(pagseguroParams);
                    }).catch(fail => {
                        error(fail);
                        console.log("falhou o brand", fail);
                    });
                }
                else if (this.platform == "mercadopago" && this.mercadopagoLoad) {
                    const bin = cardNumber.substring(0, 6);
                    Mercadopago.getInstallments({
                        bin,
                        amount
                    }, (status, response) => {
                        if (status >= 200 && status < 300) {
                            this.installments = response[0].payer_costs.map(data => {
                                return {
                                    quantity: data.installments,
                                    installmentAmount: data.installment_amount,
                                    totalAmount: data.total_amount,
                                    interestFree: data.installment_rate == 0,
                                    cardProvider: response[0].payment_method_id,
                                };
                            });
                            success(this.installments);
                        }
                        else {
                            error(response);
                            console.log("falhou as parcelas mercadopago");
                        }
                    });
                }
                else {
                    error('Biblioteca de pagamento não carregada');
                    console.log(`sem ${this.platform}`);
                }
            });
        }
        else {
            //Obter parcelas pagseguro sem informação do cartão.
            return new Promise((success, error) => {
                let pagseguroParams = {
                    amount: this.value,
                    success: (response) => {
                        //O pagseguro responde com vários tipos de cartões. Verifiquei que os mais comuns respondem com a mesma quantidade de parcelas.
                        //Uma abordagem mais segura seria sempre usar a resposta com o menor número de parcelas.
                        this.installments = response.installments["visa"];
                        success(response.installments["visa"]);
                    },
                    error: (response) => {
                        error(response);
                        console.error("Falhou no getInstallments sem cartão", response);
                    }
                };
                if (this.eventDetail) {
                    if (this.moderninha) {
                        if (this.eventDetail.physicalInstallmentsWithoutInterest > 1) {
                            pagseguroParams["maxInstallmentNoInterest"] = this.eventDetail.physicalInstallmentsWithoutInterest;
                        }
                    }
                    else {
                        if (this.eventDetail.installmentsWithoutInterest > 1) {
                            pagseguroParams["maxInstallmentNoInterest"] = this.eventDetail.installmentsWithoutInterest;
                        }
                    }
                }
                PagSeguroDirectPayment.getInstallments(pagseguroParams);
            });
        }
    }
    //pegar a bandeira do cartão (pagseguro)
    getBrand(cardNumber) {
        return new Promise((success, error) => {
            PagSeguroDirectPayment.getBrand({
                cardBin: cardNumber,
                success: (response) => {
                    success(response.brand.name);
                },
                error: (response) => {
                    console.error(response);
                    error(response);
                }
            });
        });
    }
    /**
     * Validação dos dados do usuário.
     */
    personalDataValidate() {
        if (!this.user)
            return false;
        let actualPersonalValidate = true;
        if (this.user.email.includes("@facebook.com") || !this.user.email) {
            actualPersonalValidate = false;
            this.user.email = ""; //Limpando email
        }
        if (!this.user.phone) {
            actualPersonalValidate = false;
        }
        if (!this.user.cpf) {
            actualPersonalValidate = false;
        }
        if (!this.user.birthdate) {
            actualPersonalValidate = false;
        }
        return actualPersonalValidate;
    }
    /**
     * Função que carrega os dados do evento antes de iniciar a venda.
     * @param eventId O ID do evento.
     * @param userId O ID do usuário. Os dados do usuário são baixados apenas se o parâmetro for fornecido.
     * @throws GENERAL_ERROR se a função for chamada antes de salvar os dados do carrinho de compras.
     */
    eventPurchaseData(eventId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!userId && this.user)
                userId = this.user.id;
            const eventDetail = yield this.getEventDetail(eventId);
            this.setValue();
            if (userId) {
                yield this.getUserDetail(eventDetail.event.clientId, userId);
            }
            return true;
        });
    }
    getEventDetail(eventId) {
        return __awaiter(this, void 0, void 0, function* () {
            const eventDetail = yield this.server.talkToServer("event_detail/" + encodeURIComponent(eventId), { ignoreVisit: true });
            this.eventDetail = eventDetail;
            return eventDetail;
        });
    }
    getUserDetail(companyId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.server.talkToServer('user_data', { company_id: companyId });
            this.user = user;
            return user;
        });
    }
    /**
     * Função que obtem os parâmetros básicos para criar uma compra no servidor.
     * @returns `null` Caso o usuário não esteja registrado, e um `Object` com os dados em caso contrário.
     */
    getPaymentParams() {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.user)
                return null;
            let totalPrice = 0;
            let totalFinalPrice = 0;
            let productNumber = 1;
            const data = {};
            let description = "";
            // Adicionando dados de todos os produtos
            for (const item of this.cart.items) {
                if (item.qtd > 0) {
                    data['itemId' + productNumber] = item.ticket.id.toString();
                    data['itemDescription' + productNumber] = item.ticket.name;
                    data['itemAmount' + productNumber] = valueRound(item.ticket.final_price, '.');
                    data['itemClientAmount' + productNumber] = valueRound(item.ticket.price, '.');
                    data['itemQuantity' + productNumber] = item.qtd.toString();
                    data['itemReservationId' + productNumber] = item.reservations.map(r => r.id);
                    if (this.inscriptionParticipantData) {
                        data['itemParticipants' + productNumber] = "";
                        for (let i = 0; i < this.inscriptionParticipantData.length; i++) {
                            const participantData = this.inscriptionParticipantData[i];
                            data['itemParticipants' + productNumber] += `name:${participantData.name}`;
                            data['itemParticipants' + productNumber] += `,email:${participantData.email}`;
                            if (participantData.cpf)
                                data['itemParticipants' + productNumber] += `,cpf:${participantData.cpf}`;
                            if (participantData.phone)
                                data['itemParticipants' + productNumber] += `,phone:${participantData.phone}`;
                            if (participantData.birthdate)
                                data['itemParticipants' + productNumber] += `,birthdate:${participantData.birthdate}`;
                            for (let i = 0; i < (participantData.extra_fields ? Object.keys(participantData.extra_fields).length : 0); i++) {
                                const formFieldId = parseInt(Object.keys(participantData.extra_fields)[i]);
                                data['itemParticipants' + productNumber] += `,extra_fields/${formFieldId}:${participantData.extra_fields[formFieldId]}`;
                            }
                            if (i < this.inscriptionParticipantData.length - 1)
                                data['itemParticipants' + productNumber] += "§";
                        }
                    }
                    productNumber++;
                    totalPrice += item.ticket.price * item.qtd;
                    totalFinalPrice += item.ticket.final_price * item.qtd;
                    if (description != "")
                        description += " | ";
                    description += item.qtd.toString() + "x " + item.ticket.name;
                }
            }
            data['clientId'] = this.eventDetail.event.clientId.toString();
            data['senderHash'] = yield this.senderHash;
            data['fullName'] = this.user.name;
            data['name'] = this.user.name;
            if (this.user.cpf) {
                data['usercpf'] = this.user.cpf.replace(/\.|-/g, '');
                data['cpf'] = this.user.cpf.replace(/\.|-/g, '');
            }
            else {
                data['usercpf'] = '';
                data['cpf'] = '';
            }
            data['email'] = this.user.email;
            if (this.user.phone) {
                data['completePhone'] = this.user.phone.replace(/[(_)-\s]/g, '');
                data['areaCode'] = data['completePhone'].substr(0, 2);
                data['phone'] = data['completePhone'].substring(2);
            }
            else {
                data['completePhone'] = '';
                data['areaCode'] = '';
                data['phone'] = '';
            }
            data['senderBirthdate'] = this.user.birthdate;
            data['birthday'] = data['senderBirthdate'];
            data['userId'] = this.user.id.toString();
            data['changeUser'] = false;
            data['eventId'] = this.eventDetail.event.id;
            data['purchaseId'] = (_a = this.purchase) === null || _a === void 0 ? void 0 : _a.id;
            data['companyUserId'] = this.user.companyUserId;
            data['totalPrice'] = valueRound(totalPrice, '.');
            data['totalFinalPrice'] = valueRound(totalFinalPrice, '.');
            data['eventName'] = this.eventDetail.event.name;
            data['origin'] = this.origin;
            if (this.discount)
                data['discountId'] = this.discount.id;
            if (this.eventDetail.paymentMethod == "mercadopago") {
                data["transaction_amount"] = totalFinalPrice;
                data["description"] = description;
            }
            return data;
        });
    }
    /**************************************************
    INICIA AS COMPRAS
    Chamado após escolher os ingressos e clicar em comprar
    @throws VALIDATE_EMAIL caso a compra seja paga e o usuário ainda não tiver validado seu email.
    @throws UNAUTHENTICATED caso o id de usuário não seja informado.
    @throws TICKET_UNAVAILABLE caso o ingresso solicitado não esteja mais disponível.
    @param cart O carrinho de compras, com o desconto, caso exista.
    @param eventId O ID do evento.
    @param userId O ID do usuário, caso ele esteja logado no sistema.
    @param cleanPurchaseCache Se os dados da compra devem ser limpos do cache. `true` por padrão, só deve ser sobrescrito se necessário.
    **************************************************/
    init(cart, eventId, userId, cleanPurchaseCache = true) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function* () {
            this.cleanCache(cleanPurchaseCache);
            this.isSelling = false;
            this.cart = cart;
            yield this.eventPurchaseData(eventId, userId);
            if (this.value > 0) {
                if (this.platform == "pagseguro") {
                    if (!(yield this.pagseguroResolve())) {
                        throw new OutPayError("Não carregou Pagseguro");
                    }
                }
                else {
                    if (!(yield this.mercadopagoResolve())) {
                        throw new OutPayError("Não carregou Mercadopago");
                    }
                }
                for (let item of this.cart.items) {
                    if (item.ticket.price == 0)
                        item.qtd = 0;
                }
            }
            else {
                //Compras sem valor são consideradas gratuitas e tem seus itens ajustados para valor 0
                this._paymentMethod = "free";
                if (!this.cart.discount || this.cart.discount.value != 100) {
                    for (let item of this.cart.items) {
                        if (item.ticket.price > 0)
                            item.qtd = 0;
                    }
                }
            }
            if (!userId)
                throw new OutPayError("Você não está logado.", OutPayErrorCode.UNAUTHENTICATED);
            try {
                let response = yield this.server.talkToServer("doublecheck_tickets", { cart: this.cart.items, params: yield this.getPaymentParams() }, { type: "POST", retries: 4 });
                if (response.error) {
                    if ("ticket_name" in response) {
                        throw new OutPayError(`Limite do ingresso ${response.ticket_name} atingido. Selecione uma nova quantidade.`, OutPayErrorCode.TICKET_UNAVAILABLE, { otto: true, ottoData: { image: "atento" } });
                    }
                    if ("errorMsg" in response) {
                        throw new OutPayError(response.errorMsg, OutPayErrorCode.GENERAL_ERROR, { otto: true, ottoData: { image: "atento" } });
                    }
                    // Se não souber qual foi o erro, jogar para evitar erros silenciosos.
                    throw response;
                }
                else {
                    if (response.purchase)
                        this.purchase = response.purchase;
                    if (((_a = this.eventDetail) === null || _a === void 0 ? void 0 : _a.paymentMethod) == "mercadopago") {
                        this.ticketData = response.mercadopago;
                    }
                    else {
                        this.ticketData = response.pagseguro;
                    }
                    if (this.value > 0 && this.personalDataValidate() && !this.eventDetail.have_participant_form) {
                        return { finished: false, nextStep: OutPayStep.PAYMENT_METHOD, purchase: this.purchase };
                    }
                    else {
                        return { finished: false, nextStep: OutPayStep.PURCHASE_FORM, purchase: this.purchase };
                    }
                }
            }
            catch (error) {
                if (error.status == 400) {
                    if (error.error == "verify_email" || error.error.verify_email) {
                        //Deve recuperar os dados de compra, pois mesmo com o erro já criou a pre-reserva
                        if (error.error.purchase)
                            this.purchase = error.error.purchase;
                        if (((_b = this.eventDetail) === null || _b === void 0 ? void 0 : _b.paymentMethod) == "mercadopago") {
                            this.ticketData = error.error.mercadopago;
                        }
                        else {
                            this.ticketData = error.error.pagseguro;
                        }
                        throw new OutPayError(`Enviamos um código para ${this.user.email} para você validar abaixo.`, OutPayErrorCode.VALIDATE_EMAIL);
                    }
                    if (error.error.error) {
                        throw new OutPayError(error.error.errorMsg, OutPayErrorCode.GENERAL_ERROR);
                    }
                }
                else if (error.status == 401) {
                    throw new OutPayError("Você não está logado.", OutPayErrorCode.UNAUTHENTICATED);
                }
                // Evitar erros silenciosos
                throw error;
            }
        });
    }
    /**************************************************
    INICIA O PROCESSO DO VENDEDOR
    Chamado após escolher os ingressos e clicar em vender
    **************************************************/
    sellerInit(cart, eventId, userId) {
        return __awaiter(this, void 0, void 0, function* () {
            this.cleanCache();
            this.isSelling = true;
            this.cart = cart;
            yield this.eventPurchaseData(eventId, userId);
            return yield this.nextStepSale();
        });
    }
    /**************************************************
    INICIA O PROCESSO DO VENDEDOR DE PRODUTOS
    Chamado após escolher os produtos e clicar em vender
    **************************************************/
    productSellerInit(productCart, eventDetail, user) {
        this.cleanCache();
        this.eventDetail = eventDetail;
        this.isSelling = true;
        this.productsSale = true;
        this.user = user;
        this.productCart = productCart;
        this.setValue();
        return { finished: false, nextStep: OutPayStep.PAYMENT_METHOD, purchase: this.purchase };
    }
    /**
     * saveFormData
     * Chamado após preencher os dados de formulário.
     */
    saveFormData(participantData) {
        return __awaiter(this, void 0, void 0, function* () {
            this.inscriptionParticipantData = participantData;
            if (this.isSelling) {
                if (this.deliveryMethod == "digital" && this.inscriptionParticipantData.length > 0) {
                    try {
                        this.purchaser = yield this.server.talkToServer("purchaser", { email: this.inscriptionParticipantData[0].email }, { retries: 4 });
                    }
                    catch (error) {
                        if (error.error == "name") {
                            this.purchaser = yield this.server.talkToServer("purchaser/create", {
                                name: this.inscriptionParticipantData[0].name,
                                email: this.inscriptionParticipantData[0].email,
                                phone: this.inscriptionParticipantData[0].phone,
                                cpf: this.inscriptionParticipantData[0].cpf,
                                birthdat: this.inscriptionParticipantData[0].birthdate
                            }, {
                                type: "POST",
                                retries: 4,
                            });
                        }
                        else {
                            throw error;
                        }
                    }
                }
                return yield this.nextStepSale();
            }
            else {
                if (this.value > 0) {
                    return { finished: false, nextStep: OutPayStep.PAYMENT_METHOD, purchase: this.purchase };
                }
                else {
                    return { finished: false, nextStep: OutPayStep.FINISH, purchase: this.purchase };
                }
            }
        });
    }
    /**
     * selectPaymentMethod
     * Chamada após selecionar método de pagamento para compras online.
     */
    selectPaymentMethod(paymentMethod) {
        this._paymentMethod = paymentMethod;
        if (paymentMethod == "credit") {
            this.payment.type = PaymentType.creditCard;
        }
        if (this.paymentMethod == "credit" || this.needsBuyerData) {
            return { finished: false, nextStep: OutPayStep.PAYMENT_DATA, purchase: this.purchase };
        }
        else {
            return { finished: false, nextStep: OutPayStep.FINISH, purchase: this.purchase };
        }
    }
    /**
     * Chamada após selecionar o método de pagamento para vendas.
     * @param paymentSaleMethod Método de pagamento da venda.
     * @param usingExternalAppForPayment Opcional. Permite que o outpay prossiga em uma venda de cartão sem usar moderninha.
     */
    selectSalePaymentMethod(paymentSaleMethod, usingExternalAppForPayment = false) {
        return __awaiter(this, void 0, void 0, function* () {
            this._paymentSaleMethod = paymentSaleMethod;
            if (this.moderninha && [PaymentSaleMethod.CREDIT, PaymentSaleMethod.DEBIT].includes(paymentSaleMethod)) {
                //Aplicar taxas de venda em cartão no preço da moderninha.
                this.setValue();
                if (this.eventDetail.cardSalesOnApp) {
                    try {
                        yield this.pagseguroResolve();
                    }
                    catch (_a) {
                        throw new OutPayError("Erro ao iniciar pagseguro", OutPayErrorCode.GENERAL_ERROR);
                    }
                }
                else {
                    if (usingExternalAppForPayment) {
                        this.purchase = yield this.createPurchase(true);
                    }
                    else {
                        throw new OutPayError('Use o app "Vendas" ou outra máquina e realize o pagamento no cartão para continuar.', OutPayErrorCode.UNABLE_TO_PROCEED);
                    }
                }
            }
            return yield this.nextStepSale();
        });
    }
    /**
     * Chamada após selecionar a quantidade de parcelas em uma venda de cartão de crédito.
     * @param selectedInstallment O índice da quantidade de parcelas, iniciando em zero.
     */
    selectSaleInstallments(selectedInstallment) {
        return __awaiter(this, void 0, void 0, function* () {
            this.selectedSaleInstallment = selectedInstallment;
            this._grossValue = this.installments[selectedInstallment].totalAmount;
            return yield this.nextStepSale();
        });
    }
    /**
     * Chamada após selecionar o método de entrega da venda.
     * @param deliveryMethod O método de entrega da venda.
     */
    selectDeliveryMethod(deliveryMethod) {
        return __awaiter(this, void 0, void 0, function* () {
            this._deliveryMethod = deliveryMethod;
            return yield this.nextStepSale();
        });
    }
    /**
     * Chamada para determinar o comprador em uma venda digital sem formulário.
     * @param email O email do comprador.
     * @param name O nome do comprador, para criar um novo usuário.
     * @throws NOT_FOUND caso seja informado apenas o email, e não exista usuário com esse email.
     */
    selectPurchaser(email, name) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (!name) {
                    this.purchaser = yield this.server.talkToServer("purchaser", { email }, { retries: 4 });
                }
                else {
                    this.purchaser = yield this.server.talkToServer("purchaser/create", { email, name }, {
                        type: "POST",
                        retries: 4,
                    });
                }
            }
            catch (error) {
                if (error.error == "name")
                    throw new OutPayError(error.error, OutPayErrorCode.NOT_FOUND);
                else
                    throw new OutPayError(error, OutPayErrorCode.GENERAL_ERROR);
            }
            return this.nextStepSale();
        });
    }
    /**
     * finishPurchase
     * Chamada após digitar os dados de pagamento.
     * @throws PURCHASE_ERROR quando ocorre um erro na compra.
     */
    finishPurchase(cardToken) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.value > 0 && this.eventDetail.paymentMethod == "pagseguro" && this.paymentMethod == "credit") {
                return yield this.pagseguroCreditCard();
            }
            else {
                return yield this.finalizePurchase(cardToken);
            }
        });
    }
    finishSale() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.purchase) {
                this.purchase = yield this.createPurchase();
            }
            else if (this.purchase.status < 4) {
                this.purchase.status = 4;
                yield this.server.talkToServer("purchase/" + this.purchase.id, this.purchase, {
                    type: "PUT",
                    retries: 4,
                });
            }
            return this.nextStepSale();
        });
    }
    //serve para tratar cartão tanto do pagseguro quanto do mercadopago
    pagseguroCreditCard() {
        return new Promise((resolve, reject) => {
            const param = {
                cardNumber: this.payment.credit.number.replace(/ /g, ""),
                brand: this.payment.credit.brand,
                cvv: this.payment.credit.cvv,
                expirationMonth: this.payment.credit.validity.substr(0, 2),
                expirationYear: '20' + this.payment.credit.validity.substr(3, 2),
                error: (error) => {
                    reject(error);
                },
                success: (response) => {
                    this.finalizePurchase(response.card.token).then(result => {
                        resolve(result);
                    }, (error) => reject(error));
                }
            };
            PagSeguroDirectPayment.createCardToken(param);
        });
    }
    finalizePurchase(cardToken) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            if (this.user) {
                if (!this.user.cpf)
                    this.user.cpf = this.payment.credit.cpf;
                if (!this.user.birthdate)
                    this.user.birthdate = this.payment.credit.birthdate;
                if (!this.user.phone)
                    this.user.phone = this.payment.credit.phone;
            }
            const data = yield this.getPaymentParams();
            if (cardToken) {
                const installments = this.installments[this.payment.credit.installments];
                data['paymentMethod'] = 'creditCard';
                if (this.eventDetail.paymentMethod == "mercadopago") {
                    data["payment_method_id"] = installments.cardProvider;
                }
                data['street'] = this.payment.credit.address.street;
                data['number'] = this.payment.credit.address.number;
                data['zip'] = this.payment.credit.address.zip;
                data['district'] = this.payment.credit.address.district;
                data['city'] = this.payment.credit.address.city;
                data['stateCode'] = this.payment.credit.address.stateCode;
                data['complement'] = '';
                data['installmentsQtd'] = installments.quantity.toString();
                data['installmentsValue'] = valueRound(installments.installmentAmount, '.');
                data['code'] = cardToken;
                data['cardName'] = this.payment.credit.name;
                data['cardCpf'] = this.payment.credit.cpf;
                data['cardPhone'] = this.payment.credit.phone;
                data['pagamento'] = '' + installments.quantity + ',' + installments.installmentAmount + ',' + installments.totalAmount;
            }
            else if (this.paymentMethod == "boleto") {
                data['paymentMethod'] = 'boleto';
            }
            else {
                data['paymentMethod'] = 'free';
            }
            data['ticketData'] = this.ticketData;
            this.inscriptionParticipantData = [];
            try {
                const purchase = yield this.doPayment(data);
                this.purchase = purchase;
                return { finished: true, nextStep: OutPayStep.FINISH, purchase: this.purchase };
            }
            catch (error) {
                if (error.errorCode)
                    throw error;
                throw new OutPayError(((_a = error.error) === null || _a === void 0 ? void 0 : _a.text) || "Houve um erro na compra", OutPayErrorCode.PURCHASE_ERROR);
            }
        });
    }
    doPayment(data) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            let response;
            try {
                if (this.value == 0) {
                    response = yield this.server.talkToServer("paymentMercadopago", data, { type: "POST" });
                }
                else if (this.eventDetail.paymentMethod == "mercadopago") {
                    response = yield this.http.post(this.serverUrl + 'api/paymentMercadopago', data).toPromise();
                }
                else {
                    response = yield this.http.post(this.serverUrl + 'api/payment', data).toPromise();
                }
            }
            catch (error) {
                throw new OutPayError(((_a = error.error) === null || _a === void 0 ? void 0 : _a.text) || "Houve um erro na compra", OutPayErrorCode.PURCHASE_ERROR);
            }
            if (response.error) {
                throw new OutPayError(response.errorMsg, OutPayErrorCode.PURCHASE_ERROR);
            }
            return response.purchase;
        });
    }
    createPurchase(unpaidPurchase = false) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = { purchaser_id: this.purchaser ? this.purchaser.id : null, payment_method: this.paymentSaleMethod };
            if (this.cart.discount)
                data.discount_id = this.cart.discount.id;
            if (this.cart) {
                const ticketList = {};
                const reservationList = {};
                this.cart.items.forEach(item => {
                    ticketList[item.ticket.id] = item.qtd;
                    if (item.reservations.length > 0)
                        reservationList[item.ticket.id] = item.reservations.map((r) => r.id);
                });
                data.cart = ticketList;
                if (Object.getOwnPropertyNames(reservationList).length > 0)
                    data.cart_reservation = reservationList;
            }
            if (this.productCart) {
                data.product_cart = this.productCart.items;
            }
            if (this.inscriptionParticipantData)
                data.participant_data = this.inscriptionParticipantData;
            if (this.deliveryMethod)
                data.purchase_type = this.deliveryMethod;
            if (unpaidPurchase)
                data.unpaid_purchase = unpaidPurchase;
            data.is_selling = this.isSelling;
            if (this.moderninha) {
                data.moderninha = true;
            }
            return this.server.talkToServer("purchase/create", data, {
                type: "POST",
                retries: 4,
            });
        });
    }
    /**
     * Função que analísa a compra, e retorna o próximo input necessário pelo usuário.
     * @returns Um objeto `OutPayResult`, com o próximo passo e objeto da compra.
     */
    nextStepSale() {
        return __awaiter(this, void 0, void 0, function* () {
            const result = { finished: false, nextStep: -1, purchase: this.purchase };
            //Se a compra for paga, verificar método de pagamento e parcelamento.
            if (this.value > 0) {
                if (this.paymentSaleMethod == undefined) {
                    result.nextStep = OutPayStep.PAYMENT_METHOD;
                    return result;
                }
                else {
                    if (this.moderninha && this.paymentSaleMethod == PaymentSaleMethod.CREDIT) {
                        if (this.selectedSaleInstallment < 0) {
                            result.nextStep = OutPayStep.INSTALLMENTS;
                        }
                    }
                }
            }
            else {
                if (this.paymentSaleMethod == undefined) {
                    this._paymentSaleMethod = PaymentSaleMethod.FREE;
                }
            }
            //Depois, verificar o método de entrega. Caso só seja possível um, este é selecionado.
            if (result.nextStep < 0) {
                if (!this.deliveryMethod) {
                    if (this.moderninha && (this.productsSale || this.value == 0)) {
                        this._deliveryMethod = "physical";
                    }
                    else {
                        if (this.moderninha && this.eventDetail.allow_physical_sales && this.eventDetail.allow_digital_sales) {
                            result.nextStep = OutPayStep.DELIVERY_METHOD;
                        }
                        else if (this.moderninha && this.eventDetail.allow_physical_sales) {
                            this._deliveryMethod = "physical";
                        }
                        else if (this.eventDetail.allow_digital_sales) {
                            this._deliveryMethod = "digital";
                        }
                        else {
                            throw new OutPayError("Houve um problema para continuar a venda. Fale com o administrador do evento.", OutPayErrorCode.PURCHASE_ERROR, { otto: true, ottoData: { image: "triste", duration: 3000 } });
                        }
                    }
                }
            }
            //O próximo passo é verificar dados dos compradores, via formulário ou tela do comprador.
            //Vendas de produtos não precisam de informação sobre comprador.
            if (result.nextStep < 0 && !this.productsSale) {
                if (this.eventDetail.have_participant_form || this.value == 0) {
                    if (!this.inscriptionParticipantData.length) {
                        result.nextStep = OutPayStep.PURCHASE_FORM;
                    }
                }
                else if (this.deliveryMethod == "digital" && !this.eventDetail.have_participant_form) {
                    if (!this.purchaser) {
                        result.nextStep = OutPayStep.PURCHASER;
                    }
                }
            }
            //Caso a compra seja feita no cartão, o app deverá ir para a tela de cobranças.
            if (result.nextStep < 0) {
                if (this.moderninha && this.paymentSaleMethod && [PaymentSaleMethod.CREDIT, PaymentSaleMethod.DEBIT].includes(this.paymentSaleMethod)) {
                    if (!this.purchase) {
                        this.purchase = yield this.createPurchase(true);
                        result.purchase = this.purchase;
                        result.nextStep = OutPayStep.PLUGPAG;
                    }
                }
            }
            //Caso seja uma compra física que não foi finalizada ainda, ela precisa ser impressa.
            if (result.nextStep < 0) {
                if (this.deliveryMethod == "physical") {
                    if (!this.purchase) {
                        this.purchase = yield this.createPurchase(true);
                        result.purchase = this.purchase;
                    }
                    if (this.purchase.status < 4) {
                        result.nextStep = OutPayStep.PRINT;
                    }
                    else {
                        result.nextStep = OutPayStep.FINISH;
                    }
                }
            }
            //Por fim, a venda é encerrada.
            if (result.nextStep < 0) {
                if (!this.purchase || this.purchase.status < 4)
                    yield this.finishSale();
                result.purchase = this.purchase;
                result.finished = true;
                result.nextStep = OutPayStep.FINISH;
            }
            return result;
        });
    }
}
OutPayService.ɵfac = function OutPayService_Factory(t) { return new (t || OutPayService)(i0.ɵɵinject(i1.HttpClient)); };
OutPayService.ɵprov = i0.ɵɵdefineInjectable({ token: OutPayService, factory: OutPayService.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(OutPayService, [{
        type: Injectable,
        args: [{ providedIn: 'root' }]
    }], function () { return [{ type: i1.HttpClient }]; }, null); })();
export var OutPayStep;
(function (OutPayStep) {
    /** Seleção de forma de pagamento */
    OutPayStep[OutPayStep["PAYMENT_METHOD"] = 0] = "PAYMENT_METHOD";
    /** Formulário de dados do comprador e dos participantes */
    OutPayStep[OutPayStep["PURCHASE_FORM"] = 1] = "PURCHASE_FORM";
    /** Impressão do ingresso, nos casos onde o pagamento não ocorre via PlugPag (ie. Dinheiro) */
    OutPayStep[OutPayStep["PRINT"] = 2] = "PRINT";
    /** Finalizar processo de compra */
    OutPayStep[OutPayStep["FINISH"] = 3] = "FINISH";
    /** Formulário de dados do pagamento (cartão) */
    OutPayStep[OutPayStep["PAYMENT_DATA"] = 4] = "PAYMENT_DATA";
    /** Seleção de forma de recebimento */
    OutPayStep[OutPayStep["DELIVERY_METHOD"] = 5] = "DELIVERY_METHOD";
    /** Seleção do parcelamento do pagamento */
    OutPayStep[OutPayStep["INSTALLMENTS"] = 6] = "INSTALLMENTS";
    /** Pagamento via PlugPag */
    OutPayStep[OutPayStep["PLUGPAG"] = 7] = "PLUGPAG";
    /** Identificação do comprador para vendas digitais sem formulário */
    OutPayStep[OutPayStep["PURCHASER"] = 8] = "PURCHASER";
})(OutPayStep || (OutPayStep = {}));
export class OutPayError {
    constructor(msg, errorCode = OutPayErrorCode.GENERAL_ERROR, data = {}) {
        this.msg = msg;
        this.errorCode = errorCode;
        this.data = data;
    }
    ;
    get ottoAlert() {
        if (this.data.otto) {
            return {
                text: this.msg,
                image: this.data.ottoData.image,
                duration: this.data.ottoData.duration,
            };
        }
        else
            throw new Error("Não há mensagem do otto neste erro");
    }
}
export var OutPayErrorCode;
(function (OutPayErrorCode) {
    /** Erro genérico */
    OutPayErrorCode[OutPayErrorCode["GENERAL_ERROR"] = 1] = "GENERAL_ERROR";
    /** O usuário precisa validar o email para prosseguir */
    OutPayErrorCode[OutPayErrorCode["VALIDATE_EMAIL"] = 2] = "VALIDATE_EMAIL";
    /** O ingresso solicitado já está esgotado */
    OutPayErrorCode[OutPayErrorCode["TICKET_UNAVAILABLE"] = 3] = "TICKET_UNAVAILABLE";
    /** Houve um erro ao processar a compra */
    OutPayErrorCode[OutPayErrorCode["PURCHASE_ERROR"] = 4] = "PURCHASE_ERROR";
    /** O usuário não está logado no sistema */
    OutPayErrorCode[OutPayErrorCode["UNAUTHENTICATED"] = 5] = "UNAUTHENTICATED";
    /** Não é possível executar a ação, um caminho alternativo deve ser buscado */
    OutPayErrorCode[OutPayErrorCode["UNABLE_TO_PROCEED"] = 6] = "UNABLE_TO_PROCEED";
    /** Não foi encontrado um resultado para os dados fornecidos */
    OutPayErrorCode[OutPayErrorCode["NOT_FOUND"] = 7] = "NOT_FOUND";
})(OutPayErrorCode || (OutPayErrorCode = {}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3V0LXBheS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvb3V0Z28vb3V0LXBheS9zcmMvbGliL291dC1wYXkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUErRCxPQUFPLEVBQWlGLFdBQVcsRUFBb0MsaUJBQWlCLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDaFAsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUczQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sUUFBUSxDQUFDOzs7QUFNcEMsTUFBTSxPQUFPLGFBQWE7SUFDdkIsWUFDVSxJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBSTFCLGNBQWM7UUFDUCxjQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ2YsaUJBQVksR0FBRyxFQUFFLENBQUM7UUFDbEIsbUJBQWMsR0FBRyxFQUFFLENBQUM7UUFDcEIsZUFBVSxHQUFHLEtBQUssQ0FBQztRQUVuQixXQUFNLEdBQWdCLFdBQVcsQ0FBQztRQUlsQyxTQUFJLEdBQVMsRUFBQyxLQUFLLEVBQUUsRUFBRSxFQUFDLENBQUM7UUFDekIsWUFBTyxHQUFZLElBQUksT0FBTyxFQUFFLENBQUM7UUFFakMsaUJBQVksR0FBbUIsRUFBRSxDQUFDO1FBQ2xDLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsaUJBQVksR0FBRyxLQUFLLENBQUM7UUFDckIsZ0JBQVcsR0FBZ0IsRUFBQyxLQUFLLEVBQUUsRUFBRSxFQUFDLENBQUM7UUFDdkMsK0JBQTBCLEdBQTJKLEVBQUUsQ0FBQztRQUN4TCw0QkFBdUIsR0FBRyxDQUFDLENBQUM7UUEyQm5DLHVCQUF1QjtRQUNmLFdBQU0sR0FBVyxDQUFDLENBQUM7UUFzRDNCLFlBQVk7UUFDSixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUd0QixvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUVoQyxjQUFTLEdBQUcsRUFBRSxDQUFDO0lBNUdiLENBQUM7SUF3QkgsSUFBVyxhQUFhLEtBQUssT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztJQUFBLENBQUM7SUFFM0QsSUFBVyxpQkFBaUIsS0FBSyxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7SUFBQSxDQUFDO0lBRW5FLElBQVcsY0FBYyxLQUFLLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7SUFBQSxDQUFDO0lBRTdELElBQVcsVUFBVSxLQUFLLE9BQU8sSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFBLENBQUMsQ0FBQztJQUFBLENBQUM7SUFLbEUsSUFBVyxRQUFROztRQUNoQixhQUFPLElBQUksQ0FBQyxXQUFXLDBDQUFFLGFBQWEsQ0FBQztJQUMxQyxDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsSUFBVyxjQUFjO1FBQ3RCLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDeEgsQ0FBQztJQUlELElBQVcsS0FBSztRQUNiLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN0QixDQUFDO0lBRU8sUUFBUTtRQUNiLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxNQUFNO1lBQUUsTUFBTSxJQUFJLFdBQVcsQ0FBQyxvQ0FBb0MsRUFBRSxlQUFlLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFMUosSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3BDLElBQUksU0FBaUIsQ0FBQztZQUV0QixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ2pCLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtvQkFDbEIsU0FBUyxHQUFHLENBQ1QsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7d0JBQ2hGLENBQUMsSUFBSSxDQUFDLGlCQUFpQixJQUFJLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFZLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQ3JNLENBQUE7aUJBQ0g7cUJBQU07b0JBQ0osSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsb0JBQW9CLEVBQUU7d0JBQzdELFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQztxQkFDaEM7eUJBQU07d0JBQ0osU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDO3FCQUN0QztpQkFDSDthQUNIO2lCQUFNO2dCQUNKLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQzthQUN0QztZQUVELE9BQU8sU0FBUyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDL0IsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsS0FBSyxHQUFHLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQztRQUVsRCxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDMUMsTUFBTSxZQUFZLEdBQUcsQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUUzRCxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtnQkFDaEMsTUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLFdBQUMsT0FBQSxFQUFFLENBQUMsTUFBTSxXQUFJLElBQUksQ0FBQyxJQUFJLDBDQUFFLEVBQUUsQ0FBQSxDQUFBLEVBQUEsQ0FBQyxDQUFDO2dCQUNoRixLQUFLLElBQUksWUFBWSxHQUFHLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQSxVQUFVLGFBQVYsVUFBVSx1QkFBVixVQUFVLENBQUUsVUFBVSxFQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2hHO2lCQUFNO2dCQUNKLEtBQUssSUFBSSxZQUFZLENBQUM7YUFDeEI7UUFDSixDQUFDLENBQUMsQ0FBQTtRQUVGLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRO1lBQUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLEdBQUcsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxDQUFBOztZQUNqRixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUM1QixDQUFDO0lBRUQsSUFBVyxRQUFRO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDN0IsQ0FBQztJQUVELElBQVcsWUFBWTtRQUNwQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDaEUsQ0FBQztJQVVNLEtBQUssQ0FBQyxTQUFpQixFQUFFLFlBQW9CLEVBQUUsY0FBc0IsRUFBRSxNQUFtQixFQUFFLE1BQWMsRUFBRSxPQUErQixFQUFFO1FBQ2pKLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1FBQzNCLElBQUksQ0FBQyxZQUFZLEdBQUcsWUFBWSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxjQUFjLEdBQUcsY0FBYyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBRXJCLElBQUksSUFBSSxDQUFDLFVBQVU7WUFBRSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDMUQsQ0FBQztJQUVEOzs7T0FHRztJQUNLLFVBQVUsQ0FBQyxrQkFBa0IsR0FBRyxJQUFJO1FBRXpDLElBQUksa0JBQWtCLEVBQUU7WUFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUM7WUFDN0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUM7U0FDNUI7UUFFRCxJQUFJLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQztRQUN0QixJQUFJLENBQUMsSUFBSSxHQUFHO1lBQ1QsS0FBSyxFQUFFLEVBQUU7U0FDWCxDQUFBO1FBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRztZQUNoQixLQUFLLEVBQUUsRUFBRTtTQUNYLENBQUM7UUFDRixJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUNoQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7UUFDNUIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLDBCQUEwQixHQUFHLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDbEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDM0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxTQUFTLENBQUM7UUFDaEMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLFNBQVMsQ0FBQztRQUNwQyxJQUFJLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQztRQUNqQyxJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQztJQUNoQyxDQUFDO0lBRUQ7O09BRUc7SUFDSyxjQUFjO1FBQ25CLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxPQUFPLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDaEMsc0JBQXNCLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxRQUFpRCxFQUFFLEVBQUU7Z0JBQzVGLElBQUcsUUFBUSxDQUFDLE1BQU0sSUFBSSxPQUFPLEVBQUU7b0JBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3RCLE9BQU8sS0FBSyxDQUFDO2lCQUNmO2dCQUNELEVBQUUsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyx3Q0FBd0M7Z0JBQ2pFLE9BQU8sSUFBSSxDQUFDO1lBQ2YsQ0FBQyxDQUFDLENBQUM7UUFDTixDQUFDLENBQUMsQ0FBQztJQUNOLENBQUM7SUFFTyxxQkFBcUI7UUFDMUIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBbUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxrQkFBa0IsQ0FBQyxDQUFDO0lBQy9FLENBQUM7SUFFRCwwRkFBMEY7SUFDbkYsZ0JBQWdCO1FBQ3BCLElBQUcsSUFBSSxDQUFDLGFBQWEsRUFBQztZQUNuQixPQUFPLElBQUksT0FBTyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDckM7YUFBTTtZQUNKLDRDQUE0QztZQUM1QyxJQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFDO2dCQUN2QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxPQUFPLENBQUMsQ0FBQyxFQUFFLEVBQUUsTUFBTSxFQUFFLEVBQUU7b0JBQ2hELElBQUksTUFBTSxHQUFzQixRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUNqRSxNQUFNLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRTt3QkFDbEMsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxFQUFFOzRCQUM3QyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUU7Z0NBQ2pCLElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7Z0NBQ25DLHNCQUFzQixDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dDQUN2RCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7Z0NBQ3RCLDJEQUEyRDtnQ0FFM0QsNkNBQTZDO2dDQUM3QyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztnQ0FDMUIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFNBQVMsQ0FBQztnQ0FDbEMsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDOzZCQUNYO2lDQUNJO2dDQUNGLE1BQU0sQ0FBQyw4REFBOEQsQ0FBQyxDQUFDOzZCQUN6RTt3QkFDSixDQUFDLENBQUMsQ0FBQztvQkFDTixDQUFDLENBQUMsQ0FBQztvQkFDSCxNQUFNLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7b0JBQy9CLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNyQyxDQUFDLENBQUMsQ0FBQzthQUNMO1lBRUQsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7U0FDL0I7SUFDSixDQUFDO0lBRUQsOEJBQThCO0lBQ3ZCLGtCQUFrQjtRQUN0QixJQUFHLElBQUksQ0FBQyxlQUFlLEVBQUM7WUFDckIsT0FBTyxJQUFJLE9BQU8sQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQ3JDO2FBQU07WUFDSiw0Q0FBNEM7WUFDNUMsSUFBRyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBQztnQkFDekIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksT0FBTyxDQUFDLEVBQUUsQ0FBQyxFQUFFO29CQUN4QyxJQUFJLE1BQU0sR0FBc0IsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDakUsTUFBTSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxHQUFHLEVBQUU7d0JBQ2xDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7d0JBRW5ELDZDQUE2Qzt3QkFDN0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7d0JBQzVCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxTQUFTLENBQUM7d0JBRXBDLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDWixDQUFDLENBQUMsQ0FBQztvQkFDSCxNQUFNLENBQUMsR0FBRyxHQUFHLDhEQUE4RCxDQUFDO29CQUM1RSxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDckMsQ0FBQyxDQUFDLENBQUM7YUFDTDtZQUVELE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDO1NBQ2pDO0lBQ0osQ0FBQztJQUVELDJFQUEyRTtJQUNwRSxlQUFlLENBQUMsVUFBbUI7UUFDdkMsSUFBSSxVQUFVLEVBQUU7WUFDYixVQUFVLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7WUFDMUMsbURBQW1EO1lBRW5ELE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUU7Z0JBQ25DLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBRTFCLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxXQUFXLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtvQkFDckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFXLENBQUMsQ0FBQyxJQUFJLENBQzVCLEtBQUssQ0FBQyxFQUFFO3dCQUNMLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7d0JBRWxDLElBQUksZUFBZSxHQUFROzRCQUN4QixNQUFNOzRCQUNOLEtBQUssRUFBRyxLQUFLOzRCQUNiLE9BQU8sRUFBRSxDQUFDLFFBQTRELEVBQUUsRUFBRTtnQ0FDdkUsSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dDQUNqRCxPQUFPLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDOzRCQUN6QyxDQUFDOzRCQUNELEtBQUssRUFBRSxDQUFDLFFBQWEsRUFBRSxFQUFFO2dDQUN0QixLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7Z0NBQ2hCLE9BQU8sQ0FBQyxLQUFLLENBQUMsMkJBQTJCLEVBQUUsUUFBUSxDQUFDLENBQUM7NEJBQ3hELENBQUM7eUJBQ0gsQ0FBQzt3QkFFRixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7NEJBQ25CLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtnQ0FDbEIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLG1DQUFtQyxHQUFHLENBQUMsRUFBRTtvQ0FDM0QsZUFBZSxDQUFDLDBCQUEwQixDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQ0FBbUMsQ0FBQztpQ0FDckc7NkJBQ0g7aUNBQU07Z0NBQ0osSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLDJCQUEyQixHQUFHLENBQUMsRUFBRTtvQ0FDbkQsZUFBZSxDQUFDLDBCQUEwQixDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQywyQkFBMkIsQ0FBQztpQ0FDN0Y7NkJBQ0g7eUJBQ0g7d0JBRUQsc0JBQXNCLENBQUMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxDQUFDO29CQUMzRCxDQUFDLENBQ0gsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUU7d0JBQ1osS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ3ZDLENBQUMsQ0FBQyxDQUFDO2lCQUVMO3FCQUFNLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxhQUFhLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtvQkFFaEUsTUFBTSxHQUFHLEdBQUcsVUFBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ3hDLFdBQVcsQ0FBQyxlQUFlLENBQUM7d0JBQ3pCLEdBQUc7d0JBQ0gsTUFBTTtxQkFDUixFQUFFLENBQUMsTUFBYyxFQUFFLFFBQW1KLEVBQUUsRUFBRTt3QkFDeEssSUFBSSxNQUFNLElBQUksR0FBRyxJQUFJLE1BQU0sR0FBRyxHQUFHLEVBQUU7NEJBQ2hDLElBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0NBQ3BELE9BQU87b0NBQ0osUUFBUSxFQUFFLElBQUksQ0FBQyxZQUFZO29DQUMzQixpQkFBaUIsRUFBRSxJQUFJLENBQUMsa0JBQWtCO29DQUMxQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFlBQVk7b0NBQzlCLFlBQVksRUFBRSxJQUFJLENBQUMsZ0JBQWdCLElBQUksQ0FBQztvQ0FDeEMsWUFBWSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUI7aUNBQzdDLENBQUE7NEJBQ0osQ0FBQyxDQUFDLENBQUM7NEJBQ0gsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzt5QkFDN0I7NkJBQU07NEJBQ0osS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDOzRCQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLGdDQUFnQyxDQUFDLENBQUM7eUJBQ2hEO29CQUNKLENBQUMsQ0FBQyxDQUFDO2lCQUVMO3FCQUFNO29CQUNKLEtBQUssQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO29CQUMvQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7aUJBQ3RDO1lBQ0osQ0FBQyxDQUFDLENBQUM7U0FDTDthQUFNO1lBRUosb0RBQW9EO1lBQ3BELE9BQU8sSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxFQUFFLEVBQUU7Z0JBQ25DLElBQUksZUFBZSxHQUFRO29CQUN4QixNQUFNLEVBQUUsSUFBSSxDQUFDLEtBQUs7b0JBQ2xCLE9BQU8sRUFBRSxDQUFDLFFBQTRELEVBQUUsRUFBRTt3QkFFdkUsK0hBQStIO3dCQUMvSCx3RkFBd0Y7d0JBQ3hGLElBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQzt3QkFDbEQsT0FBTyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDMUMsQ0FBQztvQkFDRCxLQUFLLEVBQUUsQ0FBQyxRQUFhLEVBQUUsRUFBRTt3QkFDdEIsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUNoQixPQUFPLENBQUMsS0FBSyxDQUFDLHNDQUFzQyxFQUFFLFFBQVEsQ0FBQyxDQUFDO29CQUNuRSxDQUFDO2lCQUNILENBQUM7Z0JBRUYsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO29CQUNuQixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7d0JBQ2xCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQ0FBbUMsR0FBRyxDQUFDLEVBQUU7NEJBQzNELGVBQWUsQ0FBQywwQkFBMEIsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsbUNBQW1DLENBQUM7eUJBQ3JHO3FCQUNIO3lCQUFNO3dCQUNKLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQywyQkFBMkIsR0FBRyxDQUFDLEVBQUU7NEJBQ25ELGVBQWUsQ0FBQywwQkFBMEIsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsMkJBQTJCLENBQUM7eUJBQzdGO3FCQUNIO2lCQUNIO2dCQUVELHNCQUFzQixDQUFDLGVBQWUsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUMzRCxDQUFDLENBQUMsQ0FBQztTQUNMO0lBQ0osQ0FBQztJQUVELHdDQUF3QztJQUNqQyxRQUFRLENBQUMsVUFBa0I7UUFDL0IsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsRUFBRTtZQUNuQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUM7Z0JBQzdCLE9BQU8sRUFBRSxVQUFVO2dCQUNuQixPQUFPLEVBQUUsQ0FBQyxRQUF1QyxFQUFFLEVBQUU7b0JBQ2xELE9BQU8sQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNoQyxDQUFDO2dCQUNELEtBQUssRUFBRSxDQUFDLFFBQWEsRUFBRSxFQUFFO29CQUN0QixPQUFPLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN4QixLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ25CLENBQUM7YUFDSCxDQUFDLENBQUM7UUFDTixDQUFDLENBQUMsQ0FBQztJQUNOLENBQUM7SUFFRDs7T0FFRztJQUNLLG9CQUFvQjtRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUk7WUFBRSxPQUFPLEtBQUssQ0FBQztRQUU3QixJQUFJLHNCQUFzQixHQUFHLElBQUksQ0FBQztRQUVsQyxJQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFDO1lBQzlELHNCQUFzQixHQUFHLEtBQUssQ0FBQztZQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUMsQ0FBQyxnQkFBZ0I7U0FDeEM7UUFFRCxJQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUM7WUFDakIsc0JBQXNCLEdBQUcsS0FBSyxDQUFDO1NBQ2pDO1FBRUQsSUFBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFDO1lBQ2Ysc0JBQXNCLEdBQUcsS0FBSyxDQUFDO1NBQ2pDO1FBRUQsSUFBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ3RCLHNCQUFzQixHQUFHLEtBQUssQ0FBQztTQUNqQztRQUVELE9BQU8sc0JBQXNCLENBQUM7SUFDakMsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ1UsaUJBQWlCLENBQUMsT0FBZSxFQUFFLE1BQWU7O1lBQzVELElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLElBQUk7Z0JBQUUsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1lBRWhELE1BQU0sV0FBVyxHQUFHLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN2RCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFFaEIsSUFBSSxNQUFNLEVBQUU7Z0JBQ1QsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQy9EO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDZixDQUFDO0tBQUE7SUFFYSxjQUFjLENBQUMsT0FBZTs7WUFDekMsTUFBTSxXQUFXLEdBQUcsTUFBTSxJQUFJLENBQUMsTUFBTyxDQUFDLFlBQVksQ0FBYyxlQUFlLEdBQUcsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEVBQUUsRUFBQyxXQUFXLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztZQUNySSxJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztZQUUvQixPQUFPLFdBQVcsQ0FBQztRQUN0QixDQUFDO0tBQUE7SUFFWSxhQUFhLENBQUMsU0FBaUIsRUFBRSxNQUFjOztZQUN6RCxNQUFNLElBQUksR0FBRyxNQUFNLElBQUksQ0FBQyxNQUFPLENBQUMsWUFBWSxDQUFPLFdBQVcsRUFBRSxFQUFDLFVBQVUsRUFBRSxTQUFTLEVBQUMsQ0FBQyxDQUFDO1lBQ3pGLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBRWpCLE9BQU8sSUFBSSxDQUFDO1FBQ2YsQ0FBQztLQUFBO0lBRUQ7OztPQUdHO0lBQ1csZ0JBQWdCOzs7WUFDM0IsSUFBRyxDQUFDLElBQUksQ0FBQyxJQUFJO2dCQUFFLE9BQU8sSUFBSSxDQUFDO1lBRTNCLElBQUksVUFBVSxHQUFRLENBQUMsQ0FBQztZQUN4QixJQUFJLGVBQWUsR0FBRyxDQUFDLENBQUM7WUFDeEIsSUFBSSxhQUFhLEdBQUssQ0FBQyxDQUFDO1lBQ3hCLE1BQU0sSUFBSSxHQUFRLEVBQUUsQ0FBQztZQUVyQixJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDckIseUNBQXlDO1lBQ3pDLEtBQUssTUFBTSxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQ2pDLElBQUksSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLEVBQUU7b0JBQ2YsSUFBSSxDQUFDLFFBQVEsR0FBRyxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDM0QsSUFBSSxDQUFDLGlCQUFpQixHQUFHLGFBQWEsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO29CQUMzRCxJQUFJLENBQUMsWUFBWSxHQUFHLGFBQWEsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxHQUFHLENBQUMsQ0FBQztvQkFDOUUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLGFBQWEsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztvQkFDOUUsSUFBSSxDQUFDLGNBQWMsR0FBRyxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUMzRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsYUFBYSxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7b0JBRTdFLElBQUksSUFBSSxDQUFDLDBCQUEwQixFQUFFO3dCQUNsQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsYUFBYSxDQUFDLEdBQUcsRUFBRSxDQUFDO3dCQUU5QyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs0QkFDOUQsTUFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUUzRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsYUFBYSxDQUFDLElBQUksUUFBUSxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7NEJBQzNFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxhQUFhLENBQUMsSUFBSSxVQUFVLGVBQWUsQ0FBQyxLQUFLLEVBQUUsQ0FBQzs0QkFDOUUsSUFBSSxlQUFlLENBQUMsR0FBRztnQ0FBRSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsYUFBYSxDQUFDLElBQUksUUFBUSxlQUFlLENBQUMsR0FBRyxFQUFFLENBQUM7NEJBQ25HLElBQUksZUFBZSxDQUFDLEtBQUs7Z0NBQUUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLGFBQWEsQ0FBQyxJQUFJLFVBQVUsZUFBZSxDQUFDLEtBQUssRUFBRSxDQUFDOzRCQUN6RyxJQUFJLGVBQWUsQ0FBQyxTQUFTO2dDQUFFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxhQUFhLENBQUMsSUFBSSxjQUFjLGVBQWUsQ0FBQyxTQUFTLEVBQUUsQ0FBQzs0QkFFckgsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtnQ0FDN0csTUFBTSxXQUFXLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQzVFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxhQUFhLENBQUMsSUFBSSxpQkFBaUIsV0FBVyxJQUFJLGVBQWUsQ0FBQyxZQUFhLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQzs2QkFDM0g7NEJBRUQsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDLE1BQU0sR0FBRyxDQUFDO2dDQUFFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxhQUFhLENBQUMsSUFBSSxHQUFHLENBQUM7eUJBQ3RHO3FCQUVIO29CQUdELGFBQWEsRUFBRSxDQUFDO29CQUVoQixVQUFVLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztvQkFDM0MsZUFBZSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7b0JBRXRELElBQUcsV0FBVyxJQUFJLEVBQUU7d0JBQUUsV0FBVyxJQUFJLEtBQUssQ0FBQztvQkFDM0MsV0FBVyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO2lCQUMvRDthQUNIO1lBRUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFZLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUMvRCxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQzNDLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSyxDQUFDLElBQUksQ0FBQztZQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUssQ0FBQyxJQUFJLENBQUM7WUFDL0IsSUFBRyxJQUFJLENBQUMsSUFBSyxDQUFDLEdBQUcsRUFBRTtnQkFDaEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFLLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3RELElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQ3BEO2lCQUFNO2dCQUNKLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3JCLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUM7YUFDbkI7WUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUssQ0FBQyxLQUFLLENBQUM7WUFDakMsSUFBRyxJQUFJLENBQUMsSUFBSyxDQUFDLEtBQUssRUFBRTtnQkFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFLLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ2xFLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDdEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDckQ7aUJBQU07Z0JBQ0osSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDdEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQzthQUNyQjtZQUNELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFLLENBQUMsU0FBUyxDQUFDO1lBQy9DLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUssQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDMUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO1lBQzdDLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBRyxJQUFJLENBQUMsUUFBUSwwQ0FBRSxFQUFFLENBQUM7WUFDdkMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFLLENBQUMsYUFBYSxDQUFDO1lBQ2pELElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxVQUFVLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxlQUFlLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDM0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFZLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztZQUNqRCxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUU3QixJQUFJLElBQUksQ0FBQyxRQUFRO2dCQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQztZQUV6RCxJQUFJLElBQUksQ0FBQyxXQUFZLENBQUMsYUFBYSxJQUFJLGFBQWEsRUFBRTtnQkFDbkQsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsZUFBZSxDQUFDO2dCQUM3QyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsV0FBVyxDQUFDO2FBQ3BDO1lBRUQsT0FBTyxJQUFJLENBQUM7O0tBQ2Q7SUFFRDs7Ozs7Ozs7Ozt1REFVbUQ7SUFDdEMsSUFBSSxDQUFDLElBQVUsRUFBRSxPQUFlLEVBQUUsTUFBZSxFQUFFLGtCQUFrQixHQUFHLElBQUk7OztZQUN0RixJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFFcEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFFdkIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFFakIsTUFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBRTlDLElBQUksSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7Z0JBQ2pCLElBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxXQUFXLEVBQUM7b0JBQzdCLElBQUksQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUMsRUFBRTt3QkFDbkMsTUFBTSxJQUFJLFdBQVcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO3FCQUNsRDtpQkFDSDtxQkFBTTtvQkFDSixJQUFJLENBQUUsQ0FBQyxNQUFNLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLEVBQUU7d0JBQ3RDLE1BQU0sSUFBSSxXQUFXLENBQUMsMEJBQTBCLENBQUMsQ0FBQztxQkFDcEQ7aUJBQ0g7Z0JBRUQsS0FBSyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtvQkFDL0IsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxDQUFDO3dCQUFFLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO2lCQUMzQzthQUNIO2lCQUFNO2dCQUNKLHNGQUFzRjtnQkFDdEYsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksR0FBRyxFQUFFO29CQUN6RCxLQUFLLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO3dCQUMvQixJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLENBQUM7NEJBQUUsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7cUJBQzFDO2lCQUNIO2FBQ0g7WUFFRCxJQUFJLENBQUMsTUFBTTtnQkFBRSxNQUFNLElBQUksV0FBVyxDQUFDLHVCQUF1QixFQUFFLGVBQWUsQ0FBQyxlQUFlLENBQUMsQ0FBQTtZQUM1RixJQUFJO2dCQUNELElBQUksUUFBUSxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU8sQ0FBQyxZQUFZLENBQWtKLHFCQUFxQixFQUFFLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUV2VCxJQUFJLFFBQVEsQ0FBQyxLQUFLLEVBQUU7b0JBQ2pCLElBQUksYUFBYSxJQUFJLFFBQVEsRUFBRTt3QkFDNUIsTUFBTSxJQUFJLFdBQVcsQ0FBQyxzQkFBc0IsUUFBUSxDQUFDLFdBQVcsMkNBQTJDLEVBQUUsZUFBZSxDQUFDLGtCQUFrQixFQUFFLEVBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsRUFBQyxLQUFLLEVBQUUsUUFBUSxFQUFDLEVBQUMsQ0FBQyxDQUFDO3FCQUM5TDtvQkFDRCxJQUFJLFVBQVUsSUFBSSxRQUFRLEVBQUU7d0JBQ3pCLE1BQU0sSUFBSSxXQUFXLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxlQUFlLENBQUMsYUFBYSxFQUFFLEVBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsRUFBQyxLQUFLLEVBQUUsUUFBUSxFQUFDLEVBQUMsQ0FBQyxDQUFDO3FCQUNySDtvQkFFRCxzRUFBc0U7b0JBQ3RFLE1BQU0sUUFBUSxDQUFDO2lCQUNqQjtxQkFBTTtvQkFDSixJQUFJLFFBQVEsQ0FBQyxRQUFRO3dCQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQztvQkFDekQsSUFBSSxPQUFBLElBQUksQ0FBQyxXQUFXLDBDQUFFLGFBQWEsS0FBSSxhQUFhLEVBQUU7d0JBQ25ELElBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQztxQkFDekM7eUJBQU07d0JBQ0osSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsU0FBUyxDQUFDO3FCQUN2QztvQkFFRCxJQUFJLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVksQ0FBQyxxQkFBcUIsRUFBRTt3QkFDNUYsT0FBTyxFQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQyxjQUFjLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUMsQ0FBQztxQkFDekY7eUJBQU07d0JBQ0osT0FBTyxFQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQyxhQUFhLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUMsQ0FBQztxQkFDeEY7aUJBQ0g7YUFDSDtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNiLElBQUksS0FBSyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7b0JBQ3RCLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxjQUFjLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUU7d0JBRTVELGlGQUFpRjt3QkFDakYsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVE7NEJBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQzt3QkFDL0QsSUFBSSxPQUFBLElBQUksQ0FBQyxXQUFXLDBDQUFFLGFBQWEsS0FBSSxhQUFhLEVBQUU7NEJBQ25ELElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7eUJBQzVDOzZCQUFNOzRCQUNKLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7eUJBQzFDO3dCQUVELE1BQU0sSUFBSSxXQUFXLENBQUMsMkJBQTJCLElBQUksQ0FBQyxJQUFLLENBQUMsS0FBSyw0QkFBNEIsRUFBRSxlQUFlLENBQUMsY0FBYyxDQUFDLENBQUM7cUJBQ2pJO29CQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUU7d0JBQ3RCLE1BQU0sSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsZUFBZSxDQUFDLGFBQWEsQ0FBQyxDQUFDO3FCQUM3RTtpQkFDSDtxQkFBTSxJQUFJLEtBQUssQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFO29CQUM3QixNQUFNLElBQUksV0FBVyxDQUFDLHVCQUF1QixFQUFFLGVBQWUsQ0FBQyxlQUFlLENBQUMsQ0FBQTtpQkFDakY7Z0JBRUQsMkJBQTJCO2dCQUMzQixNQUFNLEtBQUssQ0FBQzthQUNkOztLQUNIO0lBRUQ7Ozt1REFHbUQ7SUFDdEMsVUFBVSxDQUFDLElBQVUsRUFBRSxPQUFlLEVBQUUsTUFBYzs7WUFDaEUsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBRWxCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBRWpCLE1BQU0sSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztZQUU5QyxPQUFPLE1BQU0sSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BDLENBQUM7S0FBQTtJQUVEOzs7dURBR21EO0lBQzVDLGlCQUFpQixDQUFDLFdBQXdCLEVBQUUsV0FBd0IsRUFBRSxJQUFVO1FBQ3BGLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUVsQixJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUMvQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztRQUUvQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFaEIsT0FBTyxFQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQyxjQUFjLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUMsQ0FBQztJQUMxRixDQUFDO0lBRUQ7OztPQUdHO0lBQ1UsWUFBWSxDQUFDLGVBQXVLOztZQUM5TCxJQUFJLENBQUMsMEJBQTBCLEdBQUcsZUFBZSxDQUFDO1lBRWxELElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFDakIsSUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLFNBQVMsSUFBSSxJQUFJLENBQUMsMEJBQTBCLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDakYsSUFBSTt3QkFDRCxJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU8sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLEVBQUMsS0FBSyxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEVBQUMsRUFBRSxFQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDO3FCQUNqSTtvQkFBQyxPQUFPLEtBQUssRUFBRTt3QkFDYixJQUFJLEtBQUssQ0FBQyxLQUFLLElBQUksTUFBTSxFQUFFOzRCQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU8sQ0FBQyxZQUFZLENBQUMsa0JBQWtCLEVBQUU7Z0NBQ2xFLElBQUksRUFBRSxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtnQ0FDN0MsS0FBSyxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLO2dDQUMvQyxLQUFLLEVBQUUsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUs7Z0NBQy9DLEdBQUcsRUFBRSxJQUFJLENBQUMsMEJBQTBCLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRztnQ0FDM0MsUUFBUSxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTOzZCQUN4RCxFQUFFO2dDQUNBLElBQUksRUFBRSxNQUFNO2dDQUNaLE9BQU8sRUFBRSxDQUFDOzZCQUNaLENBQUMsQ0FBQzt5QkFDTDs2QkFBTTs0QkFDSixNQUFNLEtBQUssQ0FBQzt5QkFDZDtxQkFDSDtpQkFDSDtnQkFFRCxPQUFPLE1BQU0sSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2FBQ25DO2lCQUFNO2dCQUNKLElBQUksSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7b0JBQ2pCLE9BQU8sRUFBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUMsY0FBYyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFDLENBQUM7aUJBQ3pGO3FCQUFNO29CQUNKLE9BQU8sRUFBQyxRQUFRLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFDLENBQUM7aUJBQ2pGO2FBQ0g7UUFDSixDQUFDO0tBQUE7SUFFRDs7O09BR0c7SUFDSSxtQkFBbUIsQ0FBQyxhQUE0QjtRQUNwRCxJQUFJLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQztRQUVwQyxJQUFJLGFBQWEsSUFBSSxRQUFRLEVBQUU7WUFDNUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLFVBQVUsQ0FBQztTQUM3QztRQUVELElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxRQUFRLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUN4RCxPQUFPLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsVUFBVSxDQUFDLFlBQVksRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBQyxDQUFDO1NBQ3ZGO2FBQU07WUFDSixPQUFPLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsVUFBVSxDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBQyxDQUFDO1NBQ2pGO0lBQ0osQ0FBQztJQUVEOzs7O09BSUc7SUFDVSx1QkFBdUIsQ0FBQyxpQkFBb0MsRUFBRSwwQkFBMEIsR0FBRyxLQUFLOztZQUMxRyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsaUJBQWlCLENBQUM7WUFFNUMsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxFQUFFLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO2dCQUNyRywwREFBMEQ7Z0JBQzFELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFFaEIsSUFBSSxJQUFJLENBQUMsV0FBWSxDQUFDLGNBQWMsRUFBRTtvQkFDbkMsSUFBSTt3QkFDRCxNQUFNLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO3FCQUNoQztvQkFBQyxXQUFNO3dCQUNMLE1BQU0sSUFBSSxXQUFXLENBQUMsMkJBQTJCLEVBQUUsZUFBZSxDQUFDLGFBQWEsQ0FBQyxDQUFDO3FCQUNwRjtpQkFDSDtxQkFBTTtvQkFDSixJQUFJLDBCQUEwQixFQUFFO3dCQUM3QixJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDbEQ7eUJBQU07d0JBQ0osTUFBTSxJQUFJLFdBQVcsQ0FBQyxxRkFBcUYsRUFBRSxlQUFlLENBQUMsaUJBQWlCLENBQUMsQ0FBQztxQkFDbEo7aUJBQ0g7YUFDSDtZQUVELE9BQU8sTUFBTSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEMsQ0FBQztLQUFBO0lBRUQ7OztPQUdHO0lBQ1Usc0JBQXNCLENBQUMsbUJBQTJCOztZQUM1RCxJQUFJLENBQUMsdUJBQXVCLEdBQUcsbUJBQW1CLENBQUM7WUFDbkQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLG1CQUFtQixDQUFDLENBQUMsV0FBVyxDQUFDO1lBRXRFLE9BQU8sTUFBTSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEMsQ0FBQztLQUFBO0lBRUQ7OztPQUdHO0lBQ1Usb0JBQW9CLENBQUMsY0FBc0M7O1lBQ3JFLElBQUksQ0FBQyxlQUFlLEdBQUcsY0FBYyxDQUFDO1lBRXRDLE9BQU8sTUFBTSxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEMsQ0FBQztLQUFBO0lBRUQ7Ozs7O09BS0c7SUFDVSxlQUFlLENBQUMsS0FBYSxFQUFFLElBQWE7O1lBQ3RELElBQUk7Z0JBQ0QsSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDUixJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sSUFBSSxDQUFDLE1BQU8sQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDMUY7cUJBQU07b0JBQ0osSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLElBQUksQ0FBQyxNQUFPLENBQUMsWUFBWSxDQUFDLGtCQUFrQixFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxFQUFFO3dCQUNuRixJQUFJLEVBQUUsTUFBTTt3QkFDWixPQUFPLEVBQUUsQ0FBQztxQkFDWixDQUFDLENBQUM7aUJBQ0w7YUFDSDtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNiLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxNQUFNO29CQUFFLE1BQU0sSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUM7O29CQUNwRixNQUFNLElBQUksV0FBVyxDQUFDLEtBQUssRUFBRSxlQUFlLENBQUMsYUFBYSxDQUFDLENBQUM7YUFDbkU7WUFFRCxPQUFPLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUM5QixDQUFDO0tBQUE7SUFFRDs7OztPQUlHO0lBQ1UsY0FBYyxDQUFDLFNBQWtCOztZQUMzQyxJQUFJLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFZLENBQUMsYUFBYSxJQUFJLFdBQVcsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLFFBQVEsRUFBQztnQkFDcEcsT0FBTyxNQUFNLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2FBQzFDO2lCQUFNO2dCQUNKLE9BQU8sTUFBTSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDaEQ7UUFDSixDQUFDO0tBQUE7SUFHWSxVQUFVOztZQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUM5QztpQkFBTSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2dCQUN6QixNQUFNLElBQUksQ0FBQyxNQUFPLENBQUMsWUFBWSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFO29CQUM1RSxJQUFJLEVBQUUsS0FBSztvQkFDWCxPQUFPLEVBQUUsQ0FBQztpQkFDWixDQUFDLENBQUM7YUFDTDtZQUVELE9BQU8sSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQzlCLENBQUM7S0FBQTtJQUVELG1FQUFtRTtJQUMzRCxtQkFBbUI7UUFDeEIsT0FBTyxJQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUNwQyxNQUFNLEtBQUssR0FBRztnQkFDWCxVQUFVLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUMsRUFBRSxDQUFDO2dCQUN2RCxLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSztnQkFDaEMsR0FBRyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUc7Z0JBQzVCLGVBQWUsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQzFELGNBQWMsRUFBRSxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNoRSxLQUFLLEVBQUUsQ0FBQyxLQUFVLEVBQUUsRUFBRTtvQkFDbkIsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNqQixDQUFDO2dCQUNELE9BQU8sRUFBRSxDQUFDLFFBQXVDLEVBQUUsRUFBRTtvQkFDbEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUN0RCxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ25CLENBQUMsRUFBRSxDQUFDLEtBQVUsRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLENBQUM7YUFDSCxDQUFDO1lBRUYsc0JBQXNCLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2pELENBQUMsQ0FBQyxDQUFDO0lBQ04sQ0FBQztJQUVhLGdCQUFnQixDQUFDLFNBQWtCOzs7WUFDOUMsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNaLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUc7b0JBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUM1RCxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTO29CQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztnQkFDOUUsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSztvQkFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7YUFDcEU7WUFFRCxNQUFNLElBQUksR0FBRyxNQUFNLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBRTNDLElBQUksU0FBUyxFQUFFO2dCQUNaLE1BQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3pFLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxZQUFZLENBQUM7Z0JBRXJDLElBQUksSUFBSSxDQUFDLFdBQVksQ0FBQyxhQUFhLElBQUksYUFBYSxFQUFFO29CQUNuRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsR0FBRyxZQUFZLENBQUMsWUFBWSxDQUFDO2lCQUN4RDtnQkFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQztnQkFDcEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7Z0JBQ3BELElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO2dCQUM5QyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztnQkFDeEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7Z0JBQ2hELElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDO2dCQUMxRCxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUN4QixJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxZQUFZLENBQUMsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUMzRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsR0FBRyxVQUFVLENBQUMsWUFBWSxDQUFDLGlCQUFpQixFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUM1RSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsU0FBUyxDQUFDO2dCQUN6QixJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO2dCQUMxQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDO2dCQUU5QyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLFlBQVksQ0FBQyxRQUFRLEdBQUcsR0FBRyxHQUFHLFlBQVksQ0FBQyxpQkFBaUIsR0FBRyxHQUFHLEdBQUcsWUFBWSxDQUFDLFdBQVcsQ0FBQzthQUN6SDtpQkFBTSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksUUFBUSxFQUFFO2dCQUN4QyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsUUFBUSxDQUFDO2FBQ25DO2lCQUFNO2dCQUNKLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxNQUFNLENBQUM7YUFDakM7WUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUVyQyxJQUFJLENBQUMsMEJBQTBCLEdBQUcsRUFBRSxDQUFDO1lBQ3JDLElBQUk7Z0JBQ0QsTUFBTSxRQUFRLEdBQUcsTUFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM1QyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztnQkFDekIsT0FBTyxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQyxNQUFNLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUMsQ0FBQzthQUNoRjtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNiLElBQUksS0FBSyxDQUFDLFNBQVM7b0JBQUUsTUFBTSxLQUFLLENBQUM7Z0JBRWpDLE1BQU0sSUFBSSxXQUFXLENBQUMsT0FBQSxLQUFLLENBQUMsS0FBSywwQ0FBRSxJQUFJLEtBQUkseUJBQXlCLEVBQUUsZUFBZSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2FBQ3hHOztLQUNIO0lBRWEsU0FBUyxDQUFDLElBQVM7OztZQUM5QixJQUFJLFFBQThFLENBQUM7WUFFbkYsSUFBSTtnQkFDRCxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxFQUFFO29CQUNsQixRQUFRLEdBQUcsTUFBTSxJQUFJLENBQUMsTUFBTyxDQUFDLFlBQVksQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztpQkFDM0Y7cUJBQU0sSUFBSSxJQUFJLENBQUMsV0FBWSxDQUFDLGFBQWEsSUFBSSxhQUFhLEVBQUU7b0JBQzFELFFBQVEsR0FBRyxNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUF1RSxJQUFJLENBQUMsU0FBUyxHQUFHLHdCQUF3QixFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO2lCQUNySztxQkFBTTtvQkFDSixRQUFRLEdBQUcsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBdUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7aUJBQzFKO2FBQ0g7WUFBQyxPQUFPLEtBQUssRUFBRTtnQkFDYixNQUFNLElBQUksV0FBVyxDQUFDLE9BQUEsS0FBSyxDQUFDLEtBQUssMENBQUUsSUFBSSxLQUFJLHlCQUF5QixFQUFFLGVBQWUsQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUN4RztZQUVELElBQUksUUFBUSxDQUFDLEtBQUssRUFBRTtnQkFDakIsTUFBTSxJQUFJLFdBQVcsQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFLGVBQWUsQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUMzRTtZQUVELE9BQU8sUUFBUSxDQUFDLFFBQVEsQ0FBQzs7S0FDM0I7SUFFYSxjQUFjLENBQUMsaUJBQTBCLEtBQUs7O1lBRXpELE1BQU0sSUFBSSxHQUFRLEVBQUUsWUFBWSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsY0FBYyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBQ3RILElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRO2dCQUFFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDO1lBRWpFLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtnQkFDWixNQUFNLFVBQVUsR0FBbUMsRUFBRSxDQUFDO2dCQUN0RCxNQUFNLGVBQWUsR0FBcUMsRUFBRSxDQUFDO2dCQUM3RCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQzVCLFVBQVUsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7b0JBQ3RDLElBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQzt3QkFBRSxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBQyxFQUFFLENBQUEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUN2RyxDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQTtnQkFDdEIsSUFBSSxNQUFNLENBQUMsbUJBQW1CLENBQUMsZUFBZSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUM7b0JBQUUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGVBQWUsQ0FBQzthQUN0RztZQUVELElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtnQkFDbkIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQzthQUM3QztZQUVELElBQUksSUFBSSxDQUFDLDBCQUEwQjtnQkFBRSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDO1lBQzdGLElBQUksSUFBSSxDQUFDLGNBQWM7Z0JBQUUsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO1lBQ2xFLElBQUksY0FBYztnQkFBRSxJQUFJLENBQUMsZUFBZSxHQUFHLGNBQWMsQ0FBQztZQUMxRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7WUFFakMsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNsQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQTthQUN4QjtZQUVELE9BQU8sSUFBSSxDQUFDLE1BQU8sQ0FBQyxZQUFZLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxFQUFFO2dCQUN2RCxJQUFJLEVBQUUsTUFBTTtnQkFDWixPQUFPLEVBQUUsQ0FBQzthQUNaLENBQUMsQ0FBQztRQUNOLENBQUM7S0FBQTtJQUVEOzs7T0FHRztJQUNXLFlBQVk7O1lBQ3ZCLE1BQU0sTUFBTSxHQUFpQixFQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFDLENBQUM7WUFFdEYscUVBQXFFO1lBQ3JFLElBQUksSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7Z0JBQ2pCLElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLFNBQVMsRUFBRTtvQkFDdEMsTUFBTSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUMsY0FBYyxDQUFDO29CQUM1QyxPQUFPLE1BQU0sQ0FBQztpQkFDaEI7cUJBQU07b0JBQ0osSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxpQkFBaUIsQ0FBQyxNQUFNLEVBQUU7d0JBQ3hFLElBQUksSUFBSSxDQUFDLHVCQUF1QixHQUFHLENBQUMsRUFBRTs0QkFDbkMsTUFBTSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUMsWUFBWSxDQUFDO3lCQUM1QztxQkFDSDtpQkFDSDthQUNIO2lCQUFNO2dCQUNKLElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLFNBQVMsRUFBRTtvQkFDdEMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLGlCQUFpQixDQUFDLElBQUksQ0FBQztpQkFDbkQ7YUFDSDtZQUVELHNGQUFzRjtZQUN0RixJQUFJLE1BQU0sQ0FBQyxRQUFRLEdBQUcsQ0FBQyxFQUFFO2dCQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtvQkFFdkIsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxFQUFFO3dCQUM1RCxJQUFJLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQztxQkFDcEM7eUJBQU07d0JBRUosSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxXQUFZLENBQUMsb0JBQW9CLElBQUksSUFBSSxDQUFDLFdBQVksQ0FBQyxtQkFBbUIsRUFBRTs0QkFDckcsTUFBTSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUMsZUFBZSxDQUFDO3lCQUMvQzs2QkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFdBQVksQ0FBQyxvQkFBb0IsRUFBRTs0QkFDbkUsSUFBSSxDQUFDLGVBQWUsR0FBRyxVQUFVLENBQUM7eUJBQ3BDOzZCQUFNLElBQUksSUFBSSxDQUFDLFdBQVksQ0FBQyxtQkFBbUIsRUFBRTs0QkFDL0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxTQUFTLENBQUM7eUJBQ25DOzZCQUFNOzRCQUNKLE1BQU0sSUFBSSxXQUFXLENBQUMsK0VBQStFLEVBQUUsZUFBZSxDQUFDLGNBQWMsRUFBRSxFQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLEVBQUMsQ0FBQyxDQUFDO3lCQUN0TTtxQkFFSDtpQkFFSDthQUNIO1lBRUQseUZBQXlGO1lBQ3pGLGdFQUFnRTtZQUNoRSxJQUFJLE1BQU0sQ0FBQyxRQUFRLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDNUMsSUFBSSxJQUFJLENBQUMsV0FBWSxDQUFDLHFCQUFxQixJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxFQUFFO29CQUM3RCxJQUFJLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLE1BQU0sRUFBRTt3QkFDMUMsTUFBTSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDO3FCQUM3QztpQkFDSDtxQkFBTSxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksU0FBUyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVksQ0FBQyxxQkFBcUIsRUFBRTtvQkFDdEYsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7d0JBQ2xCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLFNBQVMsQ0FBQztxQkFDekM7aUJBQ0g7YUFDSDtZQUVELCtFQUErRTtZQUMvRSxJQUFJLE1BQU0sQ0FBQyxRQUFRLEdBQUcsQ0FBQyxFQUFFO2dCQUN0QixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxFQUFFLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsRUFBRTtvQkFDcEksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7d0JBQ2pCLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNoRCxNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7d0JBQ2hDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQztxQkFDdkM7aUJBQ0g7YUFDSDtZQUVELHFGQUFxRjtZQUNyRixJQUFJLE1BQU0sQ0FBQyxRQUFRLEdBQUcsQ0FBQyxFQUFFO2dCQUN0QixJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksVUFBVSxFQUFFO29CQUNwQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTt3QkFDakIsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7d0JBQ2hELE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztxQkFDbEM7b0JBRUQsSUFBSSxJQUFJLENBQUMsUUFBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7d0JBQzVCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQztxQkFDckM7eUJBQU07d0JBQ0osTUFBTSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDO3FCQUN0QztpQkFDSDthQUNIO1lBRUQsK0JBQStCO1lBQy9CLElBQUksTUFBTSxDQUFDLFFBQVEsR0FBRyxDQUFDLEVBQUU7Z0JBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUM7b0JBQUUsTUFBTSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQ3hFLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDaEMsTUFBTSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7Z0JBQ3ZCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQzthQUN0QztZQUVELE9BQU8sTUFBTSxDQUFDO1FBQ2pCLENBQUM7S0FBQTs7MEVBcmhDUyxhQUFhO3FEQUFiLGFBQWEsV0FBYixhQUFhLG1CQURELE1BQU07dUZBQ2xCLGFBQWE7Y0FEekIsVUFBVTtlQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQzs7QUFpaUNoQyxNQUFNLENBQU4sSUFBWSxVQW1CWDtBQW5CRCxXQUFZLFVBQVU7SUFDbkIsb0NBQW9DO0lBQ3BDLCtEQUFjLENBQUE7SUFDZCwyREFBMkQ7SUFDM0QsNkRBQWEsQ0FBQTtJQUNiLDhGQUE4RjtJQUM5Riw2Q0FBSyxDQUFBO0lBQ0wsbUNBQW1DO0lBQ25DLCtDQUFNLENBQUE7SUFDTixnREFBZ0Q7SUFDaEQsMkRBQVksQ0FBQTtJQUNaLHNDQUFzQztJQUN0QyxpRUFBZSxDQUFBO0lBQ2YsMkNBQTJDO0lBQzNDLDJEQUFZLENBQUE7SUFDWiw0QkFBNEI7SUFDNUIsaURBQU8sQ0FBQTtJQUNQLHFFQUFxRTtJQUNyRSxxREFBUyxDQUFBO0FBQ1osQ0FBQyxFQW5CVyxVQUFVLEtBQVYsVUFBVSxRQW1CckI7QUFFRCxNQUFNLE9BQU8sV0FBVztJQUVyQixZQUNVLEdBQVcsRUFDWCxZQUE2QixlQUFlLENBQUMsYUFBYSxFQUMxRCxPQUdILEVBQUU7UUFMQyxRQUFHLEdBQUgsR0FBRyxDQUFRO1FBQ1gsY0FBUyxHQUFULFNBQVMsQ0FBaUQ7UUFDMUQsU0FBSSxHQUFKLElBQUksQ0FHTDtJQUNMLENBQUM7SUFBQSxDQUFDO0lBRU4sSUFBVyxTQUFTO1FBQ2pCLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDakIsT0FBTztnQkFDSixJQUFJLEVBQUUsSUFBSSxDQUFDLEdBQUc7Z0JBQ2QsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUyxDQUFDLEtBQUs7Z0JBQ2hDLFFBQVEsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVMsQ0FBQyxRQUFRO2FBQ3hDLENBQUE7U0FDSDs7WUFBTSxNQUFNLElBQUksS0FBSyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7SUFDaEUsQ0FBQztDQUNIO0FBR0QsTUFBTSxDQUFOLElBQVksZUFlWDtBQWZELFdBQVksZUFBZTtJQUN4QixvQkFBb0I7SUFDcEIsdUVBQWlCLENBQUE7SUFDakIsd0RBQXdEO0lBQ3hELHlFQUFjLENBQUE7SUFDZCw2Q0FBNkM7SUFDN0MsaUZBQWtCLENBQUE7SUFDbEIsMENBQTBDO0lBQzFDLHlFQUFjLENBQUE7SUFDZCwyQ0FBMkM7SUFDM0MsMkVBQWUsQ0FBQTtJQUNmLDhFQUE4RTtJQUM5RSwrRUFBaUIsQ0FBQTtJQUNqQiwrREFBK0Q7SUFDL0QsK0RBQVMsQ0FBQTtBQUNaLENBQUMsRUFmVyxlQUFlLEtBQWYsZUFBZSxRQWUxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENhcnQsIERpc2NvdW50LCBFdmVudERldGFpbCwgSW5zdGFsbG1lbnRzLCBQYWdzZWd1cm9TZXNzaW9uLCBQYXltZW50LCBTZXJ2ZXIsIFVzZXIsIE90dG9BbGVydE1lc3NhZ2UsIE90dG9JbWcsIFB1cmNoYXNlLCBQcm9kdWN0Q2FydCwgUGF5bWVudE1ldGhvZCwgUGF5bWVudFR5cGUsIE91dFBheUNvbmZpZ3VyYXRpb24sIE91dGdvT3JpZ2luLCBQYXltZW50U2FsZU1ldGhvZCB9IGZyb20gJy4vbW9kZWwnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHZhbHVlUm91bmQgfSBmcm9tICcuL3V0aWwnO1xyXG5cclxuZGVjbGFyZSB2YXIgUGFnU2VndXJvRGlyZWN0UGF5bWVudDogYW55O1xyXG5kZWNsYXJlIHZhciBNZXJjYWRvcGFnbzogYW55O1xyXG5cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBPdXRQYXlTZXJ2aWNlIHtcclxuICAgY29uc3RydWN0b3IoXHJcbiAgICAgIHB1YmxpYyBodHRwOiBIdHRwQ2xpZW50LFxyXG4gICApXHJcbiAgIHsgfVxyXG5cclxuICAgLy8gRU5WSVJPTk1FTlRcclxuICAgcHVibGljIHNlcnZlclVybCA9IFwiXCI7XHJcbiAgIHB1YmxpYyBwYWdzZWd1cm9VcmwgPSBcIlwiO1xyXG4gICBwdWJsaWMgbWVyY2Fkb3BhZ29LZXkgPSBcIlwiO1xyXG4gICBwdWJsaWMgbW9kZXJuaW5oYSA9IGZhbHNlO1xyXG4gICBwdWJsaWMgc2VydmVyPzogU2VydmVyO1xyXG4gICBwdWJsaWMgb3JpZ2luOiBPdXRnb09yaWdpbiA9IFwiQVBQX09VVEdPXCI7XHJcblxyXG4gICAvLyBQVVJDSEFTRSBEQVRBXHJcbiAgIHB1YmxpYyBwdXJjaGFzZT86IFB1cmNoYXNlO1xyXG4gICBwdWJsaWMgY2FydDogQ2FydCA9IHtpdGVtczogW119O1xyXG4gICBwdWJsaWMgcGF5bWVudDogUGF5bWVudCA9IG5ldyBQYXltZW50KCk7XHJcbiAgIHB1YmxpYyB0aWNrZXREYXRhPzogYW55O1xyXG4gICBwdWJsaWMgaW5zdGFsbG1lbnRzOiBJbnN0YWxsbWVudHNbXSA9IFtdO1xyXG4gICBwdWJsaWMgaXNTZWxsaW5nID0gZmFsc2U7XHJcbiAgIHB1YmxpYyBwcm9kdWN0c1NhbGUgPSBmYWxzZTtcclxuICAgcHVibGljIHByb2R1Y3RDYXJ0OiBQcm9kdWN0Q2FydCA9IHtpdGVtczogW119O1xyXG4gICBwdWJsaWMgaW5zY3JpcHRpb25QYXJ0aWNpcGFudERhdGE6IHsgbmFtZTogc3RyaW5nOyBlbWFpbDogc3RyaW5nOyBwaG9uZT86IHN0cmluZzsgY3BmPzogc3RyaW5nOyBiaXJ0aGRhdGU/OiBzdHJpbmc7IHRpY2tldF9pZDogbnVtYmVyOyBleHRyYV9maWVsZHM/OiB7IFtmaWVsZElkOiBudW1iZXJdOiBzdHJpbmc7IH07IH1bXSA9IFtdO1xyXG4gICBwdWJsaWMgc2VsZWN0ZWRTYWxlSW5zdGFsbG1lbnQgPSAwO1xyXG4gICBwdWJsaWMgcHVyY2hhc2VyPzogVXNlcjtcclxuXHJcbiAgIHByaXZhdGUgX3BheW1lbnRNZXRob2Q/OiBQYXltZW50TWV0aG9kO1xyXG4gICBwdWJsaWMgZ2V0IHBheW1lbnRNZXRob2QoKSB7IHJldHVybiB0aGlzLl9wYXltZW50TWV0aG9kOyB9O1xyXG4gICBwcml2YXRlIF9wYXltZW50U2FsZU1ldGhvZD86IFBheW1lbnRTYWxlTWV0aG9kO1xyXG4gICBwdWJsaWMgZ2V0IHBheW1lbnRTYWxlTWV0aG9kKCkgeyByZXR1cm4gdGhpcy5fcGF5bWVudFNhbGVNZXRob2Q7IH07XHJcbiAgIHByaXZhdGUgX2RlbGl2ZXJ5TWV0aG9kPzogXCJkaWdpdGFsXCIgfCBcInBoeXNpY2FsXCI7XHJcbiAgIHB1YmxpYyBnZXQgZGVsaXZlcnlNZXRob2QoKSB7IHJldHVybiB0aGlzLl9kZWxpdmVyeU1ldGhvZDsgfTtcclxuICAgcHJpdmF0ZSBfZ3Jvc3NWYWx1ZT86IG51bWJlcjtcclxuICAgcHVibGljIGdldCBncm9zc1ZhbHVlKCkgeyByZXR1cm4gdGhpcy5fZ3Jvc3NWYWx1ZSB8fCB0aGlzLnZhbHVlIH07XHJcbiAgIFxyXG4gICBwdWJsaWMgdXNlcj86IFVzZXI7XHJcbiAgIHB1YmxpYyBldmVudERldGFpbD86IEV2ZW50RGV0YWlsO1xyXG5cclxuICAgcHVibGljIGdldCBwbGF0Zm9ybSgpIHtcclxuICAgICAgcmV0dXJuIHRoaXMuZXZlbnREZXRhaWw/LnBheW1lbnRNZXRob2Q7XHJcbiAgIH1cclxuXHJcbiAgIC8qKlxyXG4gICAgKiBWZXJpZmljYSBzZSDDqSBuZWNlc3PDoXJpbyBwZWRpciBvcyBkYWRvcyBkbyBjb21wcmFkb3IuIFxyXG4gICAgKiBTZXJ2ZSB0YW50byBwYXJhIGV4aWJpciBhIHRlbGEgZGUgZm9ybSBxdWFudG8gcHJhIGV4aWJpciBvIGZvcm11bMOhcmlvIGRlIGRhZG9zIGRvIGNvbXByYWRvclxyXG4gICAgKi9cclxuICAgcHVibGljIGdldCBuZWVkc0J1eWVyRGF0YSgpOiBib29sZWFuIHtcclxuICAgICAgcmV0dXJuICF0aGlzLmlzU2VsbGluZyAmJiAhKHRoaXMudXNlciAmJiB0aGlzLnVzZXIuZW1haWwgJiYgdGhpcy51c2VyLnBob25lICYmIHRoaXMudXNlci5jcGYgJiYgdGhpcy51c2VyLmJpcnRoZGF0ZSk7XHJcbiAgIH1cclxuXHJcbiAgIC8vVmFsb3IgZGEgY29tcHJhL3ZlbmRhXHJcbiAgIHByaXZhdGUgX3ZhbHVlOiBudW1iZXIgPSAwO1xyXG4gICBwdWJsaWMgZ2V0IHZhbHVlKCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5fdmFsdWU7XHJcbiAgIH1cclxuXHJcbiAgIHByaXZhdGUgc2V0VmFsdWUoKSB7XHJcbiAgICAgIGlmICghdGhpcy5jYXJ0Lml0ZW1zLmxlbmd0aCAmJiAhdGhpcy5wcm9kdWN0Q2FydC5pdGVtcy5sZW5ndGgpIHRocm93IG5ldyBPdXRQYXlFcnJvcihcIk8gY2FycmluaG8gYWluZGEgbsOjbyBmb2kgY2FycmVnYWRvXCIsIE91dFBheUVycm9yQ29kZS5HRU5FUkFMX0VSUk9SKTtcclxuXHJcbiAgICAgIGxldCB2YWx1ZSA9IHRoaXMuY2FydC5pdGVtcy5tYXAoaXRlbSA9PiB7XHJcbiAgICAgICAgIGxldCBpdGVtVmFsdWU6IG51bWJlcjtcclxuXHJcbiAgICAgICAgIGlmICh0aGlzLmlzU2VsbGluZykge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5tb2Rlcm5pbmhhKSB7XHJcbiAgICAgICAgICAgICAgIGl0ZW1WYWx1ZSA9IChcclxuICAgICAgICAgICAgICAgICAgKGl0ZW0udGlja2V0LnRheF9wYXllZF9ieV9jb21wYW55ID8gaXRlbS50aWNrZXQuZmluYWxfcHJpY2UgOiBpdGVtLnRpY2tldC5wcmljZSkgKlxyXG4gICAgICAgICAgICAgICAgICAodGhpcy5wYXltZW50U2FsZU1ldGhvZCA9PSBQYXltZW50U2FsZU1ldGhvZC5DUkVESVQgPyAoMSArIHRoaXMuZXZlbnREZXRhaWwhLnRheF9jcmVkaXRfYnV5ZXIpIDogKHRoaXMucGF5bWVudFNhbGVNZXRob2QgPT0gUGF5bWVudFNhbGVNZXRob2QuREVCSVQgPyAoMSArIHRoaXMuZXZlbnREZXRhaWwhLnRheF9kZWJpdF9idXllcikgOiAxKSlcclxuICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICBpZiAoaXRlbS50aWNrZXQuaXNfc2VsbGVyICYmICFpdGVtLnRpY2tldC50YXhfcGF5ZWRfYnlfY29tcGFueSkge1xyXG4gICAgICAgICAgICAgICAgICBpdGVtVmFsdWUgPSBpdGVtLnRpY2tldC5wcmljZTtcclxuICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgaXRlbVZhbHVlID0gaXRlbS50aWNrZXQuZmluYWxfcHJpY2U7XHJcbiAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpdGVtVmFsdWUgPSBpdGVtLnRpY2tldC5maW5hbF9wcmljZTtcclxuICAgICAgICAgfVxyXG5cclxuICAgICAgICAgcmV0dXJuIGl0ZW1WYWx1ZSAqIGl0ZW0ucXRkO1xyXG4gICAgICB9KS5yZWR1Y2UoKHRvdGFsLCBjdXJyZW50KSA9PiB0b3RhbCArIGN1cnJlbnQsIDApO1xyXG5cclxuICAgICAgdGhpcy5wcm9kdWN0Q2FydC5pdGVtcy5mb3JFYWNoKHByb2R1Y3RJdGVtID0+IHtcclxuICAgICAgICAgY29uc3QgcHJvZHVjdFZhbHVlID0gKHByb2R1Y3RJdGVtLnByaWNlICogcHJvZHVjdEl0ZW0ucXRkKTtcclxuXHJcbiAgICAgICAgIGlmICh0aGlzLmV2ZW50RGV0YWlsICYmIHRoaXMudXNlcikge1xyXG4gICAgICAgICAgICBjb25zdCB0ZWFtTWVtYmVyID0gdGhpcy5ldmVudERldGFpbC50ZWFtLmZpbmQodG0gPT4gdG0udXNlcklkID09IHRoaXMudXNlcj8uaWQpO1xyXG4gICAgICAgICAgICB2YWx1ZSArPSBwcm9kdWN0VmFsdWUgKyAocHJvZHVjdFZhbHVlICogKHRlYW1NZW1iZXI/LmNvbW1pc3Npb24gPyB0ZWFtTWVtYmVyLmNvbW1pc3Npb24gOiAwKSk7XHJcbiAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHZhbHVlICs9IHByb2R1Y3RWYWx1ZTtcclxuICAgICAgICAgfVxyXG4gICAgICB9KVxyXG5cclxuICAgICAgaWYgKHRoaXMuY2FydC5kaXNjb3VudCkgdGhpcy5fdmFsdWUgPSB2YWx1ZSAtIHZhbHVlICogKHRoaXMuY2FydC5kaXNjb3VudC52YWx1ZSAvIDEwMClcclxuICAgICAgZWxzZSB0aGlzLl92YWx1ZSA9IHZhbHVlO1xyXG4gICB9XHJcblxyXG4gICBwdWJsaWMgZ2V0IGRpc2NvdW50KCk6IERpc2NvdW50IHwgdW5kZWZpbmVkIHtcclxuICAgICAgcmV0dXJuIHRoaXMuY2FydC5kaXNjb3VudDtcclxuICAgfVxyXG5cclxuICAgcHVibGljIGdldCBib2xldG9QZXJtaXQoKTogYm9vbGVhbiB7XHJcbiAgICAgIHJldHVybiB0aGlzLmV2ZW50RGV0YWlsID8gdGhpcy5ldmVudERldGFpbC5oYXNCb2xldG8gOiBmYWxzZTtcclxuICAgfVxyXG5cclxuICAgLy8gUEFHU0VHVVJPXHJcbiAgIHByaXZhdGUgcGFnc2VndXJvTG9hZCA9IGZhbHNlO1xyXG4gICBwcml2YXRlIHBhZ3NlZ3Vyb1Byb21pc2U/OiBQcm9taXNlPGJvb2xlYW4+O1xyXG4gICBwcml2YXRlIHNlbmRlckhhc2g/OiBQcm9taXNlPHN0cmluZz47XHJcbiAgIHByaXZhdGUgbWVyY2Fkb3BhZ29Mb2FkID0gZmFsc2U7XHJcbiAgIHByaXZhdGUgbWVyY2Fkb3BhZ29Qcm9taXNlPzogUHJvbWlzZTxib29sZWFuPjtcclxuICAgc2Vzc2lvbklkID0gXCJcIjtcclxuXHJcbiAgIHB1YmxpYyBzZXR1cChzZXJ2ZXJVcmw6IHN0cmluZywgcGFnc2VndXJvVXJsOiBzdHJpbmcsIG1lcmNhZG9wYWdvS2V5OiBzdHJpbmcsIG9yaWdpbjogT3V0Z29PcmlnaW4sIHNlcnZlcjogU2VydmVyLCBkYXRhOiB7bW9kZXJuaW5oYT86IGJvb2xlYW59ID0ge30pe1xyXG4gICAgICB0aGlzLnNlcnZlclVybCA9IHNlcnZlclVybDtcclxuICAgICAgdGhpcy5wYWdzZWd1cm9VcmwgPSBwYWdzZWd1cm9Vcmw7XHJcbiAgICAgIHRoaXMubWVyY2Fkb3BhZ29LZXkgPSBtZXJjYWRvcGFnb0tleTtcclxuICAgICAgdGhpcy5zZXJ2ZXIgPSBzZXJ2ZXI7XHJcbiAgICAgIHRoaXMub3JpZ2luID0gb3JpZ2luO1xyXG5cclxuICAgICAgaWYgKGRhdGEubW9kZXJuaW5oYSkgdGhpcy5tb2Rlcm5pbmhhID0gZGF0YS5tb2Rlcm5pbmhhO1xyXG4gICB9XHJcblxyXG4gICAvKipcclxuICAgICogTGltcGFyIGRhZG9zIGRhIGNvbXByYS92ZW5kYS5cclxuICAgICogRGV2ZSBzZXIgY2hhbWFkbyBubyBjb21lw6dvIGRlIGNhZGEgY29tcHJhL3ZlbmRhIHByYSBldml0YXIgcXVlIHVtYSBjb21wcmEgYW50ZXJpb3IgaW5mbHVlbmNpZSBjb21wcmFzIGZ1dHVyYXMuXHJcbiAgICAqL1xyXG4gICBwcml2YXRlIGNsZWFuQ2FjaGUoY2xlYW5QdXJjaGFzZUNhY2hlID0gdHJ1ZSkge1xyXG5cclxuICAgICAgaWYgKGNsZWFuUHVyY2hhc2VDYWNoZSkge1xyXG4gICAgICAgICB0aGlzLmV2ZW50RGV0YWlsID0gdW5kZWZpbmVkO1xyXG4gICAgICAgICB0aGlzLnB1cmNoYXNlID0gdW5kZWZpbmVkO1xyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLnVzZXIgPSB1bmRlZmluZWQ7XHJcbiAgICAgIHRoaXMuY2FydCA9IHtcclxuICAgICAgICAgaXRlbXM6IFtdXHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5wcm9kdWN0Q2FydCA9IHtcclxuICAgICAgICAgaXRlbXM6IFtdXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMuX3ZhbHVlID0gMDtcclxuICAgICAgdGhpcy5wYXltZW50ID0gbmV3IFBheW1lbnQoKTtcclxuICAgICAgdGhpcy50aWNrZXREYXRhID0gdW5kZWZpbmVkO1xyXG4gICAgICB0aGlzLmluc3RhbGxtZW50cyA9IFtdO1xyXG4gICAgICB0aGlzLmlzU2VsbGluZyA9IGZhbHNlO1xyXG4gICAgICB0aGlzLnByb2R1Y3RzU2FsZSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmluc2NyaXB0aW9uUGFydGljaXBhbnREYXRhID0gW107XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRTYWxlSW5zdGFsbG1lbnQgPSAtMTtcclxuICAgICAgdGhpcy5wdXJjaGFzZXIgPSB1bmRlZmluZWQ7XHJcbiAgICAgIHRoaXMuX3BheW1lbnRNZXRob2QgPSB1bmRlZmluZWQ7XHJcbiAgICAgIHRoaXMuX3BheW1lbnRTYWxlTWV0aG9kID0gdW5kZWZpbmVkO1xyXG4gICAgICB0aGlzLl9kZWxpdmVyeU1ldGhvZCA9IHVuZGVmaW5lZDtcclxuICAgICAgdGhpcy5fZ3Jvc3NWYWx1ZSA9IHVuZGVmaW5lZDtcclxuICAgfVxyXG5cclxuICAgLyoqXHJcbiAgICAqIFNlbmRlciBoYXNoIMOpIHV0aWxpemFkbyBuYXMgY29tcHJhcyBkZSBwYWdzZWd1cm8gcHJhIGlkZW50aWZpY2FyIG8gY29tcHJhZG9yLlxyXG4gICAgKi9cclxuICAgcHJpdmF0ZSBpbml0U2VuZGVySGFzaCgpe1xyXG4gICAgICB0aGlzLnNlbmRlckhhc2ggPSBuZXcgUHJvbWlzZShvayA9PiB7XHJcbiAgICAgICAgIFBhZ1NlZ3Vyb0RpcmVjdFBheW1lbnQub25TZW5kZXJIYXNoUmVhZHkoKHJlc3BvbnNlOiB7IHN0YXR1czogc3RyaW5nOyBzZW5kZXJIYXNoOiBzdHJpbmc7IH0pID0+IHtcclxuICAgICAgICAgICAgaWYocmVzcG9uc2Uuc3RhdHVzID09ICdlcnJvcicpIHtcclxuICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgb2socmVzcG9uc2Uuc2VuZGVySGFzaCk7IC8vSGFzaCBlc3RhcsOhIGRpc3BvbsOtdmVsIG5lc3RhIHZhcmnDoXZlbC5cclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgIH0pO1xyXG4gICAgICB9KTsgIFxyXG4gICB9XHJcblxyXG4gICBwcml2YXRlIHN0YXJ0UGFnc2VndXJvU2Vzc2lvbiAoKTogT2JzZXJ2YWJsZTxQYWdzZWd1cm9TZXNzaW9uPiB7XHJcbiAgICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PFBhZ3NlZ3Vyb1Nlc3Npb24+KHRoaXMuc2VydmVyVXJsICsgJ2FwaS9nZXRJZFNlc3Npb24nKTtcclxuICAgfVxyXG5cclxuICAgLy9mb3JtYSByb2J1c3RhIGRlIGNhcnJlZ2FyIHBhZ3NlZ3VybywgZ2FyYW50ZSBjYXJyZWdhciBzY3JpcHQgZSBkZXBvaXMgY2FycmVnYXIgc2Vzc2lvbklEXHJcbiAgIHB1YmxpYyBwYWdzZWd1cm9SZXNvbHZlKCk6IFByb21pc2U8Ym9vbGVhbj57XHJcbiAgICAgIGlmKHRoaXMucGFnc2VndXJvTG9hZCl7XHJcbiAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShvayA9PiBvayh0cnVlKSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgIC8vVmVyaWZpY2Egc2UgasOhIGZvaSBpbmljaWFkbyBvIGNhcnJlZ2FtZW50b1xyXG4gICAgICAgICBpZighdGhpcy5wYWdzZWd1cm9Qcm9taXNlKXtcclxuICAgICAgICAgICAgdGhpcy5wYWdzZWd1cm9Qcm9taXNlID0gbmV3IFByb21pc2UoKG9rLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgICAgbGV0IHNjcmlwdDogSFRNTFNjcmlwdEVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcclxuICAgICAgICAgICAgICAgc2NyaXB0LmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMuc3RhcnRQYWdzZWd1cm9TZXNzaW9uKCkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQuc2Vzc2lvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNlc3Npb25JZCA9IHJlc3VsdC5zZXNzaW9uLmlkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBQYWdTZWd1cm9EaXJlY3RQYXltZW50LnNldFNlc3Npb25JZChyZXN1bHQuc2Vzc2lvbi5pZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW5pdFNlbmRlckhhc2goKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy90aGlzLnNlbmRlckhhc2ggPSBQYWdTZWd1cm9EaXJlY3RQYXltZW50LmdldFNlbmRlckhhc2goKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vVGVybWluYWRvIGNhcnJlZ2FtZW50bywgYWp1c3RhbmRvIHZhcmnDoXZlaXNcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wYWdzZWd1cm9Mb2FkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wYWdzZWd1cm9Qcm9taXNlID0gdW5kZWZpbmVkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvayh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0KFwiTyBQYWdTZWd1cm8gbsOjbyBlc3TDoSBkaXNwb27DrXZlbC4gVGVudGUgbm92YW1lbnRlIG1haXMgdGFyZGUuXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgc2NyaXB0LnNyYyA9IHRoaXMucGFnc2VndXJvVXJsO1xyXG4gICAgICAgICAgICAgICBkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKHNjcmlwdCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICB9XHJcblxyXG4gICAgICAgICByZXR1cm4gdGhpcy5wYWdzZWd1cm9Qcm9taXNlO1xyXG4gICAgICB9XHJcbiAgIH1cclxuXHJcbiAgIC8vIENhcnJlZ2FtZW50byBkbyBtZXJjYWRvcGFnb1xyXG4gICBwdWJsaWMgbWVyY2Fkb3BhZ29SZXNvbHZlKCkge1xyXG4gICAgICBpZih0aGlzLm1lcmNhZG9wYWdvTG9hZCl7XHJcbiAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShvayA9PiBvayh0cnVlKSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgIC8vVmVyaWZpY2Egc2UgasOhIGZvaSBpbmljaWFkbyBvIGNhcnJlZ2FtZW50b1xyXG4gICAgICAgICBpZighdGhpcy5tZXJjYWRvcGFnb1Byb21pc2Upe1xyXG4gICAgICAgICAgICB0aGlzLm1lcmNhZG9wYWdvUHJvbWlzZSA9IG5ldyBQcm9taXNlKG9rID0+IHtcclxuICAgICAgICAgICAgICAgbGV0IHNjcmlwdDogSFRNTFNjcmlwdEVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcclxuICAgICAgICAgICAgICAgc2NyaXB0LmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIE1lcmNhZG9wYWdvLnNldFB1Ymxpc2hhYmxlS2V5KHRoaXMubWVyY2Fkb3BhZ29LZXkpO1xyXG4gICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgLy9UZXJtaW5hZG8gY2FycmVnYW1lbnRvLCBhanVzdGFuZG8gdmFyacOhdmVpc1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLm1lcmNhZG9wYWdvTG9hZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMubWVyY2Fkb3BhZ29Qcm9taXNlID0gdW5kZWZpbmVkO1xyXG4gICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgb2sodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICBzY3JpcHQuc3JjID0gXCJodHRwczovL3NlY3VyZS5tbHN0YXRpYy5jb20vc2RrL2phdmFzY3JpcHQvdjEvbWVyY2Fkb3BhZ28uanNcIjtcclxuICAgICAgICAgICAgICAgZG9jdW1lbnQuaGVhZC5hcHBlbmRDaGlsZChzY3JpcHQpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgfVxyXG4gICAgICAgICBcclxuICAgICAgICAgcmV0dXJuIHRoaXMubWVyY2Fkb3BhZ29Qcm9taXNlO1xyXG4gICAgICB9XHJcbiAgIH1cclxuXHJcbiAgIC8vIEJ1c2NhIHZhbG9yIGRhcyBwYXJjZWxhcy4gRGV2ZSBjYXJyZWdhciBwcmltZWlybyBvIHBhZ3NlZ3Vyby9tZXJjYWRvcGFnb1xyXG4gICBwdWJsaWMgZ2V0SW5zdGFsbG1lbnRzKGNhcmROdW1iZXI/OiBzdHJpbmcpOiBQcm9taXNlPEluc3RhbGxtZW50c1tdPiB7XHJcbiAgICAgIGlmIChjYXJkTnVtYmVyKSB7XHJcbiAgICAgICAgIGNhcmROdW1iZXIgPSBjYXJkTnVtYmVyLnJlcGxhY2UoLyAvZywgXCJcIik7XHJcbiAgICAgICAgIC8vIGNvbnNvbGUubG9nKFwiUEVHQU5ETyBJTlNUQUxMTUVOVFNcIiwgY2FyZE51bWJlcik7XHJcbiAgIFxyXG4gICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHN1Y2Nlc3MsIGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGFtb3VudCA9IHRoaXMudmFsdWU7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5wbGF0Zm9ybSA9PSBcInBhZ3NlZ3Vyb1wiICYmIHRoaXMucGFnc2VndXJvTG9hZCkge1xyXG4gICAgICAgICAgICAgICB0aGlzLmdldEJyYW5kKGNhcmROdW1iZXIhKS50aGVuKFxyXG4gICAgICAgICAgICAgICAgICBicmFuZCA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgIHRoaXMucGF5bWVudC5jcmVkaXQuYnJhbmQgPSBicmFuZDtcclxuICAgICAgXHJcbiAgICAgICAgICAgICAgICAgICAgIGxldCBwYWdzZWd1cm9QYXJhbXM6IGFueSA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYW1vdW50LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmFuZDogIGJyYW5kLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOiAocmVzcG9uc2U6IHsgaW5zdGFsbG1lbnRzOiB7IFt4OiBzdHJpbmddOiBJbnN0YWxsbWVudHNbXSB9OyB9KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW5zdGFsbG1lbnRzID0gcmVzcG9uc2UuaW5zdGFsbG1lbnRzW2JyYW5kXTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgc3VjY2VzcyhyZXNwb25zZS5pbnN0YWxsbWVudHNbYnJhbmRdKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3I6IChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5lcnJvcihcIkZhbGhvdSBubyBnZXRJbnN0YWxsbWVudHNcIiwgcmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuZXZlbnREZXRhaWwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMubW9kZXJuaW5oYSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5ldmVudERldGFpbC5waHlzaWNhbEluc3RhbGxtZW50c1dpdGhvdXRJbnRlcmVzdCA+IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGFnc2VndXJvUGFyYW1zW1wibWF4SW5zdGFsbG1lbnROb0ludGVyZXN0XCJdID0gdGhpcy5ldmVudERldGFpbC5waHlzaWNhbEluc3RhbGxtZW50c1dpdGhvdXRJbnRlcmVzdDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5ldmVudERldGFpbC5pbnN0YWxsbWVudHNXaXRob3V0SW50ZXJlc3QgPiAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZ3NlZ3Vyb1BhcmFtc1tcIm1heEluc3RhbGxtZW50Tm9JbnRlcmVzdFwiXSA9IHRoaXMuZXZlbnREZXRhaWwuaW5zdGFsbG1lbnRzV2l0aG91dEludGVyZXN0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICBcclxuICAgICAgICAgICAgICAgICAgICAgUGFnU2VndXJvRGlyZWN0UGF5bWVudC5nZXRJbnN0YWxsbWVudHMocGFnc2VndXJvUGFyYW1zKTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICApLmNhdGNoKGZhaWwgPT4ge1xyXG4gICAgICAgICAgICAgICAgICBlcnJvcihmYWlsKTtcclxuICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJmYWxob3UgbyBicmFuZFwiLCBmYWlsKTtcclxuICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMucGxhdGZvcm0gPT0gXCJtZXJjYWRvcGFnb1wiICYmIHRoaXMubWVyY2Fkb3BhZ29Mb2FkKSB7XHJcblxyXG4gICAgICAgICAgICAgICBjb25zdCBiaW4gPSBjYXJkTnVtYmVyIS5zdWJzdHJpbmcoMCwgNik7XHJcbiAgICAgICAgICAgICAgIE1lcmNhZG9wYWdvLmdldEluc3RhbGxtZW50cyh7XHJcbiAgICAgICAgICAgICAgICAgIGJpbixcclxuICAgICAgICAgICAgICAgICAgYW1vdW50XHJcbiAgICAgICAgICAgICAgIH0sIChzdGF0dXM6IG51bWJlciwgcmVzcG9uc2U6IHsgcGF5bWVudF9tZXRob2RfaWQ6IGFueTsgcGF5ZXJfY29zdHM6IHsgaW5zdGFsbG1lbnRzOiBhbnk7IGluc3RhbGxtZW50X2Ftb3VudDogYW55OyB0b3RhbF9hbW91bnQ6IGFueTsgaW5zdGFsbG1lbnRfcmF0ZTogbnVtYmVyOyB9W10gfVtdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgIGlmIChzdGF0dXMgPj0gMjAwICYmIHN0YXR1cyA8IDMwMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICB0aGlzLmluc3RhbGxtZW50cyA9IHJlc3BvbnNlWzBdLnBheWVyX2Nvc3RzLm1hcChkYXRhID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgcXVhbnRpdHk6IGRhdGEuaW5zdGFsbG1lbnRzLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICBpbnN0YWxsbWVudEFtb3VudDogZGF0YS5pbnN0YWxsbWVudF9hbW91bnQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdGFsQW1vdW50OiBkYXRhLnRvdGFsX2Ftb3VudCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgaW50ZXJlc3RGcmVlOiBkYXRhLmluc3RhbGxtZW50X3JhdGUgPT0gMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FyZFByb3ZpZGVyOiByZXNwb25zZVswXS5wYXltZW50X21ldGhvZF9pZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgc3VjY2Vzcyh0aGlzLmluc3RhbGxtZW50cyk7XHJcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgIGVycm9yKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJmYWxob3UgYXMgcGFyY2VsYXMgbWVyY2Fkb3BhZ29cIik7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICBlcnJvcignQmlibGlvdGVjYSBkZSBwYWdhbWVudG8gbsOjbyBjYXJyZWdhZGEnKTtcclxuICAgICAgICAgICAgICAgY29uc29sZS5sb2coYHNlbSAke3RoaXMucGxhdGZvcm19YCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgIFxyXG4gICAgICAgICAvL09idGVyIHBhcmNlbGFzIHBhZ3NlZ3VybyBzZW0gaW5mb3JtYcOnw6NvIGRvIGNhcnTDo28uXHJcbiAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgoc3VjY2VzcywgZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgbGV0IHBhZ3NlZ3Vyb1BhcmFtczogYW55ID0ge1xyXG4gICAgICAgICAgICAgICBhbW91bnQ6IHRoaXMudmFsdWUsXHJcbiAgICAgICAgICAgICAgIHN1Y2Nlc3M6IChyZXNwb25zZTogeyBpbnN0YWxsbWVudHM6IHsgW3g6IHN0cmluZ106IEluc3RhbGxtZW50c1tdIH07IH0pID0+IHtcclxuXHJcbiAgICAgICAgICAgICAgICAgIC8vTyBwYWdzZWd1cm8gcmVzcG9uZGUgY29tIHbDoXJpb3MgdGlwb3MgZGUgY2FydMO1ZXMuIFZlcmlmaXF1ZWkgcXVlIG9zIG1haXMgY29tdW5zIHJlc3BvbmRlbSBjb20gYSBtZXNtYSBxdWFudGlkYWRlIGRlIHBhcmNlbGFzLlxyXG4gICAgICAgICAgICAgICAgICAvL1VtYSBhYm9yZGFnZW0gbWFpcyBzZWd1cmEgc2VyaWEgc2VtcHJlIHVzYXIgYSByZXNwb3N0YSBjb20gbyBtZW5vciBuw7ptZXJvIGRlIHBhcmNlbGFzLlxyXG4gICAgICAgICAgICAgICAgICB0aGlzLmluc3RhbGxtZW50cyA9IHJlc3BvbnNlLmluc3RhbGxtZW50c1tcInZpc2FcIl07XHJcbiAgICAgICAgICAgICAgICAgIHN1Y2Nlc3MocmVzcG9uc2UuaW5zdGFsbG1lbnRzW1widmlzYVwiXSk7XHJcbiAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgIGVycm9yOiAocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICBlcnJvcihyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoXCJGYWxob3Ugbm8gZ2V0SW5zdGFsbG1lbnRzIHNlbSBjYXJ0w6NvXCIsIHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG4gICBcclxuICAgICAgICAgICAgaWYgKHRoaXMuZXZlbnREZXRhaWwpIHtcclxuICAgICAgICAgICAgICAgaWYgKHRoaXMubW9kZXJuaW5oYSkge1xyXG4gICAgICAgICAgICAgICAgICBpZiAodGhpcy5ldmVudERldGFpbC5waHlzaWNhbEluc3RhbGxtZW50c1dpdGhvdXRJbnRlcmVzdCA+IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgcGFnc2VndXJvUGFyYW1zW1wibWF4SW5zdGFsbG1lbnROb0ludGVyZXN0XCJdID0gdGhpcy5ldmVudERldGFpbC5waHlzaWNhbEluc3RhbGxtZW50c1dpdGhvdXRJbnRlcmVzdDtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICBpZiAodGhpcy5ldmVudERldGFpbC5pbnN0YWxsbWVudHNXaXRob3V0SW50ZXJlc3QgPiAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgIHBhZ3NlZ3Vyb1BhcmFtc1tcIm1heEluc3RhbGxtZW50Tm9JbnRlcmVzdFwiXSA9IHRoaXMuZXZlbnREZXRhaWwuaW5zdGFsbG1lbnRzV2l0aG91dEludGVyZXN0O1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICBcclxuICAgICAgICAgICAgUGFnU2VndXJvRGlyZWN0UGF5bWVudC5nZXRJbnN0YWxsbWVudHMocGFnc2VndXJvUGFyYW1zKTtcclxuICAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgfVxyXG5cclxuICAgLy9wZWdhciBhIGJhbmRlaXJhIGRvIGNhcnTDo28gKHBhZ3NlZ3VybylcclxuICAgcHVibGljIGdldEJyYW5kKGNhcmROdW1iZXI6IHN0cmluZyk6IFByb21pc2U8c3RyaW5nPiB7XHJcbiAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgoc3VjY2VzcywgZXJyb3IpID0+IHtcclxuICAgICAgICAgUGFnU2VndXJvRGlyZWN0UGF5bWVudC5nZXRCcmFuZCh7XHJcbiAgICAgICAgICAgIGNhcmRCaW46IGNhcmROdW1iZXIsXHJcbiAgICAgICAgICAgIHN1Y2Nlc3M6IChyZXNwb25zZTogeyBicmFuZDogeyBuYW1lOiBzdHJpbmc7IH07IH0pID0+IHtcclxuICAgICAgICAgICAgICAgc3VjY2VzcyhyZXNwb25zZS5icmFuZC5uYW1lKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZXJyb3I6IChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICBlcnJvcihyZXNwb25zZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gICB9XHJcblxyXG4gICAvKipcclxuICAgICogVmFsaWRhw6fDo28gZG9zIGRhZG9zIGRvIHVzdcOhcmlvLlxyXG4gICAgKi9cclxuICAgcHJpdmF0ZSBwZXJzb25hbERhdGFWYWxpZGF0ZSgpOiBib29sZWFue1xyXG4gICAgICBpZiAoIXRoaXMudXNlcikgcmV0dXJuIGZhbHNlO1xyXG5cclxuICAgICAgbGV0IGFjdHVhbFBlcnNvbmFsVmFsaWRhdGUgPSB0cnVlO1xyXG5cclxuICAgICAgaWYodGhpcy51c2VyLmVtYWlsLmluY2x1ZGVzKFwiQGZhY2Vib29rLmNvbVwiKSB8fCAhdGhpcy51c2VyLmVtYWlsKXtcclxuICAgICAgICAgYWN0dWFsUGVyc29uYWxWYWxpZGF0ZSA9IGZhbHNlO1xyXG4gICAgICAgICB0aGlzLnVzZXIuZW1haWwgPSBcIlwiOyAvL0xpbXBhbmRvIGVtYWlsXHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmKCF0aGlzLnVzZXIucGhvbmUpe1xyXG4gICAgICAgICBhY3R1YWxQZXJzb25hbFZhbGlkYXRlID0gZmFsc2U7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmKCF0aGlzLnVzZXIuY3BmKXtcclxuICAgICAgICAgYWN0dWFsUGVyc29uYWxWYWxpZGF0ZSA9IGZhbHNlO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZighdGhpcy51c2VyLmJpcnRoZGF0ZSkge1xyXG4gICAgICAgICBhY3R1YWxQZXJzb25hbFZhbGlkYXRlID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIHJldHVybiBhY3R1YWxQZXJzb25hbFZhbGlkYXRlO1xyXG4gICB9XHJcblxyXG4gICAvKipcclxuICAgICogRnVuw6fDo28gcXVlIGNhcnJlZ2Egb3MgZGFkb3MgZG8gZXZlbnRvIGFudGVzIGRlIGluaWNpYXIgYSB2ZW5kYS5cclxuICAgICogQHBhcmFtIGV2ZW50SWQgTyBJRCBkbyBldmVudG8uXHJcbiAgICAqIEBwYXJhbSB1c2VySWQgTyBJRCBkbyB1c3XDoXJpby4gT3MgZGFkb3MgZG8gdXN1w6FyaW8gc8OjbyBiYWl4YWRvcyBhcGVuYXMgc2UgbyBwYXLDom1ldHJvIGZvciBmb3JuZWNpZG8uXHJcbiAgICAqIEB0aHJvd3MgR0VORVJBTF9FUlJPUiBzZSBhIGZ1bsOnw6NvIGZvciBjaGFtYWRhIGFudGVzIGRlIHNhbHZhciBvcyBkYWRvcyBkbyBjYXJyaW5obyBkZSBjb21wcmFzLlxyXG4gICAgKi9cclxuICAgcHVibGljIGFzeW5jIGV2ZW50UHVyY2hhc2VEYXRhKGV2ZW50SWQ6IG51bWJlciwgdXNlcklkPzogbnVtYmVyKXtcclxuICAgICAgaWYgKCF1c2VySWQgJiYgdGhpcy51c2VyKSB1c2VySWQgPSB0aGlzLnVzZXIuaWQ7XHJcblxyXG4gICAgICBjb25zdCBldmVudERldGFpbCA9IGF3YWl0IHRoaXMuZ2V0RXZlbnREZXRhaWwoZXZlbnRJZCk7XHJcbiAgICAgIHRoaXMuc2V0VmFsdWUoKTtcclxuXHJcbiAgICAgIGlmICh1c2VySWQpIHtcclxuICAgICAgICAgYXdhaXQgdGhpcy5nZXRVc2VyRGV0YWlsKGV2ZW50RGV0YWlsLmV2ZW50LmNsaWVudElkLCB1c2VySWQpO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICB9XHJcblxyXG4gICBwcml2YXRlIGFzeW5jIGdldEV2ZW50RGV0YWlsKGV2ZW50SWQ6IG51bWJlcik6IFByb21pc2U8RXZlbnREZXRhaWw+IHtcclxuICAgICAgY29uc3QgZXZlbnREZXRhaWwgPSBhd2FpdCB0aGlzLnNlcnZlciEudGFsa1RvU2VydmVyPEV2ZW50RGV0YWlsPihcImV2ZW50X2RldGFpbC9cIiArIGVuY29kZVVSSUNvbXBvbmVudChldmVudElkKSwge2lnbm9yZVZpc2l0OiB0cnVlfSk7XHJcbiAgICAgIHRoaXMuZXZlbnREZXRhaWwgPSBldmVudERldGFpbDtcclxuXHJcbiAgICAgIHJldHVybiBldmVudERldGFpbDtcclxuICAgfVxyXG5cclxuICAgcHVibGljIGFzeW5jIGdldFVzZXJEZXRhaWwoY29tcGFueUlkOiBudW1iZXIsIHVzZXJJZDogbnVtYmVyKTogUHJvbWlzZTxVc2VyPiB7XHJcbiAgICAgIGNvbnN0IHVzZXIgPSBhd2FpdCB0aGlzLnNlcnZlciEudGFsa1RvU2VydmVyPFVzZXI+KCd1c2VyX2RhdGEnLCB7Y29tcGFueV9pZDogY29tcGFueUlkfSk7XHJcbiAgICAgIHRoaXMudXNlciA9IHVzZXI7XHJcblxyXG4gICAgICByZXR1cm4gdXNlcjtcclxuICAgfVxyXG4gICBcclxuICAgLyoqXHJcbiAgICAqIEZ1bsOnw6NvIHF1ZSBvYnRlbSBvcyBwYXLDom1ldHJvcyBiw6FzaWNvcyBwYXJhIGNyaWFyIHVtYSBjb21wcmEgbm8gc2Vydmlkb3IuXHJcbiAgICAqIEByZXR1cm5zIGBudWxsYCBDYXNvIG8gdXN1w6FyaW8gbsOjbyBlc3RlamEgcmVnaXN0cmFkbywgZSB1bSBgT2JqZWN0YCBjb20gb3MgZGFkb3MgZW0gY2FzbyBjb250csOhcmlvLlxyXG4gICAgKi9cclxuICAgcHJpdmF0ZSBhc3luYyBnZXRQYXltZW50UGFyYW1zKCl7XHJcbiAgICAgIGlmKCF0aGlzLnVzZXIpIHJldHVybiBudWxsO1xyXG5cclxuICAgICAgbGV0IHRvdGFsUHJpY2UgICAgICA9IDA7XHJcbiAgICAgIGxldCB0b3RhbEZpbmFsUHJpY2UgPSAwO1xyXG4gICAgICBsZXQgcHJvZHVjdE51bWJlciAgID0gMTtcclxuICAgICAgY29uc3QgZGF0YTogYW55ID0ge307XHJcbiAgICAgIFxyXG4gICAgICBsZXQgZGVzY3JpcHRpb24gPSBcIlwiO1xyXG4gICAgICAvLyBBZGljaW9uYW5kbyBkYWRvcyBkZSB0b2RvcyBvcyBwcm9kdXRvc1xyXG4gICAgICBmb3IgKGNvbnN0IGl0ZW0gb2YgdGhpcy5jYXJ0Lml0ZW1zKSB7XHJcbiAgICAgICAgIGlmIChpdGVtLnF0ZCA+IDApIHtcclxuICAgICAgICAgICAgZGF0YVsnaXRlbUlkJyArIHByb2R1Y3ROdW1iZXJdID0gaXRlbS50aWNrZXQuaWQudG9TdHJpbmcoKTtcclxuICAgICAgICAgICAgZGF0YVsnaXRlbURlc2NyaXB0aW9uJyArIHByb2R1Y3ROdW1iZXJdID0gaXRlbS50aWNrZXQubmFtZTtcclxuICAgICAgICAgICAgZGF0YVsnaXRlbUFtb3VudCcgKyBwcm9kdWN0TnVtYmVyXSA9IHZhbHVlUm91bmQoaXRlbS50aWNrZXQuZmluYWxfcHJpY2UsICcuJyk7XHJcbiAgICAgICAgICAgIGRhdGFbJ2l0ZW1DbGllbnRBbW91bnQnICsgcHJvZHVjdE51bWJlcl0gPSB2YWx1ZVJvdW5kKGl0ZW0udGlja2V0LnByaWNlLCAnLicpO1xyXG4gICAgICAgICAgICBkYXRhWydpdGVtUXVhbnRpdHknICsgcHJvZHVjdE51bWJlcl0gPSBpdGVtLnF0ZC50b1N0cmluZygpO1xyXG4gICAgICAgICAgICBkYXRhWydpdGVtUmVzZXJ2YXRpb25JZCcgKyBwcm9kdWN0TnVtYmVyXSA9IGl0ZW0ucmVzZXJ2YXRpb25zLm1hcChyID0+IHIuaWQpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuaW5zY3JpcHRpb25QYXJ0aWNpcGFudERhdGEpIHtcclxuICAgICAgICAgICAgICAgZGF0YVsnaXRlbVBhcnRpY2lwYW50cycgKyBwcm9kdWN0TnVtYmVyXSA9IFwiXCI7XHJcblxyXG4gICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuaW5zY3JpcHRpb25QYXJ0aWNpcGFudERhdGEubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgY29uc3QgcGFydGljaXBhbnREYXRhID0gdGhpcy5pbnNjcmlwdGlvblBhcnRpY2lwYW50RGF0YVtpXTtcclxuXHJcbiAgICAgICAgICAgICAgICAgIGRhdGFbJ2l0ZW1QYXJ0aWNpcGFudHMnICsgcHJvZHVjdE51bWJlcl0gKz0gYG5hbWU6JHtwYXJ0aWNpcGFudERhdGEubmFtZX1gO1xyXG4gICAgICAgICAgICAgICAgICBkYXRhWydpdGVtUGFydGljaXBhbnRzJyArIHByb2R1Y3ROdW1iZXJdICs9IGAsZW1haWw6JHtwYXJ0aWNpcGFudERhdGEuZW1haWx9YDtcclxuICAgICAgICAgICAgICAgICAgaWYgKHBhcnRpY2lwYW50RGF0YS5jcGYpIGRhdGFbJ2l0ZW1QYXJ0aWNpcGFudHMnICsgcHJvZHVjdE51bWJlcl0gKz0gYCxjcGY6JHtwYXJ0aWNpcGFudERhdGEuY3BmfWA7XHJcbiAgICAgICAgICAgICAgICAgIGlmIChwYXJ0aWNpcGFudERhdGEucGhvbmUpIGRhdGFbJ2l0ZW1QYXJ0aWNpcGFudHMnICsgcHJvZHVjdE51bWJlcl0gKz0gYCxwaG9uZToke3BhcnRpY2lwYW50RGF0YS5waG9uZX1gO1xyXG4gICAgICAgICAgICAgICAgICBpZiAocGFydGljaXBhbnREYXRhLmJpcnRoZGF0ZSkgZGF0YVsnaXRlbVBhcnRpY2lwYW50cycgKyBwcm9kdWN0TnVtYmVyXSArPSBgLGJpcnRoZGF0ZToke3BhcnRpY2lwYW50RGF0YS5iaXJ0aGRhdGV9YDtcclxuXHJcbiAgICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgKHBhcnRpY2lwYW50RGF0YS5leHRyYV9maWVsZHMgPyBPYmplY3Qua2V5cyhwYXJ0aWNpcGFudERhdGEuZXh0cmFfZmllbGRzKS5sZW5ndGggOiAwKTsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZvcm1GaWVsZElkID0gcGFyc2VJbnQoT2JqZWN0LmtleXMocGFydGljaXBhbnREYXRhLmV4dHJhX2ZpZWxkcyEpW2ldKTtcclxuICAgICAgICAgICAgICAgICAgICAgZGF0YVsnaXRlbVBhcnRpY2lwYW50cycgKyBwcm9kdWN0TnVtYmVyXSArPSBgLGV4dHJhX2ZpZWxkcy8ke2Zvcm1GaWVsZElkfToke3BhcnRpY2lwYW50RGF0YS5leHRyYV9maWVsZHMhW2Zvcm1GaWVsZElkXX1gO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICBpZiAoaSA8IHRoaXMuaW5zY3JpcHRpb25QYXJ0aWNpcGFudERhdGEubGVuZ3RoIC0gMSkgZGF0YVsnaXRlbVBhcnRpY2lwYW50cycgKyBwcm9kdWN0TnVtYmVyXSArPSBcIsKnXCI7XHJcbiAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBwcm9kdWN0TnVtYmVyKys7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB0b3RhbFByaWNlICs9IGl0ZW0udGlja2V0LnByaWNlICogaXRlbS5xdGQ7XHJcbiAgICAgICAgICAgIHRvdGFsRmluYWxQcmljZSArPSBpdGVtLnRpY2tldC5maW5hbF9wcmljZSAqIGl0ZW0ucXRkO1xyXG5cclxuICAgICAgICAgICAgaWYoZGVzY3JpcHRpb24gIT0gXCJcIikgZGVzY3JpcHRpb24gKz0gXCIgfCBcIjtcclxuICAgICAgICAgICAgZGVzY3JpcHRpb24gKz0gaXRlbS5xdGQudG9TdHJpbmcoKSArIFwieCBcIiArIGl0ZW0udGlja2V0Lm5hbWU7XHJcbiAgICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBcclxuICAgICAgZGF0YVsnY2xpZW50SWQnXSA9IHRoaXMuZXZlbnREZXRhaWwhLmV2ZW50LmNsaWVudElkLnRvU3RyaW5nKCk7XHJcbiAgICAgIGRhdGFbJ3NlbmRlckhhc2gnXSA9IGF3YWl0IHRoaXMuc2VuZGVySGFzaDtcclxuICAgICAgZGF0YVsnZnVsbE5hbWUnXSA9IHRoaXMudXNlciEubmFtZTtcclxuICAgICAgZGF0YVsnbmFtZSddID0gdGhpcy51c2VyIS5uYW1lO1xyXG4gICAgICBpZih0aGlzLnVzZXIhLmNwZikge1xyXG4gICAgICAgICBkYXRhWyd1c2VyY3BmJ10gPSB0aGlzLnVzZXIhLmNwZi5yZXBsYWNlKC9cXC58LS9nLCAnJyk7XHJcbiAgICAgICAgIGRhdGFbJ2NwZiddID0gdGhpcy51c2VyIS5jcGYucmVwbGFjZSgvXFwufC0vZywgJycpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgICBkYXRhWyd1c2VyY3BmJ10gPSAnJztcclxuICAgICAgICAgZGF0YVsnY3BmJ10gPSAnJztcclxuICAgICAgfVxyXG4gICAgICBkYXRhWydlbWFpbCddID0gdGhpcy51c2VyIS5lbWFpbDtcclxuICAgICAgaWYodGhpcy51c2VyIS5waG9uZSkge1xyXG4gICAgICAgICBkYXRhWydjb21wbGV0ZVBob25lJ10gPSB0aGlzLnVzZXIhLnBob25lLnJlcGxhY2UoL1soXyktXFxzXS9nLCAnJyk7XHJcbiAgICAgICAgIGRhdGFbJ2FyZWFDb2RlJ10gPSBkYXRhWydjb21wbGV0ZVBob25lJ10uc3Vic3RyKDAsIDIpO1xyXG4gICAgICAgICBkYXRhWydwaG9uZSddID0gZGF0YVsnY29tcGxldGVQaG9uZSddLnN1YnN0cmluZygyKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgZGF0YVsnY29tcGxldGVQaG9uZSddID0gJyc7XHJcbiAgICAgICAgIGRhdGFbJ2FyZWFDb2RlJ10gPSAnJztcclxuICAgICAgICAgZGF0YVsncGhvbmUnXSA9ICcnO1xyXG4gICAgICB9XHJcbiAgICAgIGRhdGFbJ3NlbmRlckJpcnRoZGF0ZSddID0gdGhpcy51c2VyIS5iaXJ0aGRhdGU7XHJcbiAgICAgIGRhdGFbJ2JpcnRoZGF5J10gPSBkYXRhWydzZW5kZXJCaXJ0aGRhdGUnXTtcclxuICAgICAgZGF0YVsndXNlcklkJ10gPSB0aGlzLnVzZXIhLmlkLnRvU3RyaW5nKCk7XHJcbiAgICAgIGRhdGFbJ2NoYW5nZVVzZXInXSA9IGZhbHNlO1xyXG4gICAgICBkYXRhWydldmVudElkJ10gPSB0aGlzLmV2ZW50RGV0YWlsIS5ldmVudC5pZDtcclxuICAgICAgZGF0YVsncHVyY2hhc2VJZCddID0gdGhpcy5wdXJjaGFzZT8uaWQ7XHJcbiAgICAgIGRhdGFbJ2NvbXBhbnlVc2VySWQnXSA9IHRoaXMudXNlciEuY29tcGFueVVzZXJJZDtcclxuICAgICAgZGF0YVsndG90YWxQcmljZSddID0gdmFsdWVSb3VuZCh0b3RhbFByaWNlLCAnLicpO1xyXG4gICAgICBkYXRhWyd0b3RhbEZpbmFsUHJpY2UnXSA9IHZhbHVlUm91bmQodG90YWxGaW5hbFByaWNlLCAnLicpO1xyXG4gICAgICBkYXRhWydldmVudE5hbWUnXSA9IHRoaXMuZXZlbnREZXRhaWwhLmV2ZW50Lm5hbWU7XHJcbiAgICAgIGRhdGFbJ29yaWdpbiddID0gdGhpcy5vcmlnaW47XHJcblxyXG4gICAgICBpZiAodGhpcy5kaXNjb3VudCkgZGF0YVsnZGlzY291bnRJZCddID0gdGhpcy5kaXNjb3VudC5pZDtcclxuXHJcbiAgICAgIGlmICh0aGlzLmV2ZW50RGV0YWlsIS5wYXltZW50TWV0aG9kID09IFwibWVyY2Fkb3BhZ29cIikge1xyXG4gICAgICAgICBkYXRhW1widHJhbnNhY3Rpb25fYW1vdW50XCJdID0gdG90YWxGaW5hbFByaWNlO1xyXG4gICAgICAgICBkYXRhW1wiZGVzY3JpcHRpb25cIl0gPSBkZXNjcmlwdGlvbjtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIGRhdGE7XHJcbiAgIH1cclxuXHJcbiAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gICBJTklDSUEgQVMgQ09NUFJBU1xyXG4gICBDaGFtYWRvIGFww7NzIGVzY29saGVyIG9zIGluZ3Jlc3NvcyBlIGNsaWNhciBlbSBjb21wcmFyXHJcbiAgIEB0aHJvd3MgVkFMSURBVEVfRU1BSUwgY2FzbyBhIGNvbXByYSBzZWphIHBhZ2EgZSBvIHVzdcOhcmlvIGFpbmRhIG7Do28gdGl2ZXIgdmFsaWRhZG8gc2V1IGVtYWlsLlxyXG4gICBAdGhyb3dzIFVOQVVUSEVOVElDQVRFRCBjYXNvIG8gaWQgZGUgdXN1w6FyaW8gbsOjbyBzZWphIGluZm9ybWFkby5cclxuICAgQHRocm93cyBUSUNLRVRfVU5BVkFJTEFCTEUgY2FzbyBvIGluZ3Jlc3NvIHNvbGljaXRhZG8gbsOjbyBlc3RlamEgbWFpcyBkaXNwb27DrXZlbC5cclxuICAgQHBhcmFtIGNhcnQgTyBjYXJyaW5obyBkZSBjb21wcmFzLCBjb20gbyBkZXNjb250bywgY2FzbyBleGlzdGEuXHJcbiAgIEBwYXJhbSBldmVudElkIE8gSUQgZG8gZXZlbnRvLlxyXG4gICBAcGFyYW0gdXNlcklkIE8gSUQgZG8gdXN1w6FyaW8sIGNhc28gZWxlIGVzdGVqYSBsb2dhZG8gbm8gc2lzdGVtYS5cclxuICAgQHBhcmFtIGNsZWFuUHVyY2hhc2VDYWNoZSBTZSBvcyBkYWRvcyBkYSBjb21wcmEgZGV2ZW0gc2VyIGxpbXBvcyBkbyBjYWNoZS4gYHRydWVgIHBvciBwYWRyw6NvLCBzw7MgZGV2ZSBzZXIgc29icmVzY3JpdG8gc2UgbmVjZXNzw6FyaW8uXHJcbiAgICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gICBwdWJsaWMgYXN5bmMgaW5pdChjYXJ0OiBDYXJ0LCBldmVudElkOiBudW1iZXIsIHVzZXJJZD86IG51bWJlciwgY2xlYW5QdXJjaGFzZUNhY2hlID0gdHJ1ZSk6IFByb21pc2U8T3V0UGF5UmVzdWx0PntcclxuICAgICAgdGhpcy5jbGVhbkNhY2hlKGNsZWFuUHVyY2hhc2VDYWNoZSk7XHJcbiAgICAgIFxyXG4gICAgICB0aGlzLmlzU2VsbGluZyA9IGZhbHNlO1xyXG4gICAgICBcclxuICAgICAgdGhpcy5jYXJ0ID0gY2FydDtcclxuXHJcbiAgICAgIGF3YWl0IHRoaXMuZXZlbnRQdXJjaGFzZURhdGEoZXZlbnRJZCwgdXNlcklkKTtcclxuXHJcbiAgICAgIGlmICh0aGlzLnZhbHVlID4gMCkge1xyXG4gICAgICAgICBpZih0aGlzLnBsYXRmb3JtID09IFwicGFnc2VndXJvXCIpe1xyXG4gICAgICAgICAgICBpZiggIShhd2FpdCB0aGlzLnBhZ3NlZ3Vyb1Jlc29sdmUoKSkgKXtcclxuICAgICAgICAgICAgICAgdGhyb3cgbmV3IE91dFBheUVycm9yKFwiTsOjbyBjYXJyZWdvdSBQYWdzZWd1cm9cIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKCEgKGF3YWl0IHRoaXMubWVyY2Fkb3BhZ29SZXNvbHZlKCkpICl7XHJcbiAgICAgICAgICAgICAgIHRocm93IG5ldyBPdXRQYXlFcnJvcihcIk7Do28gY2FycmVnb3UgTWVyY2Fkb3BhZ29cIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgfVxyXG5cclxuICAgICAgICAgZm9yIChsZXQgaXRlbSBvZiB0aGlzLmNhcnQuaXRlbXMpIHtcclxuICAgICAgICAgICAgaWYgKGl0ZW0udGlja2V0LnByaWNlID09IDApIGl0ZW0ucXRkID0gMDtcclxuICAgICAgICAgfVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAvL0NvbXByYXMgc2VtIHZhbG9yIHPDo28gY29uc2lkZXJhZGFzIGdyYXR1aXRhcyBlIHRlbSBzZXVzIGl0ZW5zIGFqdXN0YWRvcyBwYXJhIHZhbG9yIDBcclxuICAgICAgICAgdGhpcy5fcGF5bWVudE1ldGhvZCA9IFwiZnJlZVwiO1xyXG4gICAgICAgICBpZiAoIXRoaXMuY2FydC5kaXNjb3VudCB8fCB0aGlzLmNhcnQuZGlzY291bnQudmFsdWUgIT0gMTAwKSB7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGl0ZW0gb2YgdGhpcy5jYXJ0Lml0ZW1zKSB7XHJcbiAgICAgICAgICAgICAgIGlmIChpdGVtLnRpY2tldC5wcmljZSA+IDApIGl0ZW0ucXRkID0gMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmICghdXNlcklkKSB0aHJvdyBuZXcgT3V0UGF5RXJyb3IoXCJWb2PDqiBuw6NvIGVzdMOhIGxvZ2Fkby5cIiwgT3V0UGF5RXJyb3JDb2RlLlVOQVVUSEVOVElDQVRFRClcclxuICAgICAgdHJ5IHtcclxuICAgICAgICAgbGV0IHJlc3BvbnNlID0gYXdhaXQgdGhpcy5zZXJ2ZXIhLnRhbGtUb1NlcnZlcjx7ZXJyb3I6IHVuZGVmaW5lZCwgcHVyY2hhc2U6IFB1cmNoYXNlLCBtZXJjYWRvcGFnbzogYW55LCBwYWdzZWd1cm86IGFueX0gfCB7ZXJyb3I6IHRydWUsIHRpY2tldF9uYW1lOiBzdHJpbmd9IHwge2Vycm9yOiB0cnVlLCBlcnJvck1zZzogc3RyaW5nfT4oXCJkb3VibGVjaGVja190aWNrZXRzXCIsIHsgY2FydDogdGhpcy5jYXJ0Lml0ZW1zLCBwYXJhbXM6IGF3YWl0IHRoaXMuZ2V0UGF5bWVudFBhcmFtcygpIH0sIHsgdHlwZTogXCJQT1NUXCIsIHJldHJpZXM6IDQgfSk7XHJcblxyXG4gICAgICAgICBpZiAocmVzcG9uc2UuZXJyb3IpIHtcclxuICAgICAgICAgICAgaWYgKFwidGlja2V0X25hbWVcIiBpbiByZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICB0aHJvdyBuZXcgT3V0UGF5RXJyb3IoYExpbWl0ZSBkbyBpbmdyZXNzbyAke3Jlc3BvbnNlLnRpY2tldF9uYW1lfSBhdGluZ2lkby4gU2VsZWNpb25lIHVtYSBub3ZhIHF1YW50aWRhZGUuYCwgT3V0UGF5RXJyb3JDb2RlLlRJQ0tFVF9VTkFWQUlMQUJMRSwge290dG86IHRydWUsIG90dG9EYXRhOiB7aW1hZ2U6IFwiYXRlbnRvXCJ9fSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKFwiZXJyb3JNc2dcIiBpbiByZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICB0aHJvdyBuZXcgT3V0UGF5RXJyb3IocmVzcG9uc2UuZXJyb3JNc2csIE91dFBheUVycm9yQ29kZS5HRU5FUkFMX0VSUk9SLCB7b3R0bzogdHJ1ZSwgb3R0b0RhdGE6IHtpbWFnZTogXCJhdGVudG9cIn19KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gU2UgbsOjbyBzb3ViZXIgcXVhbCBmb2kgbyBlcnJvLCBqb2dhciBwYXJhIGV2aXRhciBlcnJvcyBzaWxlbmNpb3Nvcy5cclxuICAgICAgICAgICAgdGhyb3cgcmVzcG9uc2U7XHJcbiAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmIChyZXNwb25zZS5wdXJjaGFzZSkgdGhpcy5wdXJjaGFzZSA9IHJlc3BvbnNlLnB1cmNoYXNlO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5ldmVudERldGFpbD8ucGF5bWVudE1ldGhvZCA9PSBcIm1lcmNhZG9wYWdvXCIpIHtcclxuICAgICAgICAgICAgICAgdGhpcy50aWNrZXREYXRhID0gcmVzcG9uc2UubWVyY2Fkb3BhZ287XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgIHRoaXMudGlja2V0RGF0YSA9IHJlc3BvbnNlLnBhZ3NlZ3VybztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMudmFsdWUgPiAwICYmIHRoaXMucGVyc29uYWxEYXRhVmFsaWRhdGUoKSAmJiAhdGhpcy5ldmVudERldGFpbCEuaGF2ZV9wYXJ0aWNpcGFudF9mb3JtKSB7XHJcbiAgICAgICAgICAgICAgIHJldHVybiB7ZmluaXNoZWQ6IGZhbHNlLCBuZXh0U3RlcDogT3V0UGF5U3RlcC5QQVlNRU5UX01FVEhPRCwgcHVyY2hhc2U6IHRoaXMucHVyY2hhc2V9O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICByZXR1cm4ge2ZpbmlzaGVkOiBmYWxzZSwgbmV4dFN0ZXA6IE91dFBheVN0ZXAuUFVSQ0hBU0VfRk9STSwgcHVyY2hhc2U6IHRoaXMucHVyY2hhc2V9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgIH1cclxuICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcclxuICAgICAgICAgaWYgKGVycm9yLnN0YXR1cyA9PSA0MDApIHtcclxuICAgICAgICAgICAgaWYgKGVycm9yLmVycm9yID09IFwidmVyaWZ5X2VtYWlsXCIgfHwgZXJyb3IuZXJyb3IudmVyaWZ5X2VtYWlsKSB7XHJcblxyXG4gICAgICAgICAgICAgICAvL0RldmUgcmVjdXBlcmFyIG9zIGRhZG9zIGRlIGNvbXByYSwgcG9pcyBtZXNtbyBjb20gbyBlcnJvIGrDoSBjcmlvdSBhIHByZS1yZXNlcnZhXHJcbiAgICAgICAgICAgICAgIGlmIChlcnJvci5lcnJvci5wdXJjaGFzZSkgdGhpcy5wdXJjaGFzZSA9IGVycm9yLmVycm9yLnB1cmNoYXNlO1xyXG4gICAgICAgICAgICAgICBpZiAodGhpcy5ldmVudERldGFpbD8ucGF5bWVudE1ldGhvZCA9PSBcIm1lcmNhZG9wYWdvXCIpIHtcclxuICAgICAgICAgICAgICAgICAgdGhpcy50aWNrZXREYXRhID0gZXJyb3IuZXJyb3IubWVyY2Fkb3BhZ287XHJcbiAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMudGlja2V0RGF0YSA9IGVycm9yLmVycm9yLnBhZ3NlZ3VybztcclxuICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgdGhyb3cgbmV3IE91dFBheUVycm9yKGBFbnZpYW1vcyB1bSBjw7NkaWdvIHBhcmEgJHt0aGlzLnVzZXIhLmVtYWlsfSBwYXJhIHZvY8OqIHZhbGlkYXIgYWJhaXhvLmAsIE91dFBheUVycm9yQ29kZS5WQUxJREFURV9FTUFJTCk7XHJcbiAgICAgICAgICAgIH0gaWYgKGVycm9yLmVycm9yLmVycm9yKSB7XHJcbiAgICAgICAgICAgICAgIHRocm93IG5ldyBPdXRQYXlFcnJvcihlcnJvci5lcnJvci5lcnJvck1zZywgT3V0UGF5RXJyb3JDb2RlLkdFTkVSQUxfRVJST1IpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgIH0gZWxzZSBpZiAoZXJyb3Iuc3RhdHVzID09IDQwMSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgT3V0UGF5RXJyb3IoXCJWb2PDqiBuw6NvIGVzdMOhIGxvZ2Fkby5cIiwgT3V0UGF5RXJyb3JDb2RlLlVOQVVUSEVOVElDQVRFRClcclxuICAgICAgICAgfVxyXG5cclxuICAgICAgICAgLy8gRXZpdGFyIGVycm9zIHNpbGVuY2lvc29zXHJcbiAgICAgICAgIHRocm93IGVycm9yO1xyXG4gICAgICB9XHJcbiAgIH1cclxuXHJcbiAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gICBJTklDSUEgTyBQUk9DRVNTTyBETyBWRU5ERURPUlxyXG4gICBDaGFtYWRvIGFww7NzIGVzY29saGVyIG9zIGluZ3Jlc3NvcyBlIGNsaWNhciBlbSB2ZW5kZXJcclxuICAgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgIHB1YmxpYyBhc3luYyBzZWxsZXJJbml0KGNhcnQ6IENhcnQsIGV2ZW50SWQ6IG51bWJlciwgdXNlcklkOiBudW1iZXIpOiBQcm9taXNlPE91dFBheVJlc3VsdD4ge1xyXG4gICAgICB0aGlzLmNsZWFuQ2FjaGUoKTtcclxuXHJcbiAgICAgIHRoaXMuaXNTZWxsaW5nID0gdHJ1ZTtcclxuICAgICAgdGhpcy5jYXJ0ID0gY2FydDtcclxuXHJcbiAgICAgIGF3YWl0IHRoaXMuZXZlbnRQdXJjaGFzZURhdGEoZXZlbnRJZCwgdXNlcklkKTtcclxuXHJcbiAgICAgIHJldHVybiBhd2FpdCB0aGlzLm5leHRTdGVwU2FsZSgpOyBcclxuICAgfVxyXG5cclxuICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAgIElOSUNJQSBPIFBST0NFU1NPIERPIFZFTkRFRE9SIERFIFBST0RVVE9TXHJcbiAgIENoYW1hZG8gYXDDs3MgZXNjb2xoZXIgb3MgcHJvZHV0b3MgZSBjbGljYXIgZW0gdmVuZGVyXHJcbiAgICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gICBwdWJsaWMgcHJvZHVjdFNlbGxlckluaXQocHJvZHVjdENhcnQ6IFByb2R1Y3RDYXJ0LCBldmVudERldGFpbDogRXZlbnREZXRhaWwsIHVzZXI6IFVzZXIpOiBPdXRQYXlSZXN1bHR7XHJcbiAgICAgIHRoaXMuY2xlYW5DYWNoZSgpO1xyXG5cclxuICAgICAgdGhpcy5ldmVudERldGFpbCA9IGV2ZW50RGV0YWlsO1xyXG4gICAgICB0aGlzLmlzU2VsbGluZyA9IHRydWU7XHJcbiAgICAgIHRoaXMucHJvZHVjdHNTYWxlID0gdHJ1ZTtcclxuICAgICAgdGhpcy51c2VyID0gdXNlcjtcclxuICAgICAgdGhpcy5wcm9kdWN0Q2FydCA9IHByb2R1Y3RDYXJ0O1xyXG5cclxuICAgICAgdGhpcy5zZXRWYWx1ZSgpO1xyXG5cclxuICAgICAgcmV0dXJuIHtmaW5pc2hlZDogZmFsc2UsIG5leHRTdGVwOiBPdXRQYXlTdGVwLlBBWU1FTlRfTUVUSE9ELCBwdXJjaGFzZTogdGhpcy5wdXJjaGFzZX07XHJcbiAgIH1cclxuXHJcbiAgIC8qKlxyXG4gICAgKiBzYXZlRm9ybURhdGFcclxuICAgICogQ2hhbWFkbyBhcMOzcyBwcmVlbmNoZXIgb3MgZGFkb3MgZGUgZm9ybXVsw6FyaW8uXHJcbiAgICAqL1xyXG4gICBwdWJsaWMgYXN5bmMgc2F2ZUZvcm1EYXRhKHBhcnRpY2lwYW50RGF0YTogeyBuYW1lOiBzdHJpbmc7IGVtYWlsOiBzdHJpbmc7IHBob25lPzogc3RyaW5nOyBjcGY/OiBzdHJpbmc7IGJpcnRoZGF0ZT86IHN0cmluZzsgdGlja2V0X2lkOiBudW1iZXI7IGV4dHJhX2ZpZWxkcz86IHsgW2ZpZWxkSWQ6IG51bWJlcl06IHN0cmluZzsgfTsgfVtdKTogUHJvbWlzZTxPdXRQYXlSZXN1bHQ+IHtcclxuICAgICAgdGhpcy5pbnNjcmlwdGlvblBhcnRpY2lwYW50RGF0YSA9IHBhcnRpY2lwYW50RGF0YTtcclxuICAgICAgXHJcbiAgICAgIGlmICh0aGlzLmlzU2VsbGluZykge1xyXG4gICAgICAgICBpZiAodGhpcy5kZWxpdmVyeU1ldGhvZCA9PSBcImRpZ2l0YWxcIiAmJiB0aGlzLmluc2NyaXB0aW9uUGFydGljaXBhbnREYXRhLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICAgdGhpcy5wdXJjaGFzZXIgPSBhd2FpdCB0aGlzLnNlcnZlciEudGFsa1RvU2VydmVyKFwicHVyY2hhc2VyXCIsIHtlbWFpbDogdGhpcy5pbnNjcmlwdGlvblBhcnRpY2lwYW50RGF0YVswXS5lbWFpbH0sIHtyZXRyaWVzOiA0fSk7XHJcbiAgICAgICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XHJcbiAgICAgICAgICAgICAgIGlmIChlcnJvci5lcnJvciA9PSBcIm5hbWVcIikge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLnB1cmNoYXNlciA9IGF3YWl0IHRoaXMuc2VydmVyIS50YWxrVG9TZXJ2ZXIoXCJwdXJjaGFzZXIvY3JlYXRlXCIsIHtcclxuICAgICAgICAgICAgICAgICAgICAgbmFtZTogdGhpcy5pbnNjcmlwdGlvblBhcnRpY2lwYW50RGF0YVswXS5uYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICBlbWFpbDogdGhpcy5pbnNjcmlwdGlvblBhcnRpY2lwYW50RGF0YVswXS5lbWFpbCxcclxuICAgICAgICAgICAgICAgICAgICAgcGhvbmU6IHRoaXMuaW5zY3JpcHRpb25QYXJ0aWNpcGFudERhdGFbMF0ucGhvbmUsXHJcbiAgICAgICAgICAgICAgICAgICAgIGNwZjogdGhpcy5pbnNjcmlwdGlvblBhcnRpY2lwYW50RGF0YVswXS5jcGYsXHJcbiAgICAgICAgICAgICAgICAgICAgIGJpcnRoZGF0OiB0aGlzLmluc2NyaXB0aW9uUGFydGljaXBhbnREYXRhWzBdLmJpcnRoZGF0ZVxyXG4gICAgICAgICAgICAgICAgICB9LCB7XHJcbiAgICAgICAgICAgICAgICAgICAgIHR5cGU6IFwiUE9TVFwiLFxyXG4gICAgICAgICAgICAgICAgICAgICByZXRyaWVzOiA0LFxyXG4gICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgdGhyb3cgZXJyb3I7XHJcbiAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICB9XHJcblxyXG4gICAgICAgICByZXR1cm4gYXdhaXQgdGhpcy5uZXh0U3RlcFNhbGUoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgaWYgKHRoaXMudmFsdWUgPiAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB7ZmluaXNoZWQ6IGZhbHNlLCBuZXh0U3RlcDogT3V0UGF5U3RlcC5QQVlNRU5UX01FVEhPRCwgcHVyY2hhc2U6IHRoaXMucHVyY2hhc2V9O1xyXG4gICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4ge2ZpbmlzaGVkOiBmYWxzZSwgbmV4dFN0ZXA6IE91dFBheVN0ZXAuRklOSVNILCBwdXJjaGFzZTogdGhpcy5wdXJjaGFzZX07XHJcbiAgICAgICAgIH1cclxuICAgICAgfVxyXG4gICB9XHJcblxyXG4gICAvKipcclxuICAgICogc2VsZWN0UGF5bWVudE1ldGhvZFxyXG4gICAgKiBDaGFtYWRhIGFww7NzIHNlbGVjaW9uYXIgbcOpdG9kbyBkZSBwYWdhbWVudG8gcGFyYSBjb21wcmFzIG9ubGluZS5cclxuICAgICovXHJcbiAgIHB1YmxpYyBzZWxlY3RQYXltZW50TWV0aG9kKHBheW1lbnRNZXRob2Q6IFBheW1lbnRNZXRob2QpOiBPdXRQYXlSZXN1bHQge1xyXG4gICAgICB0aGlzLl9wYXltZW50TWV0aG9kID0gcGF5bWVudE1ldGhvZDtcclxuICAgICAgXHJcbiAgICAgIGlmIChwYXltZW50TWV0aG9kID09IFwiY3JlZGl0XCIpIHtcclxuICAgICAgICAgdGhpcy5wYXltZW50LnR5cGUgPSBQYXltZW50VHlwZS5jcmVkaXRDYXJkO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5wYXltZW50TWV0aG9kID09IFwiY3JlZGl0XCIgfHwgdGhpcy5uZWVkc0J1eWVyRGF0YSkge1xyXG4gICAgICAgICByZXR1cm4ge2ZpbmlzaGVkOiBmYWxzZSwgbmV4dFN0ZXA6IE91dFBheVN0ZXAuUEFZTUVOVF9EQVRBLCBwdXJjaGFzZTogdGhpcy5wdXJjaGFzZX07XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgIHJldHVybiB7ZmluaXNoZWQ6IGZhbHNlLCBuZXh0U3RlcDogT3V0UGF5U3RlcC5GSU5JU0gsIHB1cmNoYXNlOiB0aGlzLnB1cmNoYXNlfTtcclxuICAgICAgfVxyXG4gICB9XHJcblxyXG4gICAvKipcclxuICAgICogQ2hhbWFkYSBhcMOzcyBzZWxlY2lvbmFyIG8gbcOpdG9kbyBkZSBwYWdhbWVudG8gcGFyYSB2ZW5kYXMuXHJcbiAgICAqIEBwYXJhbSBwYXltZW50U2FsZU1ldGhvZCBNw6l0b2RvIGRlIHBhZ2FtZW50byBkYSB2ZW5kYS5cclxuICAgICogQHBhcmFtIHVzaW5nRXh0ZXJuYWxBcHBGb3JQYXltZW50IE9wY2lvbmFsLiBQZXJtaXRlIHF1ZSBvIG91dHBheSBwcm9zc2lnYSBlbSB1bWEgdmVuZGEgZGUgY2FydMOjbyBzZW0gdXNhciBtb2Rlcm5pbmhhLlxyXG4gICAgKi9cclxuICAgcHVibGljIGFzeW5jIHNlbGVjdFNhbGVQYXltZW50TWV0aG9kKHBheW1lbnRTYWxlTWV0aG9kOiBQYXltZW50U2FsZU1ldGhvZCwgdXNpbmdFeHRlcm5hbEFwcEZvclBheW1lbnQgPSBmYWxzZSk6IFByb21pc2U8T3V0UGF5UmVzdWx0PiB7XHJcbiAgICAgIHRoaXMuX3BheW1lbnRTYWxlTWV0aG9kID0gcGF5bWVudFNhbGVNZXRob2Q7XHJcblxyXG4gICAgICBpZiAodGhpcy5tb2Rlcm5pbmhhICYmIFtQYXltZW50U2FsZU1ldGhvZC5DUkVESVQsIFBheW1lbnRTYWxlTWV0aG9kLkRFQklUXS5pbmNsdWRlcyhwYXltZW50U2FsZU1ldGhvZCkpIHtcclxuICAgICAgICAgLy9BcGxpY2FyIHRheGFzIGRlIHZlbmRhIGVtIGNhcnTDo28gbm8gcHJlw6dvIGRhIG1vZGVybmluaGEuXHJcbiAgICAgICAgIHRoaXMuc2V0VmFsdWUoKTtcclxuXHJcbiAgICAgICAgIGlmICh0aGlzLmV2ZW50RGV0YWlsIS5jYXJkU2FsZXNPbkFwcCkge1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICBhd2FpdCB0aGlzLnBhZ3NlZ3Vyb1Jlc29sdmUoKTtcclxuICAgICAgICAgICAgfSBjYXRjaCB7XHJcbiAgICAgICAgICAgICAgIHRocm93IG5ldyBPdXRQYXlFcnJvcihcIkVycm8gYW8gaW5pY2lhciBwYWdzZWd1cm9cIiwgT3V0UGF5RXJyb3JDb2RlLkdFTkVSQUxfRVJST1IpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmICh1c2luZ0V4dGVybmFsQXBwRm9yUGF5bWVudCkge1xyXG4gICAgICAgICAgICAgICB0aGlzLnB1cmNoYXNlID0gYXdhaXQgdGhpcy5jcmVhdGVQdXJjaGFzZSh0cnVlKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgdGhyb3cgbmV3IE91dFBheUVycm9yKCdVc2UgbyBhcHAgXCJWZW5kYXNcIiBvdSBvdXRyYSBtw6FxdWluYSBlIHJlYWxpemUgbyBwYWdhbWVudG8gbm8gY2FydMOjbyBwYXJhIGNvbnRpbnVhci4nLCBPdXRQYXlFcnJvckNvZGUuVU5BQkxFX1RPX1BST0NFRUQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIGF3YWl0IHRoaXMubmV4dFN0ZXBTYWxlKCk7XHJcbiAgIH1cclxuICAgXHJcbiAgIC8qKlxyXG4gICAgKiBDaGFtYWRhIGFww7NzIHNlbGVjaW9uYXIgYSBxdWFudGlkYWRlIGRlIHBhcmNlbGFzIGVtIHVtYSB2ZW5kYSBkZSBjYXJ0w6NvIGRlIGNyw6lkaXRvLlxyXG4gICAgKiBAcGFyYW0gc2VsZWN0ZWRJbnN0YWxsbWVudCBPIMOtbmRpY2UgZGEgcXVhbnRpZGFkZSBkZSBwYXJjZWxhcywgaW5pY2lhbmRvIGVtIHplcm8uXHJcbiAgICAqL1xyXG4gICBwdWJsaWMgYXN5bmMgc2VsZWN0U2FsZUluc3RhbGxtZW50cyhzZWxlY3RlZEluc3RhbGxtZW50OiBudW1iZXIpOiBQcm9taXNlPE91dFBheVJlc3VsdD4ge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkU2FsZUluc3RhbGxtZW50ID0gc2VsZWN0ZWRJbnN0YWxsbWVudDtcclxuICAgICAgdGhpcy5fZ3Jvc3NWYWx1ZSA9IHRoaXMuaW5zdGFsbG1lbnRzW3NlbGVjdGVkSW5zdGFsbG1lbnRdLnRvdGFsQW1vdW50O1xyXG5cclxuICAgICAgcmV0dXJuIGF3YWl0IHRoaXMubmV4dFN0ZXBTYWxlKCk7XHJcbiAgIH1cclxuXHJcbiAgIC8qKlxyXG4gICAgKiBDaGFtYWRhIGFww7NzIHNlbGVjaW9uYXIgbyBtw6l0b2RvIGRlIGVudHJlZ2EgZGEgdmVuZGEuXHJcbiAgICAqIEBwYXJhbSBkZWxpdmVyeU1ldGhvZCBPIG3DqXRvZG8gZGUgZW50cmVnYSBkYSB2ZW5kYS5cclxuICAgICovXHJcbiAgIHB1YmxpYyBhc3luYyBzZWxlY3REZWxpdmVyeU1ldGhvZChkZWxpdmVyeU1ldGhvZDogXCJkaWdpdGFsXCIgfCBcInBoeXNpY2FsXCIpOiBQcm9taXNlPE91dFBheVJlc3VsdD4ge1xyXG4gICAgICB0aGlzLl9kZWxpdmVyeU1ldGhvZCA9IGRlbGl2ZXJ5TWV0aG9kO1xyXG5cclxuICAgICAgcmV0dXJuIGF3YWl0IHRoaXMubmV4dFN0ZXBTYWxlKCk7XHJcbiAgIH1cclxuXHJcbiAgIC8qKlxyXG4gICAgKiBDaGFtYWRhIHBhcmEgZGV0ZXJtaW5hciBvIGNvbXByYWRvciBlbSB1bWEgdmVuZGEgZGlnaXRhbCBzZW0gZm9ybXVsw6FyaW8uXHJcbiAgICAqIEBwYXJhbSBlbWFpbCBPIGVtYWlsIGRvIGNvbXByYWRvci5cclxuICAgICogQHBhcmFtIG5hbWUgTyBub21lIGRvIGNvbXByYWRvciwgcGFyYSBjcmlhciB1bSBub3ZvIHVzdcOhcmlvLlxyXG4gICAgKiBAdGhyb3dzIE5PVF9GT1VORCBjYXNvIHNlamEgaW5mb3JtYWRvIGFwZW5hcyBvIGVtYWlsLCBlIG7Do28gZXhpc3RhIHVzdcOhcmlvIGNvbSBlc3NlIGVtYWlsLlxyXG4gICAgKi9cclxuICAgcHVibGljIGFzeW5jIHNlbGVjdFB1cmNoYXNlcihlbWFpbDogc3RyaW5nLCBuYW1lPzogc3RyaW5nKTogUHJvbWlzZTxPdXRQYXlSZXN1bHQ+IHtcclxuICAgICAgdHJ5IHtcclxuICAgICAgICAgaWYgKCFuYW1lKSB7XHJcbiAgICAgICAgICAgIHRoaXMucHVyY2hhc2VyID0gYXdhaXQgdGhpcy5zZXJ2ZXIhLnRhbGtUb1NlcnZlcihcInB1cmNoYXNlclwiLCB7IGVtYWlsIH0sIHsgcmV0cmllczo0IH0pO1xyXG4gICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnB1cmNoYXNlciA9IGF3YWl0IHRoaXMuc2VydmVyIS50YWxrVG9TZXJ2ZXIoXCJwdXJjaGFzZXIvY3JlYXRlXCIsIHsgZW1haWwsIG5hbWUgfSwge1xyXG4gICAgICAgICAgICAgICB0eXBlOiBcIlBPU1RcIixcclxuICAgICAgICAgICAgICAgcmV0cmllczogNCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgIH1cclxuICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcclxuICAgICAgICAgaWYgKGVycm9yLmVycm9yID09IFwibmFtZVwiKSB0aHJvdyBuZXcgT3V0UGF5RXJyb3IoZXJyb3IuZXJyb3IsIE91dFBheUVycm9yQ29kZS5OT1RfRk9VTkQpO1xyXG4gICAgICAgICBlbHNlIHRocm93IG5ldyBPdXRQYXlFcnJvcihlcnJvciwgT3V0UGF5RXJyb3JDb2RlLkdFTkVSQUxfRVJST1IpO1xyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gdGhpcy5uZXh0U3RlcFNhbGUoKTtcclxuICAgfVxyXG5cclxuICAgLyoqXHJcbiAgICAqIGZpbmlzaFB1cmNoYXNlXHJcbiAgICAqIENoYW1hZGEgYXDDs3MgZGlnaXRhciBvcyBkYWRvcyBkZSBwYWdhbWVudG8uXHJcbiAgICAqIEB0aHJvd3MgUFVSQ0hBU0VfRVJST1IgcXVhbmRvIG9jb3JyZSB1bSBlcnJvIG5hIGNvbXByYS5cclxuICAgICovXHJcbiAgIHB1YmxpYyBhc3luYyBmaW5pc2hQdXJjaGFzZShjYXJkVG9rZW4/OiBzdHJpbmcpOiBQcm9taXNlPE91dFBheVJlc3VsdD4ge1xyXG4gICAgICBpZiAodGhpcy52YWx1ZSA+IDAgJiYgdGhpcy5ldmVudERldGFpbCEucGF5bWVudE1ldGhvZCA9PSBcInBhZ3NlZ3Vyb1wiICYmIHRoaXMucGF5bWVudE1ldGhvZCA9PSBcImNyZWRpdFwiKXtcclxuICAgICAgICAgcmV0dXJuIGF3YWl0IHRoaXMucGFnc2VndXJvQ3JlZGl0Q2FyZCgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgICByZXR1cm4gYXdhaXQgdGhpcy5maW5hbGl6ZVB1cmNoYXNlKGNhcmRUb2tlbik7XHJcbiAgICAgIH1cclxuICAgfVxyXG5cclxuXHJcbiAgIHB1YmxpYyBhc3luYyBmaW5pc2hTYWxlKCkge1xyXG4gICAgICBpZiAoIXRoaXMucHVyY2hhc2UpIHtcclxuICAgICAgICAgdGhpcy5wdXJjaGFzZSA9IGF3YWl0IHRoaXMuY3JlYXRlUHVyY2hhc2UoKTtcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLnB1cmNoYXNlLnN0YXR1cyA8IDQpIHtcclxuICAgICAgICAgdGhpcy5wdXJjaGFzZS5zdGF0dXMgPSA0O1xyXG4gICAgICAgICBhd2FpdCB0aGlzLnNlcnZlciEudGFsa1RvU2VydmVyKFwicHVyY2hhc2UvXCIgKyB0aGlzLnB1cmNoYXNlLmlkLCB0aGlzLnB1cmNoYXNlLCB7XHJcbiAgICAgICAgICAgIHR5cGU6IFwiUFVUXCIsXHJcbiAgICAgICAgICAgIHJldHJpZXM6IDQsXHJcbiAgICAgICAgIH0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICByZXR1cm4gdGhpcy5uZXh0U3RlcFNhbGUoKTtcclxuICAgfVxyXG5cclxuICAgLy9zZXJ2ZSBwYXJhIHRyYXRhciBjYXJ0w6NvIHRhbnRvIGRvIHBhZ3NlZ3VybyBxdWFudG8gZG8gbWVyY2Fkb3BhZ29cclxuICAgcHJpdmF0ZSBwYWdzZWd1cm9DcmVkaXRDYXJkKCk6IFByb21pc2U8T3V0UGF5UmVzdWx0PiB7XHJcbiAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgIGNvbnN0IHBhcmFtID0ge1xyXG4gICAgICAgICAgICBjYXJkTnVtYmVyOiB0aGlzLnBheW1lbnQuY3JlZGl0Lm51bWJlci5yZXBsYWNlKC8gL2csXCJcIiksXHJcbiAgICAgICAgICAgIGJyYW5kOiB0aGlzLnBheW1lbnQuY3JlZGl0LmJyYW5kLFxyXG4gICAgICAgICAgICBjdnY6IHRoaXMucGF5bWVudC5jcmVkaXQuY3Z2LFxyXG4gICAgICAgICAgICBleHBpcmF0aW9uTW9udGg6IHRoaXMucGF5bWVudC5jcmVkaXQudmFsaWRpdHkuc3Vic3RyKDAsIDIpLFxyXG4gICAgICAgICAgICBleHBpcmF0aW9uWWVhcjogJzIwJyArIHRoaXMucGF5bWVudC5jcmVkaXQudmFsaWRpdHkuc3Vic3RyKDMsIDIpLFxyXG4gICAgICAgICAgICBlcnJvcjogKGVycm9yOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgcmVqZWN0KGVycm9yKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgc3VjY2VzczogKHJlc3BvbnNlOiB7IGNhcmQ6IHsgdG9rZW46IHN0cmluZzsgfTsgfSkgPT4ge1xyXG4gICAgICAgICAgICAgICB0aGlzLmZpbmFsaXplUHVyY2hhc2UocmVzcG9uc2UuY2FyZC50b2tlbikudGhlbihyZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICByZXNvbHZlKHJlc3VsdCk7XHJcbiAgICAgICAgICAgICAgIH0sIChlcnJvcjogYW55KSA9PiByZWplY3QoZXJyb3IpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICB9O1xyXG4gICBcclxuICAgICAgICAgUGFnU2VndXJvRGlyZWN0UGF5bWVudC5jcmVhdGVDYXJkVG9rZW4ocGFyYW0pO1xyXG4gICAgICB9KTtcclxuICAgfVxyXG4gICBcclxuICAgcHJpdmF0ZSBhc3luYyBmaW5hbGl6ZVB1cmNoYXNlKGNhcmRUb2tlbj86IHN0cmluZyk6IFByb21pc2U8T3V0UGF5UmVzdWx0PiB7XHJcbiAgICAgIGlmICh0aGlzLnVzZXIpIHtcclxuICAgICAgICAgaWYgKCF0aGlzLnVzZXIuY3BmKSB0aGlzLnVzZXIuY3BmID0gdGhpcy5wYXltZW50LmNyZWRpdC5jcGY7XHJcbiAgICAgICAgIGlmICghdGhpcy51c2VyLmJpcnRoZGF0ZSkgdGhpcy51c2VyLmJpcnRoZGF0ZSA9IHRoaXMucGF5bWVudC5jcmVkaXQuYmlydGhkYXRlO1xyXG4gICAgICAgICBpZiAoIXRoaXMudXNlci5waG9uZSkgdGhpcy51c2VyLnBob25lID0gdGhpcy5wYXltZW50LmNyZWRpdC5waG9uZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgY29uc3QgZGF0YSA9IGF3YWl0IHRoaXMuZ2V0UGF5bWVudFBhcmFtcygpO1xyXG4gICAgICBcclxuICAgICAgaWYgKGNhcmRUb2tlbikge1xyXG4gICAgICAgICBjb25zdCBpbnN0YWxsbWVudHMgPSB0aGlzLmluc3RhbGxtZW50c1t0aGlzLnBheW1lbnQuY3JlZGl0Lmluc3RhbGxtZW50c107XHJcbiAgICAgICAgIGRhdGFbJ3BheW1lbnRNZXRob2QnXSA9ICdjcmVkaXRDYXJkJztcclxuXHJcbiAgICAgICAgIGlmICh0aGlzLmV2ZW50RGV0YWlsIS5wYXltZW50TWV0aG9kID09IFwibWVyY2Fkb3BhZ29cIikge1xyXG4gICAgICAgICAgICBkYXRhW1wicGF5bWVudF9tZXRob2RfaWRcIl0gPSBpbnN0YWxsbWVudHMuY2FyZFByb3ZpZGVyO1xyXG4gICAgICAgICB9XHJcbiAgICAgICAgIFxyXG4gICAgICAgICBkYXRhWydzdHJlZXQnXSA9IHRoaXMucGF5bWVudC5jcmVkaXQuYWRkcmVzcy5zdHJlZXQ7XHJcbiAgICAgICAgIGRhdGFbJ251bWJlciddID0gdGhpcy5wYXltZW50LmNyZWRpdC5hZGRyZXNzLm51bWJlcjtcclxuICAgICAgICAgZGF0YVsnemlwJ10gPSB0aGlzLnBheW1lbnQuY3JlZGl0LmFkZHJlc3MuemlwO1xyXG4gICAgICAgICBkYXRhWydkaXN0cmljdCddID0gdGhpcy5wYXltZW50LmNyZWRpdC5hZGRyZXNzLmRpc3RyaWN0O1xyXG4gICAgICAgICBkYXRhWydjaXR5J10gPSB0aGlzLnBheW1lbnQuY3JlZGl0LmFkZHJlc3MuY2l0eTtcclxuICAgICAgICAgZGF0YVsnc3RhdGVDb2RlJ10gPSB0aGlzLnBheW1lbnQuY3JlZGl0LmFkZHJlc3Muc3RhdGVDb2RlO1xyXG4gICAgICAgICBkYXRhWydjb21wbGVtZW50J10gPSAnJztcclxuICAgICAgICAgZGF0YVsnaW5zdGFsbG1lbnRzUXRkJ10gPSBpbnN0YWxsbWVudHMucXVhbnRpdHkudG9TdHJpbmcoKTtcclxuICAgICAgICAgZGF0YVsnaW5zdGFsbG1lbnRzVmFsdWUnXSA9IHZhbHVlUm91bmQoaW5zdGFsbG1lbnRzLmluc3RhbGxtZW50QW1vdW50LCAnLicpO1xyXG4gICAgICAgICBkYXRhWydjb2RlJ10gPSBjYXJkVG9rZW47XHJcbiAgICAgICAgIGRhdGFbJ2NhcmROYW1lJ10gPSB0aGlzLnBheW1lbnQuY3JlZGl0Lm5hbWU7XHJcbiAgICAgICAgIGRhdGFbJ2NhcmRDcGYnXSA9IHRoaXMucGF5bWVudC5jcmVkaXQuY3BmO1xyXG4gICAgICAgICBkYXRhWydjYXJkUGhvbmUnXSA9IHRoaXMucGF5bWVudC5jcmVkaXQucGhvbmU7XHJcbiAgICAgICAgIFxyXG4gICAgICAgICBkYXRhWydwYWdhbWVudG8nXSA9ICcnICsgaW5zdGFsbG1lbnRzLnF1YW50aXR5ICsgJywnICsgaW5zdGFsbG1lbnRzLmluc3RhbGxtZW50QW1vdW50ICsgJywnICsgaW5zdGFsbG1lbnRzLnRvdGFsQW1vdW50O1xyXG4gICAgICB9IGVsc2UgaWYgKHRoaXMucGF5bWVudE1ldGhvZCA9PSBcImJvbGV0b1wiKSB7XHJcbiAgICAgICAgIGRhdGFbJ3BheW1lbnRNZXRob2QnXSA9ICdib2xldG8nO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgICBkYXRhWydwYXltZW50TWV0aG9kJ10gPSAnZnJlZSc7XHJcbiAgICAgIH1cclxuICAgICAgZGF0YVsndGlja2V0RGF0YSddID0gdGhpcy50aWNrZXREYXRhO1xyXG5cclxuICAgICAgdGhpcy5pbnNjcmlwdGlvblBhcnRpY2lwYW50RGF0YSA9IFtdO1xyXG4gICAgICB0cnkge1xyXG4gICAgICAgICBjb25zdCBwdXJjaGFzZSA9IGF3YWl0IHRoaXMuZG9QYXltZW50KGRhdGEpO1xyXG4gICAgICAgICB0aGlzLnB1cmNoYXNlID0gcHVyY2hhc2U7XHJcbiAgICAgICAgIHJldHVybiB7ZmluaXNoZWQ6IHRydWUsIG5leHRTdGVwOiBPdXRQYXlTdGVwLkZJTklTSCwgcHVyY2hhc2U6IHRoaXMucHVyY2hhc2V9O1xyXG4gICAgICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgICAgICBpZiAoZXJyb3IuZXJyb3JDb2RlKSB0aHJvdyBlcnJvcjtcclxuXHJcbiAgICAgICAgIHRocm93IG5ldyBPdXRQYXlFcnJvcihlcnJvci5lcnJvcj8udGV4dCB8fCBcIkhvdXZlIHVtIGVycm8gbmEgY29tcHJhXCIsIE91dFBheUVycm9yQ29kZS5QVVJDSEFTRV9FUlJPUik7XHJcbiAgICAgIH1cclxuICAgfVxyXG5cclxuICAgcHJpdmF0ZSBhc3luYyBkb1BheW1lbnQoZGF0YTogYW55KTogUHJvbWlzZTxQdXJjaGFzZT4ge1xyXG4gICAgICBsZXQgcmVzcG9uc2U6IHtlcnJvcjogZmFsc2UsIHB1cmNoYXNlOiBQdXJjaGFzZX0gfCB7ZXJyb3I6IHRydWUsIGVycm9yTXNnOiBzdHJpbmd9O1xyXG5cclxuICAgICAgdHJ5IHtcclxuICAgICAgICAgaWYgKHRoaXMudmFsdWUgPT0gMCkge1xyXG4gICAgICAgICAgICByZXNwb25zZSA9IGF3YWl0IHRoaXMuc2VydmVyIS50YWxrVG9TZXJ2ZXIoXCJwYXltZW50TWVyY2Fkb3BhZ29cIiwgZGF0YSwgeyB0eXBlOiBcIlBPU1RcIiB9KTtcclxuICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmV2ZW50RGV0YWlsIS5wYXltZW50TWV0aG9kID09IFwibWVyY2Fkb3BhZ29cIikge1xyXG4gICAgICAgICAgICByZXNwb25zZSA9IGF3YWl0IHRoaXMuaHR0cC5wb3N0PHtlcnJvcjogZmFsc2UsIHB1cmNoYXNlOiBQdXJjaGFzZX0gfCB7ZXJyb3I6IHRydWUsIGVycm9yTXNnOiBzdHJpbmd9Pih0aGlzLnNlcnZlclVybCArICdhcGkvcGF5bWVudE1lcmNhZG9wYWdvJywgZGF0YSkudG9Qcm9taXNlKCk7XHJcbiAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJlc3BvbnNlID0gYXdhaXQgdGhpcy5odHRwLnBvc3Q8e2Vycm9yOiBmYWxzZSwgcHVyY2hhc2U6IFB1cmNoYXNlfSB8IHtlcnJvcjogdHJ1ZSwgZXJyb3JNc2c6IHN0cmluZ30+KHRoaXMuc2VydmVyVXJsICsgJ2FwaS9wYXltZW50JywgZGF0YSkudG9Qcm9taXNlKCk7XHJcbiAgICAgICAgIH1cclxuICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcclxuICAgICAgICAgdGhyb3cgbmV3IE91dFBheUVycm9yKGVycm9yLmVycm9yPy50ZXh0IHx8IFwiSG91dmUgdW0gZXJybyBuYSBjb21wcmFcIiwgT3V0UGF5RXJyb3JDb2RlLlBVUkNIQVNFX0VSUk9SKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHJlc3BvbnNlLmVycm9yKSB7XHJcbiAgICAgICAgIHRocm93IG5ldyBPdXRQYXlFcnJvcihyZXNwb25zZS5lcnJvck1zZywgT3V0UGF5RXJyb3JDb2RlLlBVUkNIQVNFX0VSUk9SKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHJlc3BvbnNlLnB1cmNoYXNlO1xyXG4gICB9XHJcblxyXG4gICBwcml2YXRlIGFzeW5jIGNyZWF0ZVB1cmNoYXNlKHVucGFpZFB1cmNoYXNlOiBib29sZWFuID0gZmFsc2UpOiBQcm9taXNlPFB1cmNoYXNlPiB7XHJcblxyXG4gICAgICBjb25zdCBkYXRhOiBhbnkgPSB7IHB1cmNoYXNlcl9pZDogdGhpcy5wdXJjaGFzZXIgPyB0aGlzLnB1cmNoYXNlci5pZCA6IG51bGwsIHBheW1lbnRfbWV0aG9kOiB0aGlzLnBheW1lbnRTYWxlTWV0aG9kIH07XHJcbiAgICAgIGlmICh0aGlzLmNhcnQuZGlzY291bnQpIGRhdGEuZGlzY291bnRfaWQgPSB0aGlzLmNhcnQuZGlzY291bnQuaWQ7XHJcblxyXG4gICAgICBpZiAodGhpcy5jYXJ0KSB7XHJcbiAgICAgICAgIGNvbnN0IHRpY2tldExpc3Q6IHsgW3RpY2tldElkOiBudW1iZXJdOiBudW1iZXIgfSA9IHt9O1xyXG4gICAgICAgICBjb25zdCByZXNlcnZhdGlvbkxpc3Q6IHsgW3RpY2tldElkOiBudW1iZXJdOiBudW1iZXJbXSB9ID0ge307XHJcbiAgICAgICAgIHRoaXMuY2FydC5pdGVtcy5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICAgICAgICB0aWNrZXRMaXN0W2l0ZW0udGlja2V0LmlkXSA9IGl0ZW0ucXRkO1xyXG4gICAgICAgICAgICBpZihpdGVtLnJlc2VydmF0aW9ucy5sZW5ndGggPiAwKSByZXNlcnZhdGlvbkxpc3RbaXRlbS50aWNrZXQuaWRdID0gaXRlbS5yZXNlcnZhdGlvbnMubWFwKChyKT0+ci5pZCk7XHJcbiAgICAgICAgIH0pO1xyXG4gICAgICAgICBkYXRhLmNhcnQgPSB0aWNrZXRMaXN0XHJcbiAgICAgICAgIGlmIChPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyhyZXNlcnZhdGlvbkxpc3QpLmxlbmd0aCA+IDApIGRhdGEuY2FydF9yZXNlcnZhdGlvbiA9IHJlc2VydmF0aW9uTGlzdDtcclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHRoaXMucHJvZHVjdENhcnQpIHtcclxuICAgICAgICAgZGF0YS5wcm9kdWN0X2NhcnQgPSB0aGlzLnByb2R1Y3RDYXJ0Lml0ZW1zO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAodGhpcy5pbnNjcmlwdGlvblBhcnRpY2lwYW50RGF0YSkgZGF0YS5wYXJ0aWNpcGFudF9kYXRhID0gdGhpcy5pbnNjcmlwdGlvblBhcnRpY2lwYW50RGF0YTtcclxuICAgICAgaWYgKHRoaXMuZGVsaXZlcnlNZXRob2QpIGRhdGEucHVyY2hhc2VfdHlwZSA9IHRoaXMuZGVsaXZlcnlNZXRob2Q7XHJcbiAgICAgIGlmICh1bnBhaWRQdXJjaGFzZSkgZGF0YS51bnBhaWRfcHVyY2hhc2UgPSB1bnBhaWRQdXJjaGFzZTtcclxuICAgICAgZGF0YS5pc19zZWxsaW5nID0gdGhpcy5pc1NlbGxpbmc7XHJcblxyXG4gICAgICBpZiAodGhpcy5tb2Rlcm5pbmhhKSB7XHJcbiAgICAgICAgIGRhdGEubW9kZXJuaW5oYSA9IHRydWVcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHRoaXMuc2VydmVyIS50YWxrVG9TZXJ2ZXIoXCJwdXJjaGFzZS9jcmVhdGVcIiwgZGF0YSwge1xyXG4gICAgICAgICB0eXBlOiBcIlBPU1RcIixcclxuICAgICAgICAgcmV0cmllczogNCxcclxuICAgICAgfSk7XHJcbiAgIH1cclxuXHJcbiAgIC8qKlxyXG4gICAgKiBGdW7Dp8OjbyBxdWUgYW5hbMOtc2EgYSBjb21wcmEsIGUgcmV0b3JuYSBvIHByw7N4aW1vIGlucHV0IG5lY2Vzc8OhcmlvIHBlbG8gdXN1w6FyaW8uXHJcbiAgICAqIEByZXR1cm5zIFVtIG9iamV0byBgT3V0UGF5UmVzdWx0YCwgY29tIG8gcHLDs3hpbW8gcGFzc28gZSBvYmpldG8gZGEgY29tcHJhLlxyXG4gICAgKi9cclxuICAgcHJpdmF0ZSBhc3luYyBuZXh0U3RlcFNhbGUoKTogUHJvbWlzZTxPdXRQYXlSZXN1bHQ+IHtcclxuICAgICAgY29uc3QgcmVzdWx0OiBPdXRQYXlSZXN1bHQgPSB7ZmluaXNoZWQ6IGZhbHNlLCBuZXh0U3RlcDogLTEsIHB1cmNoYXNlOiB0aGlzLnB1cmNoYXNlfTtcclxuXHJcbiAgICAgIC8vU2UgYSBjb21wcmEgZm9yIHBhZ2EsIHZlcmlmaWNhciBtw6l0b2RvIGRlIHBhZ2FtZW50byBlIHBhcmNlbGFtZW50by5cclxuICAgICAgaWYgKHRoaXMudmFsdWUgPiAwKSB7XHJcbiAgICAgICAgIGlmICh0aGlzLnBheW1lbnRTYWxlTWV0aG9kID09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICByZXN1bHQubmV4dFN0ZXAgPSBPdXRQYXlTdGVwLlBBWU1FTlRfTUVUSE9EO1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5tb2Rlcm5pbmhhICYmIHRoaXMucGF5bWVudFNhbGVNZXRob2QgPT0gUGF5bWVudFNhbGVNZXRob2QuQ1JFRElUKSB7XHJcbiAgICAgICAgICAgICAgIGlmICh0aGlzLnNlbGVjdGVkU2FsZUluc3RhbGxtZW50IDwgMCkge1xyXG4gICAgICAgICAgICAgICAgICByZXN1bHQubmV4dFN0ZXAgPSBPdXRQYXlTdGVwLklOU1RBTExNRU5UUztcclxuICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgaWYgKHRoaXMucGF5bWVudFNhbGVNZXRob2QgPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3BheW1lbnRTYWxlTWV0aG9kID0gUGF5bWVudFNhbGVNZXRob2QuRlJFRTtcclxuICAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICAvL0RlcG9pcywgdmVyaWZpY2FyIG8gbcOpdG9kbyBkZSBlbnRyZWdhLiBDYXNvIHPDsyBzZWphIHBvc3PDrXZlbCB1bSwgZXN0ZSDDqSBzZWxlY2lvbmFkby5cclxuICAgICAgaWYgKHJlc3VsdC5uZXh0U3RlcCA8IDApIHtcclxuICAgICAgICAgaWYgKCF0aGlzLmRlbGl2ZXJ5TWV0aG9kKSB7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5tb2Rlcm5pbmhhICYmICh0aGlzLnByb2R1Y3RzU2FsZSB8fCB0aGlzLnZhbHVlID09IDApKSB7XHJcbiAgICAgICAgICAgICAgIHRoaXMuX2RlbGl2ZXJ5TWV0aG9kID0gXCJwaHlzaWNhbFwiO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgICAgaWYgKHRoaXMubW9kZXJuaW5oYSAmJiB0aGlzLmV2ZW50RGV0YWlsIS5hbGxvd19waHlzaWNhbF9zYWxlcyAmJiB0aGlzLmV2ZW50RGV0YWlsIS5hbGxvd19kaWdpdGFsX3NhbGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgIHJlc3VsdC5uZXh0U3RlcCA9IE91dFBheVN0ZXAuREVMSVZFUllfTUVUSE9EO1xyXG4gICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMubW9kZXJuaW5oYSAmJiB0aGlzLmV2ZW50RGV0YWlsIS5hbGxvd19waHlzaWNhbF9zYWxlcykge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLl9kZWxpdmVyeU1ldGhvZCA9IFwicGh5c2ljYWxcIjtcclxuICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmV2ZW50RGV0YWlsIS5hbGxvd19kaWdpdGFsX3NhbGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMuX2RlbGl2ZXJ5TWV0aG9kID0gXCJkaWdpdGFsXCI7XHJcbiAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBPdXRQYXlFcnJvcihcIkhvdXZlIHVtIHByb2JsZW1hIHBhcmEgY29udGludWFyIGEgdmVuZGEuIEZhbGUgY29tIG8gYWRtaW5pc3RyYWRvciBkbyBldmVudG8uXCIsIE91dFBheUVycm9yQ29kZS5QVVJDSEFTRV9FUlJPUiwge290dG86IHRydWUsIG90dG9EYXRhOiB7IGltYWdlOiBcInRyaXN0ZVwiLCBkdXJhdGlvbjogMzAwMCB9fSk7XHJcbiAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgLy9PIHByw7N4aW1vIHBhc3NvIMOpIHZlcmlmaWNhciBkYWRvcyBkb3MgY29tcHJhZG9yZXMsIHZpYSBmb3JtdWzDoXJpbyBvdSB0ZWxhIGRvIGNvbXByYWRvci5cclxuICAgICAgLy9WZW5kYXMgZGUgcHJvZHV0b3MgbsOjbyBwcmVjaXNhbSBkZSBpbmZvcm1hw6fDo28gc29icmUgY29tcHJhZG9yLlxyXG4gICAgICBpZiAocmVzdWx0Lm5leHRTdGVwIDwgMCAmJiAhdGhpcy5wcm9kdWN0c1NhbGUpIHtcclxuICAgICAgICAgaWYgKHRoaXMuZXZlbnREZXRhaWwhLmhhdmVfcGFydGljaXBhbnRfZm9ybSB8fCB0aGlzLnZhbHVlID09IDApIHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLmluc2NyaXB0aW9uUGFydGljaXBhbnREYXRhLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICByZXN1bHQubmV4dFN0ZXAgPSBPdXRQYXlTdGVwLlBVUkNIQVNFX0ZPUk07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmRlbGl2ZXJ5TWV0aG9kID09IFwiZGlnaXRhbFwiICYmICF0aGlzLmV2ZW50RGV0YWlsIS5oYXZlX3BhcnRpY2lwYW50X2Zvcm0pIHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnB1cmNoYXNlcikge1xyXG4gICAgICAgICAgICAgICByZXN1bHQubmV4dFN0ZXAgPSBPdXRQYXlTdGVwLlBVUkNIQVNFUjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vQ2FzbyBhIGNvbXByYSBzZWphIGZlaXRhIG5vIGNhcnTDo28sIG8gYXBwIGRldmVyw6EgaXIgcGFyYSBhIHRlbGEgZGUgY29icmFuw6dhcy5cclxuICAgICAgaWYgKHJlc3VsdC5uZXh0U3RlcCA8IDApIHtcclxuICAgICAgICAgaWYgKHRoaXMubW9kZXJuaW5oYSAmJiB0aGlzLnBheW1lbnRTYWxlTWV0aG9kICYmIFtQYXltZW50U2FsZU1ldGhvZC5DUkVESVQsIFBheW1lbnRTYWxlTWV0aG9kLkRFQklUXS5pbmNsdWRlcyh0aGlzLnBheW1lbnRTYWxlTWV0aG9kKSkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMucHVyY2hhc2UpIHtcclxuICAgICAgICAgICAgICAgdGhpcy5wdXJjaGFzZSA9IGF3YWl0IHRoaXMuY3JlYXRlUHVyY2hhc2UodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgIHJlc3VsdC5wdXJjaGFzZSA9IHRoaXMucHVyY2hhc2U7XHJcbiAgICAgICAgICAgICAgIHJlc3VsdC5uZXh0U3RlcCA9IE91dFBheVN0ZXAuUExVR1BBRztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vQ2FzbyBzZWphIHVtYSBjb21wcmEgZsOtc2ljYSBxdWUgbsOjbyBmb2kgZmluYWxpemFkYSBhaW5kYSwgZWxhIHByZWNpc2Egc2VyIGltcHJlc3NhLlxyXG4gICAgICBpZiAocmVzdWx0Lm5leHRTdGVwIDwgMCkge1xyXG4gICAgICAgICBpZiAodGhpcy5kZWxpdmVyeU1ldGhvZCA9PSBcInBoeXNpY2FsXCIpIHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLnB1cmNoYXNlKSB7XHJcbiAgICAgICAgICAgICAgIHRoaXMucHVyY2hhc2UgPSBhd2FpdCB0aGlzLmNyZWF0ZVB1cmNoYXNlKHRydWUpO1xyXG4gICAgICAgICAgICAgICByZXN1bHQucHVyY2hhc2UgPSB0aGlzLnB1cmNoYXNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5wdXJjaGFzZSEuc3RhdHVzIDwgNCkge1xyXG4gICAgICAgICAgICAgICByZXN1bHQubmV4dFN0ZXAgPSBPdXRQYXlTdGVwLlBSSU5UO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICByZXN1bHQubmV4dFN0ZXAgPSBPdXRQYXlTdGVwLkZJTklTSDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vUG9yIGZpbSwgYSB2ZW5kYSDDqSBlbmNlcnJhZGEuXHJcbiAgICAgIGlmIChyZXN1bHQubmV4dFN0ZXAgPCAwKSB7XHJcbiAgICAgICAgIGlmICghdGhpcy5wdXJjaGFzZSB8fCB0aGlzLnB1cmNoYXNlLnN0YXR1cyA8IDQpIGF3YWl0IHRoaXMuZmluaXNoU2FsZSgpO1xyXG4gICAgICAgICByZXN1bHQucHVyY2hhc2UgPSB0aGlzLnB1cmNoYXNlO1xyXG4gICAgICAgICByZXN1bHQuZmluaXNoZWQgPSB0cnVlO1xyXG4gICAgICAgICByZXN1bHQubmV4dFN0ZXAgPSBPdXRQYXlTdGVwLkZJTklTSDtcclxuICAgICAgfVxyXG5cclxuICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgfVxyXG4gICBcclxufVxyXG5cclxuZXhwb3J0IHR5cGUgT3V0UGF5UmVzdWx0ID0ge1xyXG4gICBmaW5pc2hlZDogYm9vbGVhbixcclxuICAgbmV4dFN0ZXA6IE91dFBheVN0ZXAsXHJcbiAgIHB1cmNoYXNlPzogUHVyY2hhc2UsXHJcbn1cclxuXHJcblxyXG5leHBvcnQgZW51bSBPdXRQYXlTdGVwIHtcclxuICAgLyoqIFNlbGXDp8OjbyBkZSBmb3JtYSBkZSBwYWdhbWVudG8gKi9cclxuICAgUEFZTUVOVF9NRVRIT0QsXHJcbiAgIC8qKiBGb3JtdWzDoXJpbyBkZSBkYWRvcyBkbyBjb21wcmFkb3IgZSBkb3MgcGFydGljaXBhbnRlcyAqL1xyXG4gICBQVVJDSEFTRV9GT1JNLFxyXG4gICAvKiogSW1wcmVzc8OjbyBkbyBpbmdyZXNzbywgbm9zIGNhc29zIG9uZGUgbyBwYWdhbWVudG8gbsOjbyBvY29ycmUgdmlhIFBsdWdQYWcgKGllLiBEaW5oZWlybykgKi9cclxuICAgUFJJTlQsXHJcbiAgIC8qKiBGaW5hbGl6YXIgcHJvY2Vzc28gZGUgY29tcHJhICovXHJcbiAgIEZJTklTSCxcclxuICAgLyoqIEZvcm11bMOhcmlvIGRlIGRhZG9zIGRvIHBhZ2FtZW50byAoY2FydMOjbykgKi9cclxuICAgUEFZTUVOVF9EQVRBLFxyXG4gICAvKiogU2VsZcOnw6NvIGRlIGZvcm1hIGRlIHJlY2ViaW1lbnRvICovXHJcbiAgIERFTElWRVJZX01FVEhPRCxcclxuICAgLyoqIFNlbGXDp8OjbyBkbyBwYXJjZWxhbWVudG8gZG8gcGFnYW1lbnRvICovXHJcbiAgIElOU1RBTExNRU5UUyxcclxuICAgLyoqIFBhZ2FtZW50byB2aWEgUGx1Z1BhZyAqL1xyXG4gICBQTFVHUEFHLFxyXG4gICAvKiogSWRlbnRpZmljYcOnw6NvIGRvIGNvbXByYWRvciBwYXJhIHZlbmRhcyBkaWdpdGFpcyBzZW0gZm9ybXVsw6FyaW8gKi9cclxuICAgUFVSQ0hBU0VSXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBPdXRQYXlFcnJvciB7XHJcbiAgIFxyXG4gICBjb25zdHJ1Y3RvciAoXHJcbiAgICAgIHB1YmxpYyBtc2c6IHN0cmluZyxcclxuICAgICAgcHVibGljIGVycm9yQ29kZTogT3V0UGF5RXJyb3JDb2RlID0gT3V0UGF5RXJyb3JDb2RlLkdFTkVSQUxfRVJST1IsXHJcbiAgICAgIHB1YmxpYyBkYXRhOiB7XHJcbiAgICAgICAgIG90dG8/OiBib29sZWFuLFxyXG4gICAgICAgICBvdHRvRGF0YT86IHtpbWFnZTogT3R0b0ltZywgZHVyYXRpb24/OiBudW1iZXJ9XHJcbiAgICAgIH0gPSB7fSxcclxuICAgKSB7IH07XHJcblxyXG4gICBwdWJsaWMgZ2V0IG90dG9BbGVydCgpOiBPdHRvQWxlcnRNZXNzYWdlIHtcclxuICAgICAgaWYgKHRoaXMuZGF0YS5vdHRvKSB7XHJcbiAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHRleHQ6IHRoaXMubXNnLFxyXG4gICAgICAgICAgICBpbWFnZTogdGhpcy5kYXRhLm90dG9EYXRhIS5pbWFnZSxcclxuICAgICAgICAgICAgZHVyYXRpb246IHRoaXMuZGF0YS5vdHRvRGF0YSEuZHVyYXRpb24sXHJcbiAgICAgICAgIH1cclxuICAgICAgfSBlbHNlIHRocm93IG5ldyBFcnJvcihcIk7Do28gaMOhIG1lbnNhZ2VtIGRvIG90dG8gbmVzdGUgZXJyb1wiKTtcclxuICAgfVxyXG59XHJcblxyXG5cclxuZXhwb3J0IGVudW0gT3V0UGF5RXJyb3JDb2RlIHtcclxuICAgLyoqIEVycm8gZ2Vuw6lyaWNvICovXHJcbiAgIEdFTkVSQUxfRVJST1IgPSAxLFxyXG4gICAvKiogTyB1c3XDoXJpbyBwcmVjaXNhIHZhbGlkYXIgbyBlbWFpbCBwYXJhIHByb3NzZWd1aXIgKi9cclxuICAgVkFMSURBVEVfRU1BSUwsXHJcbiAgIC8qKiBPIGluZ3Jlc3NvIHNvbGljaXRhZG8gasOhIGVzdMOhIGVzZ290YWRvICovXHJcbiAgIFRJQ0tFVF9VTkFWQUlMQUJMRSxcclxuICAgLyoqIEhvdXZlIHVtIGVycm8gYW8gcHJvY2Vzc2FyIGEgY29tcHJhICovXHJcbiAgIFBVUkNIQVNFX0VSUk9SLFxyXG4gICAvKiogTyB1c3XDoXJpbyBuw6NvIGVzdMOhIGxvZ2FkbyBubyBzaXN0ZW1hICovXHJcbiAgIFVOQVVUSEVOVElDQVRFRCxcclxuICAgLyoqIE7Do28gw6kgcG9zc8OtdmVsIGV4ZWN1dGFyIGEgYcOnw6NvLCB1bSBjYW1pbmhvIGFsdGVybmF0aXZvIGRldmUgc2VyIGJ1c2NhZG8gKi9cclxuICAgVU5BQkxFX1RPX1BST0NFRUQsXHJcbiAgIC8qKiBOw6NvIGZvaSBlbmNvbnRyYWRvIHVtIHJlc3VsdGFkbyBwYXJhIG9zIGRhZG9zIGZvcm5lY2lkb3MgKi9cclxuICAgTk9UX0ZPVU5EXHJcbn0iXX0=